%	smooth		- Smooth data using a variety of methods
%
%	[ny]	= smooth(x,y,w,[,para])
%
%	_____OUTPUT_____________________________________________________________
%	ny	new (smoothed) y corresponding to x		(col vectors)
%
%	_____INPUT______________________________________________________________
%	x	independent scalars				(row vector)
%	y	dependent vector				(col vectors)
%		(to be independently smoothed in each row/dim)
%	w	weight at each abscissae			(row vector)
%	para	see lanspara.m paraget.m			(string)
%		-clos -smoother
%
%	_____NOTES______________________________________________________________
%	for demo, call function without parameters
%	- # points in ny may not = # points in y due to removal of duplicate x
%	- very small value of span needed (0.00001) for cubic spline smoother
%	  due to unit interval normalization of knots
%	
%	_____SEE ALSO___________________________________________________________
%	llr	lwavg	cspline1	
%
%	(C) 1998.11.03 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[ny]	= smooth(x,y,w,para)

fac	= 1;	%	magnification factor for length of curve

if nargin>0
%--------------------	regular
[d,n]	= size(y);
%----- set default parameters
if nargin<4
	para=[];
end
clos		= paraget('-clos',para);
smoother	= paraget('-smoother',para);
if clos						% compute closing gap distance
	gapdist	= vdist(y(:,1),y(:,n));
	para	= paraset('-clos',gapdist,para);
end

switch smoother,
	case 1,
		ny	= llr(x,y,1:length(x),para);	
	case 2,
		ny	= lwavg(x,y,1:length(x),para);
	case 3,
		span	= paraget('-span',para);
		p	= 1-span;
		[d,n]	= size(y);
		for j=1:d
			if clos~=0
				%-----	closed curve
				flen	= max(x)+gapdist;
				x	= fac*x/flen;
				gapdist	= fac*gapdist/flen;
				ny(j,:)	= cspline1p(x',y(j,:)',p,gapdist,w')';
			else
				%-----	open curve
				flen	= max(x);
				x	= fac*x/flen;	% unit length
				ny(j,:)	= cspline1(x',y(j,:)',p,w')';
			end
		end
debug=0;
if debug
figure(2)
for j=1:d
	subplot(4,1,j)
	plot(x,ny(j,:),'r-',x,y(j,:),'k+');
	title(j);
end
drawnow;
figure(1)
%pause
end
	otherwise,
		error('smoother unspecified');
end
%--------------------	regular ends
else
%--------------------	demo
disp('running smooth.m in demo mode');

para	= '-smoother 3 -span .00001 -clos 1';
n	= 50;
noise	= .5;
int	= 2*pi/n;
x	= 0:int:(2*pi-int);
y	= cos(x)+sin(3*x)+(rand(size(x))-.5)*noise;
fx	= smooth(x,y,ones(size(x)),para);
clf
plot(x,y,'.b');
hold on;
plot(x,fx,'r-');
title('Smoother');

%--------------------	demo ends
end
