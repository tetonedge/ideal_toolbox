%	wlsqr		- Weighted polynomial Least Square Fit @ x(px)
%
%	[yhat]	= wlsqr(x,y,w,px[,order])
%
%	_____OUTPUT_____________________________________________________________
%	yhat	fitted value of x(px)				(scalar)
%
%	_____INPUT______________________________________________________________
%	x	scalar independent variable			(row vector)
%	y	scalar dependent variable			(row vector)
%	w	weights for each x				(row vector)
%	px	index of x point to compute fit			(scalar)
%	order	order of polynomial fit				(scalar)
%		1	linear
%		2	quadratic
%		3	cubic
%		...
%		defaults to 1
%
%	(C) 1998.02.24 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [yhat]	= wlsqr(x,y,w,px,order)

if nargin<5
	order	= 1;
end

x=	x';
y=	y';
w=	w';

n	= length(y);

if length(w)<order+1,
	disp('too few weights for specified order');
	return
end

%---------- short cut (not very short, and very bad too)
%	#pts		inv		shortcut
%	20		.55		.44
%	200		13.45		10.6
%	900		44.1		32.13
%syhat	= 0;
%syhat=mean(w.*y)
%syhat	= w'*y;
%return

A	= ones(n,1);

for i=1:order
	A	= [A x.^i];
end

wroot	= sqrt(w);


%---------- pseudo inverse solution to weighted least squares
yw	= wroot.*y;
Aw	= (wroot*ones(1,order+1)).*A;
beta	= inv(Aw'*Aw)*(Aw')*yw;
yhat	= A(px,:)*beta;
