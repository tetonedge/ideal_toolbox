%	cspline1	- Cubic smoothing spline 1-D
%
%	[fx,x,y]	= cspline1(x,y,p[,w])
%
%	_____OUTPUT_____________________________________________________________
%	fx	smoothed y corresponding to f(x)		(col vector)
%	x	sorted x					(col vector)
%	y	sorted y					(col vector)
%
%	_____INPUT______________________________________________________________
%	x	independent scalars				(col vector)
%	y	dependent vector				(col vector)
%	p	smoothing parameter				(scalar)
%		[0,1] : 1 implies point interpolation
%	w	weight						(col vector)	
%		1	normal weight
%		f	positive real
%
%	_____NOTES______________________________________________________________
%	for demo, call function without parameters
%
%	- At least 4 data points needed!
%	- If weight w is not specified, equal weightage is assumed
%	- x MUST BE DISTINCT!!!!
%
%	_____SEE ALSO___________________________________________________________
%	cspline1p
%
%	(C) 1998.09.18 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/


function	[fx,x,y]	= cspline1(x,y,p,w)

if nargin>0
%--------------------	regular
[n d]	= size(y);
if n<4
	error('At least 4 points are needed for cubic spline interpolation');
end
if nargin<4
	w	= ones(n,1);			% initially all equal weights
end
sig	= 1./w;					% more weight => less variation

%-----	Precompute common terms
h	= diff(x);				% 1 ... n-1
hi	= 1./h;

a	= sig(1:n-2).*hi(1:n-2);
b	= sig(3:n).*hi(2:n-1); 			b	= [0;0;b];
c	= sig(2:n-1).*(hi(1:n-2)+hi(2:n-1));	c	= [0;c];

%----------	Form disbanded version of 6*(1-p)*Q'*D*D*Q+pR as B
%----------	there are 5 bands altogether
%-----	Initialized B to prepare for Gaussian Elimination
B	= zeros(n-2,6);
fac	= 6*(1-p);

%-----	Form	6(1-p)*Q'*D*D*Q + p*R
band15	= fac*(a(3:n-2).*b(3:n-2));
band24	= p*h(2:n-2) + fac*(-b(3:n-1).*c(3:n-1)-a(2:n-2).*c(2:n-2));
band3	= (p*2)*(h(1:n-2)+h(2:n-1)) + fac*(a(1:n-2).*a(1:n-2)+b(3:n).*b(3:n)+c(2:n-1).*c(2:n-1));

B(3:n-2,1)	= band15;
B(2:n-2,2)	= band24;
B(:,3)		= band3;
B(1:n-3,4)	= band24;
B(1:n-4,5)	= band15;

%-----	Incorporate target vector
%-----	Shortcut for Q'*y is diff(diff(y).*hi);
B(:,6)		= diff(diff(y).*hi);

%-------------------- Gaussian Eliminate 6*(1-p)*Q'*D*D*Q+pR 
for j=1:n-4;
	B(j,4:6)	= (1/B(j,3))*B(j,4:6);
	B(j+1,[3 4 6])	= B(j+1,[3 4 6])-B(j+1,2)*B(j,[4:6]);
	B(j+2,[2 3 6])	= B(j+2,[2 3 6])-B(j+2,1)*B(j,[4:6]);
end
j=n-3;
	B(j,4:6)	= (1/B(j,3))*B(j,4:6);
	B(j+1,[3 4 6])	= B(j+1,[3 4 6])-B(j+1,2)*B(j,[4:6]);
j=n-2;
	B(j,6)		= B(j,6)/B(j,3);
j=n-3;
	B(j,6)		= B(j,6)-B(j,4)*B(j+1,6);
for j=n-4:-1:1;
	B(j,6)		= B(j,6)-B(j,[4 5])*B(j+[1 2],6);
end
%-----	Solution found via Gaussian elimination (no pivoting)
u	= B(:,6);

%-----	Shortcut for Qu
Qu	= diff([0;diff([0;u;0]).*hi;0]);
DDQu	= sig.*sig.*Qu;

%-----	Smoothed values
fx	= y-fac*DDQu;

%--------------------	regular ends
else
%--------------------	demo
disp('running cspline1.m in demo mode');
n	= 100;
p	= .9;
noise	= .5;
int	= 2*pi/n;
x	= 0:int:(2*pi-int);
x	= x';
y	= cos(x)+sin(3*x)+(rand(size(x))-.5)*noise;
fx	= cspline1(x,y,p);		% verification
clf
plot(x,y,'.b');
hold on;
plot(x,fx,'r-');
title('Natural Cubic Smoothing spline');
%--------------------	demo ends
end
