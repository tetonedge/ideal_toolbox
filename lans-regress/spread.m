%	spread		- Spread out knots/data to remove repetitions
%
%	[x,y]	= spread(x,y)
%
%	_____OUTPUTS____________________________________________________________
%	x	spreaded knot indices in 1-D			(vector)
%	y	spreaded knot values				(col vectors)
%
%	_____INPUTS_____________________________________________________________
%	x	knot indices in 1-D	(sorted)		(vector)
%	y	knot values		(sorted)		(col vectors)
%
%	_____NOTES______________________________________________________________
%	For principal curves, x=knots, y=data points projecting to x	
%
%	_____EXAMPLE____________________________________________________________
%
%	(C) 1998.04.27 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[x,y]	= spread(x,y)

extend	= .01;		% fraction of line to expand

[d,n]	= size(y);

wsame	= finddup(x);
[ng,dum]= size(wsame);

if ~isempty(wsame)
	if ng>1
		for g=1:ng-1
			s1	= wsame(g,1);		% starting knot
			s2	= wsame(g,2);		% ending knot
			ns	= wsame(g,3);		% number of repeats (+ self)
			dv	= y(:,s2+1)-y(:,s2);	% displacement vector
			dvlen	= vdist(dv,0);		
			shiftv	= (0:ns-1)*extend*dvlen/(ns-1);
			x(s1:s2)= x(s1:s2)+shiftv;
			%-----	sort distance of y to original duplicate point
			%-----	allocate points according to this
			%-----	not much use
		end
	end
		s1	= wsame(ng,1);		% starting knot
		s2	= wsame(ng,2);		% ending knot
		ns	= wsame(ng,3);		% number of repeats (+ self)
			
		if s2<n					% exists points after s2
			dv	= y(:,s2+1)-y(:,s2);	% displacement vector
			dvlen	= vdist(dv,0);	
			shiftv	= (0:ns-1)*extend*dvlen/(ns-1);
			x(s1:s2)= x(s1:s2)+shiftv;
		else					% no points after s2
			dv	= y(:,s1)-y(:,s1-1);	% displacement vector
			dvlen	= vdist(dv,0);		
			shiftv	= -(0:ns-1)*extend*dvlen/(ns-1);
			x(s1:s2)= x(s1:s2)+shiftv;
		end
end
