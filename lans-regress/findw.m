%	findw		- Compute weights for llr 
%
%	[nx,icnx,w,ix]	= findw(x,cx,para)
%
%	_____OUTPUTS____________________________________________________________
%	nx	normalized/centered x's,			(row vector) 
%	icnx	new center index w.r.t. nx			(scalar)
%	w	row weight vector w.r.t. nx			(row vector)
%	ix	indices for original x's			(row vector)
%		(to be used for y's)
%	_____INPUTS_____________________________________________________________
%	x	all independent scalars				(row vector)
%	cx	index of center					(scalar)
%	para	parameter string				(string)
%		-clos		{0,1}
%		-span		[0,1]
%		-kernel		{0,1,2}
%		-smoopara	order
%
%	_____SEE ALSO___________________________________________________________
%	llr	wlsqr	lwavg
%
%	(C) 1998.04.20 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[nx,icnx,w,ix]	= findw(x,cx,para)

%----- get parameters
clos	= paraget('-clos',para);
span	= paraget('-span',para);
kernel	= paraget('-kernel',para);
order	= paraget('-smoopara',para);

n		= length(x);
minpoints	= order+3;		% minimum no. of points required
sp		= max(min(minpoints,n),round(span*n));

[ix,nx,hi,icnx]	= knn1(x,cx,sp,clos);

if kernel==1
	cont	= abs(nx)/hi;
	w	= (1-cont.^3).^3;
end
