%	lwavg		- Local Weighted Average (w.r.t. abscissae)
%
%	function [ny]	= lwavg(x,y,xwant,para)
%
%	_____OUTPUT_____________________________________________________________
%	ny	new y corresponding to x(xwant) 		(col vectors)
%
%	_____INPUT______________________________________________________________
%	x	independent scalars				(row vector)
%	y	dependent vector				(col vectors)
%		('smoothed' in one go all dimensions)
%	xwant	indices of x whose llr value is desired		(row vector)
%	para	see lanspara.m paraget.m			(string)
%		-kernel
%
%	(C) 1998.2.24 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[ny]	= lwavg(x,y,xwant,para)

[d n]	= size(y);

if nargin<4
	para	= [];
end

%----- get parameters
kernel	= paraget('-kernel',para);

%----- 1: Symmetric Cubic Kernel -----------------------------------------------
if (kernel==1)
	for i	= 1:length(xwant)
		xi		= x(xwant(i));
		[nx,icnx,w,ix]	= findw(x,xwant(i),para);

%----------	local weighted averaging (no smoothing)
		wtotal	= sum(w);
		nw	= ones(d,1)*w/wtotal;
		ny(:,i)	= sum((nw.*y(:,ix))')';
	end	% i
end	%if
%----- end Symmetric Cubic Kernel ----------------------------------------------
return

figure(2)
scatline([x(1,:);y(1,:);x(1,:);y(2,:)],[x(1,:);ny(1,:);x(1,:);ny(2,:)]);
axis normal
drawnow;
figure(1)
