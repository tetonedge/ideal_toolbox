%	llr		- Local Linear Regression
%
%	[ny]	= llr(x,y,xwant,para)
%
%	_____OUTPUTS____________________________________________________________
%	ny	new y corresponding to x(xwant)			(col vectors)
%
%	_____INPUTS_____________________________________________________________
%	x	independent scalars				(row vector)
%	y	dependent vector				(col vectors)
%		(independently smoothed in each row/dim)
%	xwant	indices of x whose llr value is desired		(row vector)
%	para	see lanspara.m paraget.m			(string)
%		-kernel -smoopara
%
%	_____SEE ALSO___________________________________________________________
%	wlsqr
%
%	(C) 1998.4.20 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[ny]	= llr(x,y,xwant,para)

[d n]	= size(y);
nwant	= length(xwant);

if nargin<4
	para	= []
end

%----- get parameters
kernel	= paraget('-kernel',para);
order	= paraget('-smoopara',para);

%----- 1: Symmetric Cubic Kernel -----------------------------------------------
if (kernel==1)
	for i	= 1:nwant
		xi		= x(xwant(i));
		[nx,icnx,w,ix]	= findw(x,xwant(i),para);

		%----- local polynomial regression (smoothing)
		for dim=1:d
			ny(dim,i)	= wlsqr(nx,y(dim,ix),w,icnx,order);
		end
	end	% i
end	%if
%----- end Symmetric Cubic Kernel ----------------------------------------------
