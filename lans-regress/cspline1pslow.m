%	cspline1pslow	- Periodic cubic smoothing spline 1-D (unaccelerated)
%
%	[fx]	= cspline1pslow(x,y,p,g,w)
%
%	_____OUTPUT_____________________________________________________________
%	fx	smoothed y corresponding to f(x)		(col vector)
%	x	sorted x					(col vector)
%	y	sorted y					(col vector)
%
%	_____INPUT______________________________________________________________
%	x	independent scalars				(col vector)
%	y	dependent vector				(col vector)
%	p	smoothing parameter				(scalar)
%		[0,1] : 1 implies point interpolation
%	g	gap length between x(n) and x(1)		(scalar)
%	w	weight						(col vector)	
%		1	normal weight
%		f	positive real
%
%	_____NOTES______________________________________________________________
%	- At least 4 data points needed!
%	- If weight w is not specified, equal weightage is assumed
%	- x MUST BE DISTINCT!!!!
%	- Faster than accelerated method for n<400 on a Pentium 120
%
%	_____SEE ALSO___________________________________________________________
%	cspline1p	cspline1
%	
%	(C) 1998.09.17 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[fx]	= cspline1pslow(x,y,p,g,w)

[n d]	= size(y);
if n<4
	error('At least 4 points are needed for cubic spline interpolation');
end
if nargin<5
	w	= ones(n,1);			% initially all equal weights
end
sig	= 1./w;					% more weight => less variation

%-----	Precompute common terms
h0	= g;
h	= diff(x);		% 1 ... n-1
hn	= g;

hi0	= 1/h0;
hi	= 1./h;
hin	= 1/hn;

a	= sig(1:n-1).*hi(1:n-1);
a(n)	= sig(n)*hin;
b	= sig(2:n).*hi(1:n-1);
b	= [sig(1)*hi0;b];
c	= sig(2:n-1).*(hi(1:n-2)+hi(2:n-1));
c	= [0;c];
c(n)	= sig(n)*(hi(n-1)+hin);

p6	= 6*(1-p);

%-----	shortcut Qty = Qt*y
Qty1	= [-3*hi(1)-2/g 3*hi(1)+hi(2) -hi(2) 2/g]*y([1 2 3 n]);
Qtyn	= [hin hi(n-1) -(hi(n-1)+hin)]*y([1 n-1 n]);
Qty	= [Qty1;diff(diff(y).*hi);Qtyn];


%====================	compute A
band15	= (a(3:n-2).*b(3:n-2));
band24	= (-b(3:n-1).*c(3:n-1)-a(2:n-2).*c(2:n-2));
band3	= (a(1:n-2).*a(1:n-2)+b(3:n).*b(3:n)+c(2:n-1).*c(2:n-1));
%====================	END compute A
%====================	compute C
Cfac	= [-(3*hi(1)+2*hi0)*sig(1) (3*hi(1)+hi(2))*sig(2) -sig(3)*hi(2) 0 2*sig(n)*hi0;sig(1)*hin 0 0 sig(n-1)*hi(n-1) -sig(n)*(hi(n-1)+hin)];
C	= Cfac*Cfac';
%====================	END compute C

%========================================	for n<9 use matrix inversion
	A	= diag(band3)+diag(band24,-1)+diag(band24,1)+diag(band15,-2)+diag(band15,2);
	Af	= [zeros(n,1) [zeros(1,n-2); A ;zeros(1,n-2)] zeros(n,1)];
	B	= zeros(n,n);
	B(2,1)	= -a(1)*sig(1)*(3*hi(1)+2*hi0)-c(2)*sig(2)*(3*hi(1)+hi(2))-b(3)*b(3);
	B(3,1)	= a(2)*sig(2)*(3*hi(1)+hi(2))+b(3)*c(3);
	B(4,1)	= -a(3)*b(3);
	B(n-1,1)= 2*a(n)*b(n);
	B(2,n)	= a(1)*b(1);
	B(n-2,n)= a(n-1)*b(n-1);
	B(n-1,n)= -a(n-1)*c(n-1)-b(n)*c(n);

	Cf	= zeros(n,n);
	Cf(1,1)	= C(1,1);
	Cf(n,n)	= C(2,2);
	Cf(1,n)	= C(1,2);
	Cf(n,1)	= C(1,2);
	QtD2Q	= Af + B + B' + Cf;

	%====================	R(n,n)	optimized 1998 Sep 01
	dleft	= diag([h(2:n-2)],-1);
	dcent	= diag(2*(h(2:n-1)+h(1:n-2)));
	dright	= diag([h(2:n-2)],1);
	R	= dleft+dcent+dright;
	
	R		= [zeros(n-2,1) R zeros(n-2,1)];
	R		= [zeros(1,n);R;zeros(1,n)];
	R(2,1)		= h(1);
	R(n-1,n)	= h(n-1);
	
	R(1,1)		= h(1);
	
	R(n,[n-1 n 1])	= [h(n-1) 2*(h(n-1)+hn) hn];	% wrap over formula

	%====================	Compute u
	u	= (p6*QtD2Q+p*R)\Qty;

	%====================	shortcut Qu = Q*u
	Qu		= diff([0;diff([0;u(2:n-1);0]).*hi;0]);
	Qufirst3	= [-3*hi(1)-2*hi0 hin;3*hi(1)+hi(2) 0;-hi(2) 0]*u([1 n]);
	Qulast2		= [0 hi(n-1);2*hi0 -(hi(n-1)+hin)]*u([1 n]);
	Qu([1 2 3 n-1 n])= Qu([1 2 3 n-1 n])+[Qufirst3;Qulast2];
	D2Qu	= sig.*sig.*Qu;

	%====================	Smoothed values
	fx	= y-p6*D2Qu;
