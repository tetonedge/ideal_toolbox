%	cspline1p	- Cubic smoothing spline 1-D PERIODIC
%
%	[fx,x,y]	= cspline1p(x,y,p,g,[,w])
%
%	_____OUTPUT_____________________________________________________________
%	fx	smoothed y corresponding to f(x)		(col vector)
%	x	sorted x					(col vector)
%	y	sorted y					(col vector)
%
%	_____INPUT______________________________________________________________
%	x	independent scalars				(col vector)
%	y	dependent vector				(col vector)
%	p	smoothing parameter				(scalar)
%		[0,1] : 1 implies point interpolation
%	g	gap length between x(n) and x(1)		(scalar)
%	w	weight						(col vector)	
%		1	normal weight
%		f	positive real
%
%	_____NOTES______________________________________________________________
%	for demo, call function without parameters
%
%	- At least 4 data points needed!
%	- If weight w is not specified, equal weightage is assumed
%	- x MUST BE DISTINCT!!!!
%	- Accelerated method works ONLY for n>=9!!!!!!!!
%		- It is assumed in the banded matrix that n>=9
%	- For 3<n<9, direct matrix inversion is used
%
%	_____SEE ALSO___________________________________________________________
%	cspline1.m
%
%	(C) 1998.09.18 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[fx,x,y]	= cspline1p(x,y,p,g,w)

if nargin>0
%--------------------	regular
[n d]	= size(y);
if n<4
	error('At least 4 points are needed for cubic spline interpolation');
end
if nargin<5
	w	= ones(n,1);			% initially all equal weights
end
sig	= 1./w;					% more weight => less variation

%-----	Precompute common terms
h0	= g;
h	= diff(x);		% 1 ... n-1
hn	= g;

hi0	= 1/h0;
hi	= 1./h;
hin	= 1/hn;

a	= sig(1:n-1).*hi(1:n-1);
a(n)	= sig(n)*hin;
b	= sig(2:n).*hi(1:n-1);
b	= [sig(1)*hi0;b];
c	= sig(2:n-1).*(hi(1:n-2)+hi(2:n-1));
c	= [0;c];
c(n)	= sig(n)*(hi(n-1)+hin);

p6	= 6*(1-p);

%-----	shortcut Qty = Qt*y
Qty1	= [-3*hi(1)-2/g 3*hi(1)+hi(2) -hi(2) 2/g]*y([1 2 3 n]);
Qtyn	= [hin hi(n-1) -(hi(n-1)+hin)]*y([1 n-1 n]);
Qty	= [Qty1;diff(diff(y).*hi);Qtyn];


%----------	shortcut QtD2Q = Qt*D2*Q
%====================	compute A
band15	= (a(3:n-2).*b(3:n-2));
band24	= (-b(3:n-1).*c(3:n-1)-a(2:n-2).*c(2:n-2));
band3	= (a(1:n-2).*a(1:n-2)+b(3:n).*b(3:n)+c(2:n-1).*c(2:n-1));
%====================	END compute A
%====================	compute C
Cfac	= [-(3*hi(1)+2*hi0)*sig(1) (3*hi(1)+hi(2))*sig(2) -sig(3)*hi(2) 0 2*sig(n)*hi0;sig(1)*hin 0 0 sig(n-1)*hi(n-1) -sig(n)*(hi(n-1)+hin)];
C	= Cfac*Cfac';
%====================	END compute C
%========================================	for n<9 MUST use matrix inversion
%========================================	n<600 inversion is FASTER
if n<600
	A	= diag(band3)+diag(band24,-1)+diag(band24,1)+diag(band15,-2)+diag(band15,2);
	Af	= [zeros(n,1) [zeros(1,n-2); A ;zeros(1,n-2)] zeros(n,1)];
	B	= zeros(n,n);
	B(2,1)	= -a(1)*sig(1)*(3*hi(1)+2*hi0)-c(2)*sig(2)*(3*hi(1)+hi(2))-b(3)*b(3);
	B(3,1)	= a(2)*sig(2)*(3*hi(1)+hi(2))+b(3)*c(3);
	B(4,1)	= -a(3)*b(3);
	B(n-1,1)= 2*a(n)*b(n);
	B(2,n)	= a(1)*b(1);
	B(n-2,n)= a(n-1)*b(n-1);
	B(n-1,n)= -a(n-1)*c(n-1)-b(n)*c(n);

	Cf	= zeros(n,n);
	Cf(1,1)	= C(1,1);
	Cf(n,n)	= C(2,2);
	Cf(1,n)	= C(1,2);
	Cf(n,1)	= C(1,2);
	QtD2Q	= Af + B + B' + Cf;

	%====================	R(n,n)	optimized 1998 Sep 01
	dleft	= diag([h(2:n-2)],-1);
	dcent	= diag(2*(h(2:n-1)+h(1:n-2)));
	dright	= diag([h(2:n-2)],1);
	R	= dleft+dcent+dright;
	
	R		= [zeros(n-2,1) R zeros(n-2,1)];
	R		= [zeros(1,n);R;zeros(1,n)];
	R(2,1)		= h(1);
	R(n-1,n)	= h(n-1);
	
	R(1,1)		= h(1);
	
	R(n,[n-1 n 1])	= [h(n-1) 2*(h(n-1)+hn) hn];	% wrap over formula

	%====================	Compute u
	u	= (p6*QtD2Q+p*R)\Qty;

	%====================	shortcut Qu = Q*u
	Qu		= diff([0;diff([0;u(2:n-1);0]).*hi;0]);
	Qufirst3	= [-3*hi(1)-2*hi0 hin;3*hi(1)+hi(2) 0;-hi(2) 0]*u([1 n]);
	Qulast2		= [0 hi(n-1);2*hi0 -(hi(n-1)+hin)]*u([1 n]);
	Qu([1 2 3 n-1 n])= Qu([1 2 3 n-1 n])+[Qufirst3;Qulast2];
	D2Qu	= sig.*sig.*Qu;

	%====================	Smoothed values
	fx	= y-p6*D2Qu;

	return
end
%========================================	END n<600 inversion is FASTER
%====================	compute B
B21	= -a(1)*sig(1)*(3*hi(1)+2*hi0)-c(2)*sig(2)*(3*hi(1)+hi(2))-b(3)*b(3);
B31	= a(2)*sig(2)*(3*hi(1)+hi(2))+b(3)*c(3);
B41	= -a(3)*b(3);
Bn_11	= 2*a(n)*b(n);
B2n	= a(1)*b(1);
Bn_2n	= a(n-1)*b(n-1);
Bn_1n	= -a(n-1)*c(n-1)-b(n)*c(n);
%====================	END compute B

%====================	form R bands
rband1	= hn;
rband2	= h(1:n-1);
rband3	= [h(1);2*( h(1:n-1) + [h(2:n-1);hn])];
rband4	= [0;h(2:n-1)];
%====================	END form R bands
%====================	Form Major Banded matrix
B	= zeros(n,6);				%5 band matrices with solution
nband15	= [B31;band15;Bn_2n];
nband24	= [B21;band24;Bn_1n];
nband3	= [C(1,1);band3;C(2,2)];
B(3:n,1)	= nband15*p6;
B(2:n,2)	= nband24*p6 + rband2*p;
B(:,3)		= nband3*p6 + rband3*p;
B(1:n-1,4)	= nband24*p6 + rband4*p;
B(1:n-2,5)	= nband15*p6;
B(:,6)		= Qty;

%====================	Form Left & Right minor rows in terms of loner windows
loner	= p6*B41;
win1	= [0 0 0 p6*Bn_11 p6*C(1,2)];
win2	= [0 0 0 0 p6*B2n];
win3	= [p6*Bn_11 0 0 0 0];
win4	= [p6*C(1,2)+p*rband1 p6*B2n 0 0 0];

%====================	Gaussian eliminate lower left and upper right loners
fac		= loner*(1/B(3,1));
B(4,[1:4 6])	= B(4,[1:4 6]) - fac*B(3,[2:6]);	% lower

fac		= loner*(1/B(2,5));
B(1,3:6)	= B(1,3:6)-fac*B(2,[2:4 6]);		% upper
win1(5)		= win1(5)-fac*win2(5);			% win1 also modified

%===================	Gaussian eliminates minor row win2
for j=n-2:-1:3
	fac	= win2(5)*(1/B(j,5));
	win2	= win2-fac*B(j,1:5);
	B(2,6)	= B(2,6)-fac*B(j,6);
	%col position of 1st win element for next step
	nstart= j-3;
	if nstart<5
		left	= B(2,nstart+1);
	else
		left	= 0;
	end
	win2	= [left win2(1:4)];	
end
B(2,2:5)	= win2(2:5);

%===================	Gaussian eliminates minor row win1
for j=n-2:-1:3
	fac	= win1(5)*(1/B(j,5));
	win1	= win1-fac*B(j,1:5);
	B(1,6)	= B(1,6)-fac*B(j,6);
	%col position of 1st win element for next step
	nstart= j-3;
	if nstart<4
		left	= B(1,nstart+2);
	else
		left	= 0;
	end
	win1	= [left win1(1:4)];	
end

j=2;
win1	= win1(2:5);
fac	= win1(4)*(1/B(j,5));
win1	= win1-fac*B(j,2:5);
B(1,6)	= B(1,6)-fac*B(j,6);
B(1,3:5)= win1(1:3);

%===================	Gaussian eliminates minor row win3
for j=3:n-2
	fac	= win3(1)*(1/B(j,1));
	win3	= win3-fac*B(j,1:5);
	B(n-1,6)= B(n-1,6)-fac*B(j,6);
	%col position of last win element for next step
	nend	= j+3;
	if nend>n-4
		right	= B(n-1,4-n+nend);
	else
		right	= 0;
	end
	win3	= [win3(2:5) right];	
end
B(n-1,1:4)	= win3(1:4);

%===================	Gaussian eliminates minor row win4
for j=3:n-2
	fac	= win4(1)*(1/B(j,1));
	win4	= win4-fac*B(j,1:5);
	B(n,6)	= B(n,6)-fac*B(j,6);
	%col position of last win element for next step
	nend	= j+3;
	if nend>n-3
		right	= B(n,3-n+nend);
	else
		right	= 0;
	end
	win4	= [win4(2:5) right];	
end

j	= n-1;
win4	= win4(1:4);
fac	= win4(1)*(1/B(j,1));
win4	= win4-fac*B(j,1:4);
B(n,6)	= B(n,6)-fac*B(j,6);
B(n,1:3)= win4(2:4);

%--------------------	Gaussian Eliminate tridiagonal resultant matrix
for j=1:n-2;
	B(j,4:6)	= (1/B(j,3))*B(j,4:6);
	B(j+1,[3 4 6])	= B(j+1,[3 4 6])-B(j+1,2)*B(j,[4:6]);
	B(j+2,[2 3 6])	= B(j+2,[2 3 6])-B(j+2,1)*B(j,[4:6]);
end
j=n-1;
	B(j,4:6)	= (1/B(j,3))*B(j,4:6);
	B(j+1,[3 4 6])	= B(j+1,[3 4 6])-B(j+1,2)*B(j,[4:6]);
j=n;
	B(j,6)		= B(j,6)/B(j,3);
j=n-1;
	B(j,6)		= B(j,6)-B(j,4)*B(j+1,6);
for j=n-2:-1:1;
	B(j,6)		= B(j,6)-B(j,[4 5])*B(j+[1 2],6);
end
%--------------------	Solution found via Gaussian elimination (no pivoting)
u	= B(:,6);

%====================	shortcut Qu = Q*u
Qu		= diff([0;diff([0;u(2:n-1);0]).*hi;0]);
Qufirst3	= [-3*hi(1)-2*hi0 hin;3*hi(1)+hi(2) 0;-hi(2) 0]*u([1 n]);
Qulast2		= [0 hi(n-1);2*hi0 -(hi(n-1)+hin)]*u([1 n]);
Qu([1 2 3 n-1 n])= Qu([1 2 3 n-1 n])+[Qufirst3;Qulast2];
D2Qu	= sig.*sig.*Qu;

%====================	Smoothed values
fx	= y-p6*D2Qu;

%--------------------	regular ends
else
%--------------------	demo
disp('running cspline1p.m in demo mode');
n	= 100;
p	= .9;
noise	= .5;
int	= 2*pi/n;
x	= 0:int:(2*pi-int);
x	= x';
y	= cos(x)+sin(3*x)+(rand(size(x))-.5)*noise;
fx	= cspline1p(x,y,p,int);		% verification
clf
plot(x,y,'.b');
hold on;
plot(x,fx,'r-');
hold on;
plot([x;x+2*pi],[fx;fx],'r-');
title('Periodic Cubic Smoothing spline');
%--------------------	demo ends
end


