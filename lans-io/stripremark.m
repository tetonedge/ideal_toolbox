%	stripremark	- Strip remarks from a line
%
%	[cdata,status]	= stripremark(rdata[,remchar])
%
%	_____OUTPUTS____________________________________________________________
%	cdata	clean data					(string)
%	status	cdata indicator					(scalar)
%		0	no change (i.e. no remark)
%		1	remarks stripped
%		2	all remarks
%		-1	empty string
%
%	_____INPUTS_____________________________________________________________
%	rdata	remarked data					(string)
%	remchar	remark anchor character				(char)
%		'%'						default
%
%	_____EXAMPLE____________________________________________________________
%	stripremark('When will Texas civilization begin?% How is the question?')
%	returns
%	['When will Texas civilization begin?',1]
%
%	_____NOTES______________________________________________________________
%	- works on a single line only, i.e. discard everything after remchar
%
%	(C) 1998.4.26 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [cdata,status]       = stripremark(rdata,remchar)

if nargin<2
	remchar	= '%';
end

if isempty(rdata),			% empty string
	cdata	= '';
	status	= -1;
	return;
end

whererem	= find(rdata==remchar);
if any(whererem)
	firstrem	= min(whererem);
	cdata		= rdata(1:firstrem-1);
	if firstrem==1
		status	= 2;		% all remarks
	else
		status	= 1;		% some remarks (stripped)
	end
else
	cdata		= rdata;
	firstrem	= 0;
	status		= 0;		% no remarks
end;
