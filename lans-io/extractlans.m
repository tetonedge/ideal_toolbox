%	extractlans	- Extract LANS API parameter from a string
%
%	[para]	= extractlans(paratype,oneline[,delimiter])
%
%	_____OUTPUTS____________________________________________________________
%	para		parameter				(vector)
%
%	_____INPUTS_____________________________________________________________
%	paratype	parameter type				(string)
%			(case insensitive)
%	oneline		one line of LANS API file		(string)
%	delimiter	char separating name and value		(char)
%			':'					default
%	
%	_____EXAMPLE____________________________________________________________
%	extractlans('type',' TYPE: STATIC ')	= 'STATIC'
%
%	_____NOTES______________________________________________________________
%	- all parameters will be processed in UPPERCASE
%
%	_____SEE ALSO___________________________________________________________
%	loadlans
%
%	(C) 1998.4.20 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [para]	= extractlans(paratype,oneline,delimiter)

if nargin<3
	delimiter	= ':';
end

wheregot	= findstr(upper(oneline),[upper(paratype) delimiter]);
if any(wheregot)
	wheredelimit	= find(oneline==delimiter);
	wei3		= length(oneline);
	tou2		= min(wheredelimit+1,wei3);
	para		= oneline(tou2:wei3);
	para		= strfilter(para,'killchar',' ');
	para		= upper(para);
else
	para	= '';
end;
