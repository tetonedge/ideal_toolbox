%	loadpara	- Load parameter file
%
%	[para]	= loadpara(fname)
%
%	_____OUTPUTS____________________________________________________________
%	para	see lanspara.m paraget.m			(string)
%
%	_____INPUTS_____________________________________________________________
%	fname	path/filename					(string)
%		
%	_____NOTES______________________________________________________________
%	- tabs in parameter file ok 
%	- comments preceeded by '%'
%	- isempty() condition added for compatibility with MATCOM 3
%
%	_____SEE ALSO___________________________________________________________
%	loadlans
%
%	(C) 1998.04.17 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [para]	= loadpara(fname)

tab	= 9;		% ASCII code for tab
spc	= 32;		% ASCII code for space
	
fid	= fopen(fname,'r');

para	= [];
oneline	= ' ';

%while (ischar(oneline)&(~isempty(oneline)))
while (~feof(fid))		% compatibility with MATCOM3
	oneline		= fgetl(fid);
	if (~isempty(oneline))
		[cdata,status]	= stripremark(oneline);
		if status<2				% not all remarks
			cdata	= strfilter(cdata,'replace',[tab ' ']);
			para	= [para spc cdata];
		end
	end
end

fclose(fid);

para	= strfilter(para,'merge',spc);			% merge spaces
