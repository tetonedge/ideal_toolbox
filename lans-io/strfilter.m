%	strfilter	- Perform various string filtering operations 
%
%	[outstr]	= strfilter(instr,filtype[,fpara])
%
%	_____OUTPUTS____________________________________________________________
%	outstr		filtered string				(string)
%
%	_____INPUTS_____________________________________________________________
%	instr	input string					(string)
%	filtype	filter name					(string)
%		merge		merge adjacent char's specified by fpara
%		killchar	remove char's specified by fpara
%		replace		replace char fpara(1) by fpara(2)		
%		insertbf	insert char before searched char
%		insertaf	insert char after searched char
%	fpara	filter parameter				(vector)
%
%	_____EXAMPLE____________________________________________________________
%	strfilter('This is a test  of typing','merge',' ')
%	This is a test of typing
%
%	strfilter('barbaric trucks roam the state of texas','killchar','te')
%	barbaric trucks roam the sta of xas
%
%	strfilter('barbarians in texas make no turn signals','replace','tT)
%	barbarians in Texas make no Turn signals
%
%	strfilter('TerminaTor','insertbf','T ')          
%	 Termina Tor
%
%	strfilter('TerminaTor','insertaf','T ')          
%	T erminaT or
%
%	_____NOTES______________________________________________________________
%	- cannot have nested if...end inside case for MATCOM 3, thus
%	  tryerr.m was created to overcome this limitation
%	- requires MATLAB 5 or higher
%
%	_____SEE ALSO___________________________________________________________
%	case switch
%
%	(C) 1999.11.15 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [outstr]	= strfilter(instr,filtype,fpara)

mask	= ones(1,length(instr));		% mask for output string

where		= findstr(instr,fpara(1));
switch lower(filtype)
	case 'merge',
		tryerr(nargin<3,'you did not specify the merge character.');
		tryerr(max(size(fpara))~=1,'merge character should have size 1.');
		mask(where)	= zeros(size(where));		
		retain		= killadj(where);
		mask(retain)	= ones(size(retain));	
		outstr		= instr(mask~=0);

	case 'killchar',
		tryerr(nargin<3,'you did not specify the killchar character.');
		removerange	= [where;where+length(fpara)-1];
		outstr		= lans_remove(removerange,instr);

	case 'replace',
		tryerr(nargin<3,'you did not specify the find/replace characters.');
		tryerr(sum(size(fpara))~=3,'find and replace characters each limited to size 1.');
		outstr		= instr;
		outstr(where)	= ones(size(where))*fpara(2);

	case 'insertbf',
		outstr		= [];
		if where(1)==1
			lseg	= '';
		else
			lseg	= instr(1:where(1)-1);
		end

		li	= length(instr);
		lw	= length(where);
		for w=1:lw
			r1	= where(w);
			if lw-w>=1
				r2	= where(w+1)-1;
			else
				r2	= li;
			end
			rseg	= instr(r1:r2);
			lseg	= [lseg fpara(2) rseg];
		end
		outstr	= lseg;

	case 'insertaf',
		outstr		= [];
		if where(1)==1
			lseg	= fpara(1);
		else
			lseg	= instr(1:where(1));
		end

		li	= length(instr);
		lw	= length(where);
		for w=1:lw
			r1	= min(where(w)+1,li);
			if lw-w>=1
				r2	= where(w+1);
			else
				r2	= li;
			end
			rseg	= instr(r1:r2);
			lseg	= [lseg fpara(2) rseg];
		end
		outstr	= lseg;

	otherwise,
		error(sprintf('%s - filter type not supported, see help.',filtype));
end
