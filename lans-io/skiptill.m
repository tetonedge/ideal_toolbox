%	skiptill	- Skip commented lines until desired phrase is detected
%
%	[value]	= skiptill(fid,phrase[,remchar])
%
%	_____OUTPUTS____________________________________________________________
%	value	value of corresponding phrase	  		(string)
%
%	_____INPUTS_____________________________________________________________
%	fid	file id						(scalar)
%	phrase	desired keyword					(string)
%		[]	skip until first non-remark line and
%			reset fid to beginning of that line
%	remchar	remark anchor character				(char)
%		'%'						default
%
%	(C) 1998.4.20 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[value]= skiptill(fid,phrase,remchar)

if nargin<3
	remchar	= '%';
end

if ~isempty(phrase)
	value	= [];
	while ~any(value)
		oneline		= fgetl(fid);
		[cdata]		= stripremark(oneline,remchar);
		value		= extractlans(phrase,cdata);
	end
else
	% skip fully commented lines until non-remark line is found
	status	= 1;
	while(status)
		pos1		= ftell(fid);
		oneline		= fgetl(fid);
		[cdata,status]	= stripremark(oneline,remchar);
		pos2		= ftell(fid);
	end

	%-----	reset fid to point before first non-remark line
	fseek(fid,-(pos2-pos1),0);
end;
