%	killadj		- Kill (merge) set of adjacent numbers into head of set
%
%	[outnum]= killadj(innum[,keep])
%
%	_____OUTPUTS____________________________________________________________
%	outnum	head replaces adjacent numbers	  		(row vector)
%
%	_____INPUTS_____________________________________________________________
%	innum	input numbers					(row vector)
%	keep	which digit to keep				(scalar)
%		0	keep header				default
%		1	keep trailer
%	_____EXAMPLE____________________________________________________________
%	killadj([1 2 3 7 8 9],0) returns [1 7]
%	killadj([1 2 3 7 8 9],1) returns [3 9]
%
%	(C) 2000.04.06 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[outnum]= killadj(innum,keep)

if nargin<2
	keep = 0;
end

mask	= ones(size(innum));
nin	= length(innum);

if keep==0
%	vdiff			= innum-[innum(1) innum(1:nin-1)];
	vdiff			= [0 diff(innum)];

	whereconsecutive	= find(vdiff==1);
else
%	vdiff			= [innum(2:nin) innum(nin)]-innum;
	vdiff			= [diff(innum) 0];
	whereconsecutive	= find(vdiff==1);
end

mask(whereconsecutive)	= zeros(size(whereconsecutive));

outnum			= innum(mask~=0);
