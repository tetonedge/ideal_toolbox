%	lans_load	- Load LANS API dataset file
%
%	[lansdata]			= lans_load(fname[,header]);
%	[in,out,nin,nout,nsamp]		= lans_load(fname[,header])
%
%	_____OUTPUTS____________________________________________________________
%	in	input data				(col vectors)
%	out	output data				(col vectors)
%	nin	# inputs				(scalar)
%	nout	# outputs				(scalar)
%	nsamp	# samples				(scalar)
%		or
%	lansdata.in
%	lansdata.out
%	lansdata.nin
%	lansdata.nout
%	lansdata.nsamp
%
%	* be reminded that in and out are col vectors,
%	i.e. each row is a dimension, and each col is a sample
%
%	_____INPUTS_____________________________________________________________
%	fname	path/filename				(string)
%		datafile.lans
%	header	anything				(any)
%		if 2nd argument is specified, ONLY header is read.
%		Useful for quickly examining the dataset.
%
%       _____EXAMPLE____________________________________________________________
%	Assuming the file 'iris.lans' is a LANS formatted file in the path, then
%		lans_load('iris','dummyargument'); returns
%			File : iris.lans
%			nin  :     4
%			nout :     1
%			nsamp:   150
% 		
%	_____NOTES______________________________________________________________
%	- suffix .lans will be appended to the fname if not specified
%	- specify 2nd input parameter to quickly examine dataset header without
%	  loading dataset.
%	- ORDER of the parameter field is VERY IMPORTANT, see LANS API sample
%	- remarks may be placed anywhere in the header (BEFORE data)
%	- cannot handle missing data; each data row must contain equal # of cols
%	- anything following a '%' in a line is ignored (remark)
%
%	_____SEE ALSO___________________________________________________________
%	lans_partition
%	
%
%	(C) 1999.11.18 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________
%	- SEQUENCE data type NOT handled

function [in,out,nin,nout,nsamp]= lans_load(fname,header)

%_____	append '.lans' if necessary
if isempty(findstr(fname,'.lans'))
	fname	= [fname '.lans'];
end

fid		= fopen(fname,'r');

datatype	= '';

%----------	read lines until keyword 'TYPE'
while ~any(datatype)
	oneline		= fgetl(fid);
	[cdata,status]	= stripremark(oneline);
	datatype	= extractlans('type',cdata);
end

switch datatype
	case 'STATIC',
		%----------	read lines until keyword 'INPUTS'
		nin	= str2num(skiptill(fid,'inputs'));

		%----------	read lines until keyword 'OUTPUTS'
		nout	= str2num(skiptill(fid,'outputs'));

		%----------	read lines until keyword 'SAMPLES'
		nsamp	= str2num(skiptill(fid,'samples'));

		%----------	read lines until non-remark line (header ends)
		skiptill(fid,[]);

		row	= nin+nout;		% # total dimensions
		col	= nsamp;			% # samples

		%__________	read data if nargin<2
		if nargin<2
		ldata	= fscanf(fid,'%f',[row,col]);

		in	= ldata(1:nin,:);
		out	= ldata(nin+1:row,:);	
		else
			in	= [];
			out	= [];
			%_____	show statistics if invoked from command line
			if nargout==0
				str	= sprintf('\nFile : %s\nnin  : %5d\nnout : %5d\nnsamp: %5d\n',fname,nin,nout,nsamp);
				disp(str);
			end
		end
		
	case 'SEQUENCE',
		%[in,out,nin,nout,nsamp]= lansloadsequence(fid);	
		% currently not supported
		error(sprintf('%s type recognized but currently unsupported in file %s',datatype,fname));

	otherwise,
		error(sprintf('%s type unrerecognized in file %s',datatype,fname));
end

fclose(fid);

if nargout==1
	lansdata.in	= in;
	lansdata.out	= out;
	lansdata.nin	= nin;
	lansdata.nout	= nout;
	lansdata.nsamp	= nsamp;
	in		= lansdata;
end
