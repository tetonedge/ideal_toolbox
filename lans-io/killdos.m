%	killdos		- Kill char 13 in DOS linefeeds comprising chars 10+13
%
%	[cdata]       = killdos(dosdata)
%
%	_____OUTPUTS____________________________________________________________
%	cdata	clean data stripped of char 13			(string)
%
%	_____INPUTS_____________________________________________________________
%	dosdata	DOS formatted files with 10+13 as linefeeds	(string)
%
%	_____SEE ALSO___________________________________________________________
%	strfilter
%	
%	(C) 1998.04.08 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [cdata]       = killdos(dosdata)

cdata	= strfilter(dosdata,'killchar',13);
