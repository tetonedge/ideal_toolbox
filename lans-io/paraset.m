%	paraset		- Set one '-para val' formatted parameter in string
%
%	[npstr]	= paraset(pname,pvalue,pstring)
%
%	_____OUTPUTS____________________________________________________________
%	npstr		new string containing updated parameter	(string)
%
%	_____INPUTS_____________________________________________________________
%	pname		dashed header name of desired parameter	(string)
%	pvalue		numeric value				(row vector)
%	pstring		string containing parameters		(string)
%
%	_____EXAMPLE____________________________________________________________
%	paraset('-maxiter',[30],'-maxiter 10 -span .1') returns
%	-maxiter 30 -span .1
%
%	paraset('-maxiter',[30],' ') returns
%
%	_____NOTES______________________________________________________________
%	'-pname pvalue' appended to pstring if pname is not in pstring
%
%	_____SEE ALSO___________________________________________________________
%	paraget
%
%	(C) 1999.04.28 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [npstr]	= paraset(pname,pvalue,pstring)

%========== preprocess
%----- append 1 space to end
pname(length(pname)+1)		= 32;
pstring(length(pstring)+1)	= 32;

%----- kill dos linefeeds, replace linefeeds by spaces &
%----- merge adjacent spaces
pname	= strfilter(pname,'killchar',13);
pname	= strfilter(pname,'replace',[10 ' ']);
pname	= strfilter(pname,'merge',' ');

pstring	= strfilter(pstring,'killchar',13);
pstring	= strfilter(pstring,'replace',[10 ' ']);
pstring	= strfilter(pstring,'merge',' ');

%----- remove first and last spaces if exist
pl	= length(pname);
if pname(1)==' '
	pname	= pname(2:pl);
       	pl	= pl-1;
end

if pname(pl)==' '
	pname	= pname(1:pl-1);
end


%----- remove first space in pstring if exist
if pstring(1)==' '
	pstring	= pstring(2:length(pstring));
end

%----- find ending spaces in pstring
spc2	= find(pstring==' ');
lp	= length(pstring);

pstart	= findstr(pstring,[pname ' ']);
if ~isempty(pstart)
	pstart	= max(pstart);			% take last one
	lowb	= lans_lub(spc2,pstart);	% space after pname
	if lowb<max(spc2)
		upb	= spc2(find(spc2==lowb)+1);	% 2nd space after pname
		npstr= [pstring(1:lowb) num2str(pvalue) pstring(upb:lp)];
	else
		error('Error in parameter string');
	end
else
	if length(pvalue)>1
		npstr	= [pstring ' ' pname ' ' pvalue];
	else
		npstr	= [pstring ' ' pname ' ' num2str(pvalue)];
	end
	npstr	= strfilter(npstr,'merge',' ');
end;
