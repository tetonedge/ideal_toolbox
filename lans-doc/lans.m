%	lans		- Display general information about LANS toolbox
%	lans(1)		- Display detail information about each LANS function
%			- dump to a file 'lans.txt' in lans or working directory
%_______________________________________________________________________________
%
%       LANS Pattern Recognition Toolbox
%
%	(MATLAB 5.0 or higher required)
%
%	(C) 2000.08.13 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu
%
%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/
%_______________________________________________________________________________
%This messaged was invoked by the 'lans' command
%
%	- Do a help on each sub-libraries for detailed help,
%	  e.g.
%		help lans-doc
%		help lans-fextract
%
%	- Some lans-functions, when invoked without any parameters, gives a demo
%	  e.g.
%	  	lans_group
%	  	lans_pps
%		lans_classifier
%
%	- Some (deprecated) functions may require the following toolboxes, which
%	  may be available from http://lans.ece.utexas.edu
%		NETLAB
%		GTM Toolbox
%______________________________________________________________________________
%Currently available sub-libraries:
function []	= lans(detail);
matlabdir	= getenv('LANSMATLAB');


if nargin==0
	listdetail=0;
	lansfname	= 'lans.s.txt';
else
	listdetail=1;
	lansfname	= 'lans.txt';
end
%clc;

lansfid	= fopen([matlabdir filesep lansfname],'w');
if lansfid==-1
	% save in current working directory
	lansfid	= fopen(lansfname,'w');
end

hstr	= help('lans');
disp(hstr);
fprintf(lansfid,'%s\n',hstr);

s	= dir(matlabdir);
for l=1:length(s)
	list	= s(l).name;
	if strcmp(list(1:min(5,length(list))),'lans-')
		fid	= fopen([matlabdir filesep list filesep '0.m'],'r');
		line	= fgetl(fid);
		fclose(fid);
		if listdetail
			dstr=help(list);
			disp(dstr);
			fprintf(lansfid,'%s\n',dstr);
		else
			tstr	= sprintf('%s', line(2:end));
			disp(tstr);
			fprintf(lansfid,'%s\n',tstr);
		end
	
		% check for 2nd level directories
		s2	= dir([matlabdir filesep list]);
		for l2=1:length(s2)
			atleast1	= 0;
			list2	= s2(l2).name;
			if strcmp(list2(1:min(5,length(list2))),'lans-')	
				fid	= fopen([matlabdir filesep list filesep list2 filesep '0.m'],'r');
				line	= fgetl(fid);
				fclose(fid);
				if listdetail
					dstr=help(list2);
					disp(dstr);
					fprintf(lansfid,'%s\n',dstr);
				else
					tstr	= sprintf('%s', line(2:end));
					disp(tstr);
					fprintf(lansfid,'%s\n',tstr);
				end
				atleast1	= 1;
			end
		end
		if atleast1
			tstr	= sprintf(' ');
			disp(tstr);
			fprintf(lansfid,'%s\n',tstr);
		end
	end
end
fclose(lansfid);
