%	lans_findML	- Compute M and L indices for simulation
%
%	[Midx,Lidx]	= lans_findML(Q,Mmax,nmfold)
%
%	_____OUTPUTS____________________________________________________________
%	Midx	Indices of M, number of components		(row vector)
%	Lidx	Corresponding L, number of bases		(row vector)
%
%	_____INPUTS_____________________________________________________________
%	Q	Dimensionality of submanifold			(scalar)
%	Mmax	Maximum # of reference vectors			(scalar)
%	nmfold	# of manifolds to create (upper bound)		(scalar)
%
%	_____NOTES______________________________________________________________
%	- currently only handles Q = 1,2
%	- minimum # of bases = 4	for all Q
%	- maximum # of bases = 100^Q
%
%	_____SEE ALSO___________________________________________________________
%	lans_exinit
%
%	(C) 1999.06.06 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[Midx,Lidx]	= lans_findML(Q,Mmax,nmfold)

Lmax	= 100^Q;
Lmin	= 4;
Lfactor	= 100;
M1min	= 4;

switch Q,
case 1,
	gap	= Mmax/nmfold;
	Midx	= lans_finduniq(ceil(max(gap,M1min):gap:Mmax));	
	Lidx	= ceil(Midx/Lfactor);
case 2,
	M1top	= floor(sqrt(Mmax));
	if M1top<nmfold
		M1idx	= M1min:M1top;	% smallest is M1min x M1min grid
	else
		if nmfold>1
			nmfold	= nmfold-1;
			gap	= (M1top-M1min)/nmfold;
			M1idx	= lans_finduniq( ceil(M1min:gap:M1top) );	
		else
			M1idx	= M1top;
		end
	end
	Midx	= M1idx.^2;
	L1idx	= round(Midx.^.25);
	Lidx	= L1idx.^2;

otherwise,
	error('Q>2 unimplemented');
end
Lidx	= lans_clip(Lidx,Lmin,Lmax);
