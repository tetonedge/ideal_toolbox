%	lans_proj2tri	- Project data onto a triangular patch in high-D space
%
%	[se,np,idx]	= lans_proj2tri(y,tri,tridx)
%
%	_____OUTPUTS____________________________________________________________
%	se	squared error from each vector y to patch	(row vector)
%	np	nearest point on manifold			(col vectors)
%	idx	index of np w.r.t. si				(col vectors)
%
%	_____INPUTS_____________________________________________________________
%	y	data						(col vectors)
%	tri	3 vector points of triangular patch ordered as	(col vectors)
%			1 2
%			3	
%		
%	tridx	manifold indices of the 3 points		(col vectors)
%
%	_____NOTES______________________________________________________________
%	- for demo, call function without parameters
%	- se is squared error for each point, NOT distance!
%	- if y not 'above' plane, than nearest grid projection is given
%
%	_____SEE ALSO___________________________________________________________
%	lans_proj2line	lans_proj2sqr	lans_psurfproj lans_ontriplane
%
%	(C) 1999.11.29 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/
function	[se,np,idx]	= lans_proj2tri(y,tri,tridx)

if nargin>0
%__________ REGULAR ____________________________________________________________
[D,N]	= size(y);
[Q,dum]	= size(tri);

%______	Initialize nearest projection to patch origin node
se	= zeros(1,N);
np	= tri(:,1)*ones(1,N);			% base tri node
idx	= tridx(:,1)*ones(1,N);			% base tri index

%	1 find y's that projects ONTO sv
[marked,yproj,coef]	= lans_ontriplane(y,tri);
yes			= find(marked==1);
no			= find(marked==0);

if ~isempty(yes)
%	2 compute perpendicular SQUAREDdistances (for yes nodes)
	se(:,yes)	= vdist2(yproj,y(:,yes));
	np(:,yes)	= yproj;

%	3 compute interpolated indices (for yes nodes)
	idx(:,yes)	= tridx*coef(:,yes);
end

%	4 compute nearest grid projections (for no nodes)
if ~isempty(no)
	[np(:,no),idx(:,no),results]	= lans_proj2line(y(:,no),tri,tridx,'-clos 1');
	se(:,no)	= results{1};
end

%__________ REGULAR ends _______________________________________________________
else
%__________ DEMO _______________________________________________________________
clf;clc;
disp('running lans_proj2tri.m in demo mode');

D	= 3;

y	= rand(D,5);
tri	= [0 0 0;1 0 0;0 1 0]';
tridx	= [0 0;1 0;0 1]';
[se,np,idx]     = lans_proj2tri(y,tri,tridx)

clf
lans_plotmd(tri(:,[2 1 3]),'r-','-hold 1');
lans_plotmd(tri(:,[2 3]),'r--','-hold 1');
lans_plotmd(y,'bo','-hold 1');
lans_plotproj(np,y,'m-');
rotate3d on;

%__________ DEMO ends __________________________________________________________
end
