%	lans_psurfgrad	- Compute q gradient vectors in data space R^D
%
%	[grad[,grad2]]	= lans_psurfgrad(psurf);
%
%	_____OUTPUTS____________________________________________________________
%	grad	1st derivative DxQxM matrix			(3matrix)
%	grad2	2nd derivative DxQxM matrix			(3matrix)
%		
%		each DxQ matrix (:,:,{1...M}) denotes
%			Q derivative vectors in the original space
%			(w.r.t. Q manifold directions in latent space)
%
%	_____INPUTS_____________________________________________________________
%	psurf	subset of psurf	with M latent/reference points	(psurf)
%
%	_____NOTES______________________________________________________________
%	- uses
%		activation function gradients for GTM computed psurf
%		weighted average for non-GTM psurf
%	- see lans_gradient for outputs that strictly caters to Q=1, i.e.
%	  grad,grad2	= DxM
%	- gradient magnitude reflects actual magnitude of slope
%	- grad computed are approx. orthogonal in data space!!!!
%	  due to the approx. orthogonality of psurf.lgrad in the first place
%	
%
%	_____SEE ALSO___________________________________________________________
%	lans_gradient
%
%	(C) 1999.04.30 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[grad,grad2]	= lans_psurfgrad(psurf);

if isfield(psurf,'lgrad')&isfield(psurf,'W')

	s2	= psurf.s*psurf.s;
	for j=1:psurf.M
		grad(:,:,j)	= psurf.W(:,1:end-1)*psurf.lgrad(:,:,j);
		if nargout==2
			grad2(:,:,j)	= psurf.W(:,1:end-1)*(-psurf.lgrad(:,:,j)/s2);	
		end
	end
else	% Q=1
	if nargout==2
		[g,g2]		= lans_gradient(psurf,'-gradient avg');		
		grad2(:,1,:)	= g2(:,1,:);
	else
		g	= lans_gradient(psurf,'-gradient avg');		
	end
	grad(:,1,:)	= g;
end
