%	lans_asmoothness- Compute smoothness of curve using change in angles!
%
%	[asmoo,fobtuse]	= lans_asmoothness(psurf);
%
%	_____OUTPUTS____________________________________________________________
%	asmoo	Single metric for assessing smoothness Qx1	(col vector)
%		average angle deviation (degrees)
%	fobtuse	fraction of angles being obtuse	Qx1		(col vector)
%		[0,1]
%		w.r.t. each manifold dimension
%		Q = # of manifold dimensions
%
%	_____INPUTS_____________________________________________________________
%	psurf	subset of psurf	with M latent/reference points	(psurf)
%
%	_____NOTES______________________________________________________________
%	- computes the average absolute angle deviation over all points and
%	manifolds using unitgradients
%	- less susceptible to singularities than lans_smoothness
%
%	_____SEE ALSO___________________________________________________________
%	lans_smoothness
%
%	(C) 2000.07.09 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/


%	Need to check why complex angles are formed

function	[asmoo,fobtuse]	= lans_asmoothness(psurf);

threshold	= 1e-10;
debug		= 0;

[D M]	= size(psurf.f);
Q	= psurf.Q;

asmoo	= zeros(Q,1);
fobtuse	= zeros(Q,1);

if isfield(psurf,'xidx')	% GTM based psurf
	grad	= lans_psurfgrad(psurf);
	xidx	= psurf.xidx;
	Md	= size(psurf.xidx);
	%_____	Q-D manifold		Q>1
	for q=1:Q
		dangle		= [];
		for m=1:Md(q)
			cidx	= shiftdim(xidx,q-1);
			theidx	= cidx(m,:);
			df	= squeeze(grad(:,q,theidx));
			dfmag	= sqrt(sum(df.*df));
			% throw away zero vectors
			keep	= find(dfmag>threshold);
			udf	= df(:,keep)./(ones(D,1)*dfmag(:,keep));
			dangle	= [dangle acos(sum(udf(:,1:end-1).*udf(:,2:end)))];
		end
		nobtuse		= find(dangle>=pi/2);
		fobtuse(q)	= length(nobtuse)/length(dangle);
		% dangle is concatenation of all M*M angles
%		asmoo(q)	= mean(dangle);
		asmoo(q)	= sum(dangle)/M;
	end
else				% others (assumed 1-D only)
%	df	= lans_gradient(psurf,'-gradient right');
	df	= diff(psurf.f')';		%equivalent to above
	dfmag	= sqrt(sum(df.*df));
	% throw away zero vectors
	keep	= find(dfmag>threshold);
	udf	= df(:,keep)./(ones(D,1)*dfmag(:,keep));
	dangle	= acos(sum(udf(:,1:end-1).*udf(:,2:end)));
%	asmoo	= mean(dangle);
%	Taking the sum is a more accurate measure roughness, indept of M
	asmoo	= sum(dangle);
	nobtuse	= find(dangle>=pi/2);
	fobtuse	= length(nobtuse)/length(dangle);

	%_____debug
	if debug
		figure(1)
		clf;
		lans_plotgrad(psurf.f(:,1:end-1),udf,'r-');
	end
end

asmoo	= real(asmoo);		% need some checking here
asmoo	= asmoo*180/pi;		% convert to degrees
