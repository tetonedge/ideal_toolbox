%	lans_sphere2idx	- convert longitude/latitude to absolute storage index
%
%	[idx]	= lans_sphere2idx(sang<,options>);
%
%	_____OUTPUTS____________________________________________________________
%	idx	storage index					(row vector)
%
%	_____INPUTS_____________________________________________________________
%	sang	[lat;long] specifies latitude and longitude	(col vectors)
%	options	-yint	latitude degree intervals		(scalar)
%		-xint	longitude degree intervals		(scalar)
%		default = 10 degrees 
%
%	_____EXAMPLE____________________________________________________________
%	lans_sphere2idx([-80;0]) gives 2
%
%	_____NOTES______________________________________________________________
%	- nodes aligned as follows
%	idx	1     2     3     4     5        ....          
%	sang1	-90   -80   -80   -80	-80		80    90
%	sang2	0     0     10    20    30              350   0
%
%	- assume ranges
%		y or latitude	-90 to 90
%		x or longitude	0 to 350
%	- only a single node exists at the North or South poles
%
%	_____SEE ALSO___________________________________________________________
%	lans_ppsinit.m
%
%	(C) 1999.07.20 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [idx]	= lans_sphere2idx(sang,options);

if nargin<2
	options	= '';
end

yint	= paraget('-yint',options,10);
ny	= 2*90/yint +1;
xint	= paraget('-xint',options,10);
nx	= 360/xint;

lat	= sang(1,:);
long	= sang(2,:);

atnorth	= (lat==90);
atsouth	= (lat==-90);
atpoles	= atnorth | atsouth;

idx	= atsouth+2*~atpoles+2*atnorth+(((lat+90)/yint)-~atsouth)*nx + (~atpoles).*long/xint;
