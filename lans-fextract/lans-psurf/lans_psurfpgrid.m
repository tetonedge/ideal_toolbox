%	lans_psurfpgrid	- Draw psurf grid manifold
%
%	[]	= lans_psurfpgrid(psurf[,ptype])
%
%	_____OUTPUTS____________________________________________________________
%
%	_____INPUTS_____________________________________________________________
%	psurf	subset of psurf	with M latent/reference points	(psurf)
%	ptype	plot type					(string)
%
%	_____NOTES______________________________________________________________
%	- works only for 2-D grids	
%	- overlays on current plot
%
%	_____SEE ALSO___________________________________________________________
%	lans_psurfinit
%
%	(C) 1999.05.15 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function		[]	= lans_psurfpgrid(psurf,ptype)

if nargin<2
	ptype	= 'g-';
end
Q	= psurf.Q;
M1	= round(psurf.M^(1/Q));

if Q~=2
	return;
end

for q=1:M1
	xrange	= psurf.xidx(q,1:M1);
	yrange	= psurf.xidx(1:M1,q);
	lans_plotmd(psurf.f(:,xrange),ptype,psurf.f(:,yrange),ptype,'-hold 1');
end

