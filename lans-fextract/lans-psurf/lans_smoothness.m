%	lans_smoothness	- Compute smoothness of curve using 2nd derivatives
%
%	[smoo,svec]	= lans_smoothness(psurf);
%
%	_____OUTPUTS____________________________________________________________
%	smoo	Single metric for assessing smoothness		(scalar)
%	svec	metric for each original dimension	 	(DxQ)
%		normalized by arc length
%
%	_____INPUTS_____________________________________________________________
%	psurf	subset of psurf	with M latent/reference points	(psurf)
%
%	_____NOTES______________________________________________________________
%	- Computes the approximate Integral of f''(x)^2 w.r.t. dx, incidentally
%	this is also the error used in cubic smoothing splines
%	- to compensate for manifold size, measure in dimension d is scaled
%	  by the length of the manifold in the original dimension
%	- assumes equal grid spacing for GTM derived psurf
%
%	_____SEE ALSO___________________________________________________________
%	lans_psurfgrad
%
%	(C) 1999.04.29 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[smoo,svec]	= lans_smoothness(psurf);

[D M]		= size(psurf.f);
Q		= psurf.Q;

%grad		= lans_psurfgrad(psurf);		%	DxQxM
if isfield(psurf,'lgrad')&isfield(psurf,'W')
	s2	= psurf.s*psurf.s;
	for j=1:psurf.M
		grad(:,:,j)	= psurf.W(:,1:end-1)*psurf.lgrad(:,:,j);
		grad2(:,:,j)	= psurf.W(:,1:end-1)*(-psurf.lgrad(:,:,j)/s2);
	end
	%_____	assumes grid spacing are equal in all manifold dimensions
	Md	= (psurf.M)^(1/Q);

	idx	= psurf.xidx;
	for q=1:Q
		for m=1:Md

		end
	end

	gdist	= sum(psurf.x(:,1)-psurf.x(:,2));
	svec	= sum(grad2.*grad2,2);
	svec	= sum(svec*gdist,3);
	smoo	= mean(svec);
else
	%_____	limited to 1-dimension only
	[dfdx,grad2]	= lans_gradient(psurf,'-gradient right');
	
	%_____	square of 2nd derivative
	f2	= grad2.*grad2;
	dx	= diff(psurf.x);

	svec	= f2(:,1:end-1).*(ones(D,1)*dx);
	svec	= sum(svec')';

	%_____	normalized by arc length
	arclength	= psurf.x(end);
	svec		= svec/arclength;
	smoo		= mean(svec);
end
