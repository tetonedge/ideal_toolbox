%	lans_pca	- Principal Component Analysis (batch) 
%
%	[pc_data,pvar,paxis]	= lans_pca(data[,para])
%
%	_____OUTPUTS____________________________________________________________
%	pc_data		projected data				(col vectors)
%			(sorted; kth row = kth pc)
%	pvar		variance in each principal direction 	(col vector)
%			= eigenvalues of covar matrix
%			(sorted in descending order)
%	paxis		principal axes (unit vector)		(col vectors)
%			(sorted according to pvar)
%
%	_____INPUTS_____________________________________________________________
%	data		unormalized data			(col vectors)
%	para		see paraget.m				(string)
%			-prec used for feature selection
%
%	_____NOTES______________________________________________________________
%	- data is neither mean nor variance normalized
%	- added guard to use eye(d) instead if data is nearly identity, this is
%	  to ensure results from matlab and matcom matches
%	- to get the unit deviation, multiply sqrt(pvar)*paxis
%
%	_____SEE ALSO___________________________________________________________
%	lans_eigsort	ipca
%
%	(C) 2000.04.04 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[pc_data,pvar,paxis]	= lans_pca(data,para)

if nargin<2
	para='';
end

prec	= paraget('-prec',para);
s	= cov(data');
r	= rank(s);

%----------	check near identity, to ensure same results for matcom and matlab
if r<length(s)
%	error('linear dependence detected in data');
	err	= 2*prec;
else
	dif	= s-eye(r);
	dif	= dif.*dif;
	err	= sum(sum(dif))/(r*r);
end
if err>prec
	[evector evalue]= lans_eigsort(s,para);
else
	[evector evalue]= lans_eigsort(eye(r),para);
end

paxis		= evector;
pc_data		= paxis'*data;
pvar		= diag(evalue);
