%	lda		- Linear Discriminant Analysis (batch)
%
%	[pldata,pvar,paxis]	= lda(ldata[,options])
%
%	_____OUTPUTS____________________________________________________________
%	pldata	projected labeled data				(col vectors)
%		(sorted; kth row = kth pc)
%
%	pvar	variance in each discriminant direction		(col vector)
%		(sorted in descending order)
%
%	ldaxis	linear discriminant axes			(col vectors)
%		(sorted according to pvar)
%
%	_____INPUTS_____________________________________________________________
%	ldata	labeled unnormalized data			(col vectors)
%	options	options string					(string)
%		-ldacrit	see ldacrit.m
%		-prec		see paraget.m
%		-lambda		see paraget.m
%
%	_____NOTES______________________________________________________________
%	- For the default criterion of 'fisher1' the transformed dimension is at
%	  most (#classes - 1)
%	- 'fisher2' uses the total scatter matrix and thus is not limited
%
%	_____SEE ALSO___________________________________________________________
%	ldacrit.m	ldaiter.m
%
%	(C) 1998.08.07 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[pldata,pvar,paxis]	= lda(ldata,options)

if nargin<2
	options='';
end
criterion	= paraget('-ldacrit',options);

[d1,n]	= size(ldata);
d	= d1-1;

[gdata,gindex,nclass]	= group(ldata);

mu	= mean(gdata(1:d,:)')';
wsum	= 0;
bsum	= 0;

for i=1:nclass
	i1	= gindex(i,2);
	i2	= gindex(i,3);
	nc 	= i2-i1+1;
	cdata	= gdata(1:d,i1:i2);
	wsum	= wsum + (nc-1)*cov(cdata');

	m	= mean(cdata')';
	bsum	= bsum + nc*(m-mu)*(m-mu)';	
end

sw	= wsum/n;isw	= inv(sw);
sb	= bsum/n;
st	= sw+sb;

switch (lower(criterion))
	case 'fisher1'
		matrix	= isw*sb;
	case 'fisher2'
		matrix	= isw*st;
	otherwise
		matrix	= ldaiter(sw,sb,options);
end

[paxis pvar]	= eigtuned(matrix,'-discard 1');

pldata		= paxis'*gdata(1:d,:);
pldata		= [pldata;gdata(d1,:)];
