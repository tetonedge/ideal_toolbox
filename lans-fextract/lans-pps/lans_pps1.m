%	lans_pps1	- 1 iteration/epoch of Probabilistic Principal Surface
%
%	[pps,Lcomp,Lavg]	= lans_pps1(pps,y,options)
%
%	_____OUTPUTS____________________________________________________________
%	pps	updated PPS					(structure)
%		see lans_ppsinit.m
%	Lcomp	updated true log likelihood			(scalar)
%	Lavg	updated expected log likelihood			(scalar)
%		
%	_____INPUTS_____________________________________________________________
%	pps	current PPS					(structure)
%	y	unormalized data in original dimension		(col vectors)
%	options							(string)
%		-algo		algorithm type			(string)
%				'gtm'	default
%				'gtm'	approximation for alpha~=1	
%				'gem'	generalized EM algorithm
%		-eta		learning rate for GEM algorithm	(scalar)
%				0.01	default
%		-regularize	regularization factor		(real+)
%				0	default
%				overides pps.gamma
%				(also specified in pps.gamma by lans_ppsinit)
%		-tol		tolerance factor for GEM	(scalar)
%				0.01	default
%
%	_____NOTES______________________________________________________________
%	- Lcomp computed w.r.t. updated PPS
%	- Lavg computed w.r.t. previous responsibility matrix R, if available
%	- if pps.R is empty, compute it here
%
%	_____SEE ALSO___________________________________________________________
%	lans_ppsgtm1 lans_ppsgem1 lans_pps
%
%	(C) 2000.04.16 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________
%	implement GEM method 

function	[pps,Lcomp,Lavg]	= lans_pps1(pps,y,options)

if isempty(pps.R)
	[pps.R,Lcomp,Lavg]      = lans_ppspost(pps,y,options);
end
algo		= paraget('-algo',options,'gtm');
switch algo
%__________	GTM approximation
case 'gtm',
	[pps,Lcomp,Lavg]	= lans_ppsgtm1(pps,y,options);

%__________	GEM algorithm	(also updates alpha)
case 'gem',
	% check for Lavg problems
	pps	= lans_ppsgem1(pps,y,options);
end
