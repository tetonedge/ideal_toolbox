%

%	(C) 2000.06.28 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	dLdW=ppsgem1(W,pps,y,Rold,regularize)

%
N		= size(y,2);
alpha		= pps.alpha;			% clampling factor
beta		= pps.beta;			% spherical variance
p		= pps.p;			% latent basis activations
D		= pps.D;			% data dimensionality
Q		= pps.Q;			% reduced dimensionality
M		= pps.M;			% # nodes
L		= pps.L;			% # latent basis

%__________	compute posterior/responsibility matrix and likelihood
%[Rold,Lavg,Lcomp]	= lans_ppspost(pps,y,options);  % M x N

%__________	iteratively update pps.W	(unoptimize, computationally)
B	= (D-alpha*Q)./(beta*(D-Q));
S	= alpha./beta;

%	compute constants
coeff1	= 1/B;
coeff2	= (S-B)./(B.*S);
for m=1:M
	Psi(:,:,m)	= pps.dpdx(:,:,m)*pps.dpdx(:,:,m)';			
end

%	iterate
p	= pps.p;

% stop when max(max(abs(dldw)))<tol, i.e. ~=0
dLdW	= regularize*N*W;
for n=1:N
	for m=1:M
		err	= y(:,n)-W*p(:,m);
%		t1	= W*Psi(:,:,m)*W';
		ot	= orth(W*pps.dpdx(:,:,m));
		t1	= ot*ot';
		term1	= (coeff1*eye(D)-coeff2*t1)*err*p(:,m)';
%		t2	= W*Psi(:,:,m);
		t2	= ot*pps.dpdx(:,:,m)';

		term2	= coeff2*err*err'*t2;
		dLdW	= dLdW + Rold(m,n)*(term1 + term2);
	end
end
