%	lans_ppspost	- Compute PPS posterior responsibility matrix/likelihood
%
%	[R,Lcomp]	= lans_ppspost(pps,y,options)
%	[R,Lcomp,Lavg]	= lans_ppspost(pps,y,options,Rold)
%
%	_____OUTPUTS____________________________________________________________
%	R	posterior responsibility matrix (M x N)		(matrix)
%		P(m|n)	= p(y_n | m)*P(m)/sum_h[p(y_n | h)*P(h)]
%			M = # components
%			N = # samples
%	Lcomp	Complete log likelihood				(scalar)
%		sum_n log{sum_m [p(y_n | m)*P(m)]}
%	Lavg	Average log likelihood				(scalar)
%		sum_m sum_n {R_mn * ln [p(y_n | m)]}
%		
%	_____INPUTS_____________________________________________________________
%	pps	PPS (see lans_ppsinit.m)			(structure)
%	y	data points					(col vectors)
%	options							(string)
%		-regularize	regularization factor		(scalar)
%				0	default
%
%	_____EXAMPLE____________________________________________________________
%
%	_____NOTES______________________________________________________________
%	- Lavg computed only if Rold is specified!
%	- based on lans_posterior
%	- regularization also considered
%	- lans_pdf responsible for checking orthonormality
%	- if 3sphere is used, then only 1st 2 set of gradients are used since
%	  local dimensionality for 3sphere is 2
%
%	_____SEE ALSO___________________________________________________________
%	lans_posterior	lans_basis	lans_pdf	lans_ppsinit
%
%	(C) 1999.11.29 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________

function	[R,Lcomp,Lavg] = lans_ppspost(pps,y,options,Rold)

if nargin<3
	options	= [];
end
regularize      = paraget('-regularize',options,0);

issphere	= strcmp(paraget('-manifold',options),'3sphere');

if issphere
	if size(y,1)>3
		Q	= pps.Q;
	else
		Q	= pps.Q-1;	
	end
else
	Q	= pps.Q;
end

lf	= lans_md2lin(pps.f);
if pps.alpha==1
	%_____	unoriented
	bas	= lans_basis('Gaussian-spherical',lf,'variance',ones(1,pps.M)./pps.beta,'prior',pps.prior);
else
	%_____	oriented
	alpha	= lans_clip(pps.alpha,eps,pps.D/pps.Q-eps); % restrict range
	bas	= lans_basis('Gaussian-clamped',lf,'variance',ones(1,pps.M)./pps.beta,'alpha',alpha,'tmanifold',pps.dfdx(:,1:Q,:),'prior',pps.prior);
end

if nargin<4
	[R,Lcomp,Lavg]	= lans_posterior(y,bas);		% M x N
else
	[R,Lcomp,Lavg]	= lans_posterior(y,bas,Rold);		% M x N
end

if regularize>0
	logpw	= .5*pps.L*pps.D*log(.5*regularize/pi) - .5*regularize*sum(sum(pps.W.*pps.W));
	Lavg	= Lavg + logpw;
	Lcomp	= Lcomp+ logpw;
end
