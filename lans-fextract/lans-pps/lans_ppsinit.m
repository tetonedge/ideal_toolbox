%	lans_ppsinit	- Probabilistic Principal Surface (PPS) initialization
%
%	[pps,Lcomp,Lavg,mse]	= lans_ppsinit(y)
%		  		  lans_ppsinit(y,options)
%		  		  lans_ppsinit(y,options,state)
%
%	_____OUTPUTS____________________________________________________________
%	pps	probabilistic principal surface (PPS)		(structure)
%		.str	Description string			(string)
%		.manifold	type				(string)
%		.f	nodes f(x) in data space		( D x gx x gy)
%				.f{1}	contains grid minus poles
%				.f{2}	optionally contains [south north center]
%								(cell)
%		.x	nodes x in latent dimension		( Q x gx x gy )
%								(cell)
%			GRID information contained therein
%			GRID scaled to lie within [-1 1];
%			controlled by constants GRIDMIN, GRIDMAX
%			for 3sphere
%				.x{1}	contains grid minus poles
%				.x{2}	optionally contains [south north center]
%					
%			UNIVERSALLY use lans_md2lin to collapse!!
%		.xdim	latent grid structure of nodes		(cell)
%		.W	Weight matrix				( D x L )
%		.sigma	full covariance at each .f		( D x D x M)
%			used only if alpha~=1
%			can be derived directly from dfdx
%				sigma	= B*eye(D)+(S-B)dfdx
%			where
%				B	= (D-alpha*Q)*sig/(D-Q)
%				S	= alpha*sig;
%
%		.alpha	global attenuation factor		(scalar)
%			0 < alpha < 1	PPS (oriented GTM)
%			    alpha = 1	GTM/SOM
%			1 < alpha < D/Q manifold aligned GTM
%		.beta	global inverse spherical variance	(scalar)
%		.gamma	regularization parameter		(scalar)
%		.prior	prior probability for each ref vector	(vector)
%		.p_cen	latent basis centers			( Q x L-1 )
%			Each basis is Q-D; there are L-1 bases
%			(last one is bias)
%		.p_sig	latent basis width (1 sigma)		( 1 x L-1 )
%			max(average nearest basis distance,unexplained variance)
%			= sqrt(p_var);
%		.p_fac	factor for p_sig			(scalar)
%			1	default
%			(factor of p_var will be squared!)
%		.p	latent basis activations w.r.t. x	( L x M )
%			(l,m) represents activation of basis l
%			function of W and p
%			function of W and p
%			for latent node m
%			- Lth term denotes constant bias activation (=1)
%			- .p is constant for fixed x, p_cen, p_sig
%		.po	activation of latent Origin		( L x 1)
%			useful for "clustering"
%		.dpdx	latent basis gradient w.r.t. x		( L x Q x M )
%			row (:,:,m) represents latent basis
%			gradient at node m where the LxQ gradient matrix
%			dp/dx	= [
%				dp_1(x)/dx_1 ... dp_1(x)/dx_Q
%
%				dp_L(x)/dx_1 ... dp_L(x)/dx_Q
%				= [dP/dx_1 ... dP/dx_Q]
%			where each row
%			dp_l(X_m)/dX = (p_cen(:,l)-X_m)'*p(l,m)/(p_sigf^2)
%			- dpdx is only approximately orthogonal!!!!
%			- resulting in an approximately orthogonal dfdx!!!
%			- computed once
%			- the Lth row is 0, corresponding to constant bias
%		.dfdx	manifold gradient w.r.t. x		(D x Q x M)
%			Gradient vector in original dimension is 
%				df/dx_q	= W*dpdx(:,q,:);
%			which is a MxDxQ matrix
%			- computed only if alpha!=1
%		.D	dimensionality of data space		(integer+)
%		.Q	dimensionality of latent manifold	(integer+)
%		.M	# manifold nodes			(integer+)
%		.L	# latent basis functions (include bias)	(integer+)
%		.R	Responsibility matrix			(M x N)
%			
%	Lcomp	true log likelihood				(scalar)
%	Lavg	expected log likelihood				(scalar)
%	mse	Mean Squared Error using projection -method	(scalar)
%
%	_____INPUTS_____________________________________________________________
%	y	data in original dimension			(col vectors)
%	options	(see lanspara.m)				(string)
%		-algo	Update algorithm			(string)
%			"gtm"	approximated GTM		default
%			"gem"	Generalized EM, updating alpha
%		-alpha	attenuation factor			(scalar)
%			specifies starting alpha if "gem" algorithm used
%			0 < . < 1	PPS (oriented GTM)
%			1		GTM/SOM
%			1 < . < D/Q	manifold-aligned GTM	default
%		-center	Include center node			(scalar)
%			0	No	(default)
%			1	Yes		
%			(only works for 3sphere manifold)
%			(create a center latent node)
%		-debug	{0,1}
%		-eta	learning rate for GEM algorithm		(scalar)
%		-init	Initialization method for 3sphere	(string)
%			data*	based on data (assumed N=M)
%			manifold manifold in 3 Principal component dirs
%			gaussian about mean with radius = SIGFAC*s.d.
%			permute	randomly select M points
%
%		-L1	max # latent bases in each dimension	(integer+)
%			(excluding bias)
%			> 3 recommended
%			(not for 3sphere)
%		-L2	optional 2nd dimension
%			L1			default
%			(excluding bias)
%			(not for 3sphere)
%		-Lratio	basis follows nodes grid ratio?		(binary)
%			0 regular		default
%			1 (Yes)	
%			setting Lratio=1 uses fewer latent bases
%			(only for 3sphere & 3hemisphere)
%		-Lalpha	clamping factor for LATENT basis	(integer)
%			useful only for '3sphere' manifold
%			0 < Lalpha < 3/2
%			1			default
%		-Lfac	latent basis width factor		(scalar)
%			1			default
%		-M1	max # manifold nodes in each dimension	(integer+)
%		-M2	optional 2nd dimension
%			=M1			default
%		-manifold					(string)
%			'1line'
%				'1infiniteline'	(unimplemented)
%			'2square'
%				'2circle'	(unimplemented)
%			'3sphere'
%			'3hemisphere'
%		-method	projection method
%			see lans_ppsproj.m
%		-repeat	whether to repeat latent nodes at poles	(binary)
%			0	no (default)	
%			1	yes
%			(valid only for 3sphere manifold)
%			(latent basis ALWAYS non-repeating)
%		-gtm	GTM Toolbox compatibility mode		(scalar)
%			0	No		default
%			1	Yes		
%				adjust p_cen, p_sig
%		-eint	elevation interval			(degrees)
%			currently applicable only for '3sphere' manifold
%			10			default
%		-rint	rotation interval			(degrees)
%			currently applicable only for
%			'3sphere' and '3hemisphere' manifold
%			10			default
%	state	random state used for initialization		(col vector)
%		[]*
%
%		* default
%
%	_____NOTES______________________________________________________________
%	for demo, call function without parameters
%	- Curse of dimensionality may produce Lavg = 0 for high-D data
%	- dfdx is NOT orthogonal but span the Q-D space. It is artificially made
%	  orthonormal in the lans_pdf.m function
%	- if D=Q, alpha = 1 except for '3sphere' manifold
%	- (M) may contain additional dimensions e.g.
%				L+1 x 10 x 5 
%	- Lalpha~=1 reduces error
%
%	- For small values of L1, Lratio=1 results in a better fit, otherwise
%	  Lratio=0 is better (more regularly spaced latent basis)
%	- 3-sphere
%		- use lans_md2lin to linearize pps.x which puts poles last
%			[pps.x{1} pps.x{2}] 
%		- single latent basis @ poles
%		- # latent bases (for same x ratio) is
%			L = L1*(L1+1) - 2(L1-1) + 1 = L1^2 - L1 + 3
%	  	  or for regularly spaced bases
%	  		L = (L1-1)*L1 + 1
%	  	- latent basis width varies with location
%		- if no orientation desired, PPS will be initialized to small
%		  random Gaussian values about the mean of y
%	- actual latent basis variance is obtained as a product of p_sig,p_fac
%	  where p_sig is fixed. To alter variance, set p_fac
%	- after computation, pps.R should be set to [] in order to save memory
%	- latent basis always NON-repeating at poles
%	- if -center 1
%		- y is appended with mean(y')'
%		- pps.f{2}(:,3) denotes center node in data space
%		- an extra latent node at latent origin is created
%		- NO need to add a latent basis at latent origin
%
%	_____SEE ALSO___________________________________________________________
%	lans_pps	lans_basis	lans_pdf
%
%	(C) 2000.02.26 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________
%	GTM compatibility implemented only for Q=1

function [pps,Lcomp,Lavg,mse]	= lans_ppsinit(y,options,state)
if nargin>0
%__________ REGULAR ____________________________________________________________
GRIDMIN	= -1;			% min value of unit grid
GRIDMAX	= 1;			% max value of unit grid
SIGFAC	= .5;

if nargin<3
	if nargin<2
		options	= [];
	end
else
	if ~isempty(state)
		if length(state>2)
			randn('state',state(1:2));% initialize random number generation
		else
			randn('state',state);
		end
		rand('state',state);		% initialize random number generation
	end
end

%__________	Get parameters
[D N]		= size(y);

debug		= paraget('-debug',options,0);
algo		= paraget('-algo',options,'gtm');
alpha		= paraget('-alpha',options,1);
center		= paraget('-center',options,0);
gamma		= paraget('-regularize',options,0);
gtm		= paraget('-gtm',options,0);
init		= paraget('-init',options,'data');
manifold	= paraget('-manifold',options);
Q		= str2num(manifold(1));
M1		= paraget('-M1',options,floor(N^(1/Q)));
M2		= paraget('-M2',options,M1);
L1		= paraget('-L1',options,round(M1/5));
L2		= paraget('-L2',options,L1);
p_fac		= paraget('-Lfac',options,1);
Lalpha		= paraget('-Lalpha',options,1);
Lratio		= paraget('-Lratio',options,0);

%__________	Initialize PPS structure
pps.str		= 'Insert remarks here';
pps.manifold	= manifold;
pps.f		= 0;
pps.x		= [];
pps.xdim	= [];
pps.W		= 0;

pps.sigma	= 0;
pps.alpha	= alpha;
pps.beta	= 1;
pps.gamma	= gamma;
pps.prior	= 1;

pps.p_cen	= 0;
pps.p_sig	= 1;
pps.p_fac	= p_fac;

pps.p		= 0;
pps.po		= 0;
pps.dpdx	= 0;
pps.dfdx	= 0;

pps.D		= D;
pps.Q		= Q;
pps.M		= M1;
pps.L		= L1;		% temp assignment, changes w.r.t. manifold
pps.R		= [];	

%__________	Check if D>Q
%if D<=Q
	%--------- different for '3sphere'
%	alpha	= 1;
%end

%__________	Initialize manifold
switch (manifold)
	case '1line'
		%__________	latent vectors x (arranged along a line) 
		pps.x{1}	= lans_scale(1:M1,GRIDMIN,GRIDMAX);
		pps.xdim	= lans_cellsize(pps.x,1);
		M		= M1;

		%__________	latent basis (also arranged along a line)
		L		= L1+1;
		p_cen		= lans_scale(1:L1,GRIDMIN,GRIDMAX);
		p_sig		= (p_cen(2)-p_cen(1))*ones(1,L1);
		if gtm==1
			p_cen	= p_cen*L1/(L1-1);	% yield GTM scaling
			p_var	= p_fac*(p_sig.^2);	% operates on variance
		else
			p_var = (p_fac*p_sig).^2;	% " on sigma
		end

	case '2square'
		%__________	latent vectors x (arranged on 2-D grid)
		x1	= lans_scale(1:M1,GRIDMIN,GRIDMAX);
		x2	= lans_scale(1:M2,GRIDMIN,GRIDMAX);
		[pps.x{1}(2,:,:) pps.x{1}(1,:,:)]	= meshgrid(x1,x2);
		pps.xdim= lans_cellsize(pps.x,1);

		M	= M1*M2;

		%__________	latent basis (not arranged)
		l1	= lans_scale(1:L1,GRIDMIN,GRIDMAX);
		l2	= lans_scale(1:L2,GRIDMIN,GRIDMAX);
		[l(2,:,:) l(1,:,:)]	= meshgrid(l1,l2);
		p_cen	= lans_md2lin(l);
		h_dist	= l(1,2,1)-l(1,1,1);
		v_dist	= l(2,1,2)-l(2,1,1);
		L	= size(p_cen,2)+1;
		p_sig	= min(h_dist,v_dist)*ones(1,L-1);
		p_var	= (p_fac*p_sig).^2;	

	case '3sphere'
	% gridx gridy follows Cartesian convention
	% (not scanning convention, i.e. gridx ~= row, gridy ~=col)
		%__________	latent vectors x (arranged on 2-D grid)
		% single vector at N/S pole to test rotational invariance
		%
		% x{1}(:,gridx,gridy) gives latent col vector @ (gridx,gridy)

		% make elevation (elist) and rotation (rlist) indices
		eint	= paraget('-eint',options,10);
		rint	= paraget('-rint',options,10);

		elist	= -90:eint:90;
		ne	= length(elist);

		rlist	= 0:rint:350;
		nr	= length(rlist);
		pps.x	= lans_sphere(elist,rlist,options);
		
		% begin obsolete
		if center
			pps.x{2}	= [pps.x{2} zeros(Q,1)];
		end
		% end obsolete

		pps.xdim= lans_cellsize(pps.x,1);
		
		%__________	latent basis (not stored topologically)
		% - latent basis can follow the latent node or regular spacing
		% - only 1 basis needed at N and S pole
		% - L1 = max # nodes in each direction (x,y)
		
		% follows latent node's ratio of # x and y nodes
		if Lratio
			fac		= ne/nr;
			if ne>nr
				L_rlist	= lans_scale(1:(1+round(L1/fac)),0,360);
				L_elist	= lans_scale(1:L1,elist(0),elist(end));
			else
				L_rlist	= lans_scale(1:(L1+1),0,360);
				L_elist	= lans_scale(1:round(L1*fac),elist(0),elist(end));
			end
		else
			L_rlist	= lans_scale(1:(L1+1),rlist(1),360);
			L_elist	= lans_scale(1:L1,elist(1),elist(end));
		end

		L_rlist	= L_rlist(1:end-1);

		[p_cen,p_sig]	= lans_sphere(L_elist,L_rlist,'-repeat 0 -linear 1');
		p_var		= (p_fac*p_sig).^2;
		L		= length(p_sig)+1;	% reset correct L

	case '3hemisphere'		% center NOT FUNCTIONAL
	% gridx gridy follows Cartesian convention
	% (not scanning convention, i.e. gridx ~= row, gridy ~=col)
		%__________	latent vectors x (arranged on 2-D grid)
		% single vector at N/S pole to test rotational invariance
		%
		% x{1}(:,gridx,gridy) gives latent col vector @ (gridx,gridy)

		% make elevation (elist) and rotation (rlist) indices
		eint	= paraget('-eint',options,10);
		rint	= paraget('-rint',options,10);

		elist	= 0:eint:90;
		ne	= length(elist);

		rlist	= 0:rint:350;
		nr	= length(rlist);
		pps.x	= lans_sphere(elist,rlist);
			% ------- begin obsolete
			if center
				pps.x{2}	= [pps.x{2} zeros(Q,1)];
			end
			% ------- end obsolete
		pps.xdim= lans_cellsize(pps.x,1);
		
		%__________	latent basis (not stored topologically)
		% - latent basis can follow the latent node or regular spacing
		% - only 1 basis needed at N and S pole
		% - L1 = max # nodes in each direction (x,y)
		
		% follows latent node's ratio of # x and y nodes
		if Lratio
			fac		= ne/nr;
			if ne>nr
				L_rlist	= lans_scale(1:round(L1/fac),rlist(0),rlist(end));
				L_elist	= lans_scale(1:L1,elist(0),elist(end));
			else
				L_rlist	= lans_scale(1:L1,rlist(0),rlist(end));
				L_elist	= lans_scale(1:round(L1*fac),elist(0),elist(end));
			end
		else
			L_rlist	= lans_scale(1:L1,rlist(1),rlist(end));
			L_elist	= lans_scale(1:L1,elist(1),elist(end));
		end

		[p_cen,p_sig]	= lans_sphere(L_elist,L_rlist,'-repeat 0 -linear 1');
		p_var		= (p_fac*p_sig).^2;
		L		= length(p_sig)+1;	% reset correct L
		%------------------------------------- svar==?
	otherwise
		error('Manifold unsupported!');
end	% case manifold

%__________	compute fixed activation and bias
if (Lalpha~=1)&(strcmp(manifold,'3sphere'))
	% specify single unit vector emanating from origin 
	for l=1:L-1
		ovec(:,1,l)	= p_cen(:,l);
	end
	bas	= lans_basis('Gaussian-clamped',p_cen,'variance',p_var,'alpha',Lalpha,'omanifold',ovec);
else
	% isotropic latent basis
	bas	= lans_basis('Gaussian-spherical',p_cen,'variance',p_var,'unormalized',1);
end

lx	= lans_md2lin(pps.x);
M	= size(lx,2);

p	= lans_pdf(lx,bas,1);
po	= lans_pdf(zeros(Q,1),bas,1);

%__________	compute initial W
if Q==3
	% assume 3sphere
	mu	= mean(y')';
	sig	= std(y')';
	switch lower(init)
	case 'data'
		if center
			ry	= [y mu];
		else
			ry	= y;
		end
		if size(ry,2)~=size(p,2)
			error(sprintf('Initialization using data Failed! # data %d  ~= # nodes %d',N,M));
		end
	case 'gaussian'
		if center	% last point is mean
			ry	= [mu*ones(1,M-1)+sig*ones(1,M-1).*randn(D,M-1) mu];
		else
			ry	= mu*ones(1,M)+sig*ones(1,M).*randn(D,M);
		end
	case 'permute'
		if N<M
			error('# nodes > # data points!');
		end
		rorder	= randperm(N);
		ry	= y(:,rorder(1:M));
	case 'manifold'
		% sphere in principal subspace (3-D)
		[pc_data,pvar,paxis]    = lans_pca(y);	
		lx	= lans_md2lin(pps.x);
		scale	= sqrt(pvar(1:3))';
		ry	= (paxis(:,1:3).*(ones(D,1)*scale))*lx+mu*ones(1,M);
	end	% switch lower(init)
elseif Q==2
	% find 1st,2nd PC (form a grid) in data spc
	x1		= lans_scale(1:M1,GRIDMIN,GRIDMAX);
	x2		= lans_scale(1:M2,GRIDMIN,GRIDMAX);
	[pcay,svar]	= lans_pcgrid(x1,x2,y);
	% map latent grid to 2-D principal subspc in data spc
	ry		= lans_md2lin(pcay);
elseif Q==1
	% find 1st PC in data spc
	[pcay,svar]	= lans_pcgrid(pps.x{1},y);
	% map latent line to 1st PC in data spc
	ry		= pcay;
end
W	= ry/p;
%_____________________________________________________________
pps.L		= L;
pps.M		= M;
pps.p		= p;
pps.po		= po;
pps.p_cen	= p_cen;
pps.p_sig	= p_sig;
pps.W		= W;

%	reference vectors in original space
lf		= W*p;
pps.f		= lans_lin2md(lf,pps.xdim);

%__________	Compute Jacobian gradient at each reference vector x __________
%at each basis p_cen,	dp/dx	= (p_cen-x)*p/(sig^2)
if (alpha~=1)|debug
	pps.dpdx	= zeros(L,Q,M);				% L x Q x M
%	[lx,xdim]	= lans_md2lin(pps.x);
	sigma		= ones(M,1)*(pps.p_sig.*pps.p_sig);	% M x L
	for q=1:Q
		muplane	= ones(M,1)*pps.p_cen(q,:);		% M x L
		xplane	= lx(q,:)'*ones(1,L-1);			% M x L
		dplane	= (muplane-xplane)./(sigma.*sigma);	% M x L
		gplane	= dplane.*p(1:end-1,:)';		% M x L
		pps.dpdx(1:L-1,q,:)	= gplane';		% L-1 x Q x M
		pps.dpdx(L,q,:)		= 0;
	end
end

%__________	initial beta (median dist between nodes in data space)
fdist2	= lans_dist(lf,'-metric Euclidean2') + eye(M)*realmax;
% mean not used here because it is skewed by the zero distances of
% duplicating pole values
%nnd2	= mean(min(fdist2));
nnd2	= median(min(fdist2));

if Q<3
	% also consider PCA eigenvalues
	pps.beta= 1/max(lans_clip(nnd2,eps,realmax),svar(Q+1));
else
	% use median distance between nodes in data space
	pps.beta= 1/lans_clip(nnd2,eps,realmax);
end

%__________	prior for reference vectors
pps.prior	= ones(1,M)/M;		% constant in general

%__________	initial manifold gradient & sigma 
if alpha~=1
	dfdx		= W*reshape(pps.dpdx,L,M*Q);
	pps.dfdx	= reshape(dfdx,D,Q,M);	% NOT orthogonal but spans R^Q
end

%__________	likelihood
[pps.R,Lcomp,Lavg]	= lans_ppspost(pps,y,options);


%__________	projection
if nargout>3
	[dum,dum,result]	= lans_ppsproj(pps,y,options);	
	mse			= mean(result{1});
end

%__________ REGULAR ends _______________________________________________________
else
%__________ DEMO _______________________________________________________________
clf;clc;
disp('running lans_ppsinit.m in demo mode');
N	= 100;          % # samples
nsig	= 0;		% noise s.d.
%nsig	= .05;		% noise s.d.

%pps_o	= '-alpha 1.1 -center 1 -init data -Lfac 2 -L1 7 -Lratio 0 -Lalpha 1 -M1 10 -manifold 3sphere -eint 30 -rint 30';
pps_o	= '-alpha 1.1 -center 1 -init manifold -Lfac 2 -L1 7 -Lratio 0 -Lalpha 1 -M1 10 -manifold 3hemisphere -eint 30 -rint 30';
%pps_o	= '-alpha 1.1 -Lfac 2 -L1 10 -Lratio 1 -Lalpha 1 -M1 10 -manifold 2square';
%pps_o	= '-alpha 1.1 -Lfac 2 -L1 10 -Lratio 1 -Lalpha 1 -M1 10 -manifold 1line';

y	= lans_sphere(0:30:90,0:30:330,'-repeat 0 -linear 1');
y	= y+nsig*randn(size(y));
%y	= rand(3,N);

%__________     Initialize PPS
[pps,Lcomp,Lavg]	= lans_ppsinit(y,pps_o)

%__________	plot results
clf;
h{1}	= lans_ppsplot(pps,'bo-',pps_o,y,'g.');
h{2}	= lans_plotmd(pps.p_cen(:,[end-1 1:end-2 end]),'rx-','-hold 1');	% latent basis

h{3}	= lans_plotmd(pps.W*pps.po,'r+','-hold 1');
%_________	plot gradients
lf	= lans_md2lin(pps.f);
lans_plotmd(zeros(3,1),'ko','-hold 1');		% center
lans_plotmd(lf(:,end),'ko','-hold 1');	% north pole
lans_plotgrad(lf(:,end),pps.dfdx(:,:,end),'b')

hh	= cat(2,h{:})
title('R^{Q} : Latent Space (Rotate with mouse)');
legend(hh,'pps.f','y','pps.p\_cen',1);
rotate3d on;

%mse	= sum(vdist2(y,yhat))
%__________ DEMO ends __________________________________________________________
end
