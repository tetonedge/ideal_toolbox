%	lans_ppsplot	- Plots PPS manifold
%
%	[h]	= lans_ppsplot(pps, ptype, options, y, ytype)
%		  lans_ppsplot(pps, ptype, options, y)
%		  lans_ppsplot(pps, ptype, options)
%		  lans_ppsplot(pps, ptype)
%		  lans_ppsplot(pps)
%
%	_____OUTPUTS____________________________________________________________
%	h	handles corresponding to pps, y, center		(col vector)
%		h(1)	= PPS
%		h(2)	= y
%		h(3)	= center
%
%	_____INPUTS_____________________________________________________________
%	pps	Probabilistic Principal Surface			(pps)
%		see lans_ppsinit.m
%	ptype	plot type					(string)
%		'g-'	Default
%	options							(string)
%		-debug	
%		-plotcenter	whether to plot center		(binary)
%				0	default
%				pc	plottype
%
%	_____NOTES______________________________________________________________
%	- plots manifold of dimensions Q = 1,2. Higher order manifolds simply
%	  plotted using its 1-D arranged (lans_md2lin) version.
%	- defaults to '-hold 1', i.e. overlays on current plot
%	- works only for pps.x as cell
%	- specify -debug if gradient is desired
%
%	_____SEE ALSO___________________________________________________________
%	lans_ppsinit
%
%	(C) 2000.02.26 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________
%	handle 3sphere/3hemisphere manifold

function		[h]	= lans_ppsplot(pps,ptype,options,y,ytype)

YTYPE	= 'm.';
PTYPE	= 'bo-';

if nargin>3
	plotdata	= 1;
	if nargin==4
		ytype	= YTYPE;
	end
else
	plotdata	= 0;
end

if nargin<3
	options	= [];
	if nargin<2
		ptype	= PTYPE;
	end
end

plotcenter	= paraget('-plotcenter',options,0);
debug	= paraget('-debug',options,0);
hold1	= paraget('-hold',options,0);

Q	= pps.Q;

if plotdata
	h(2)	= lans_plotmd(y,ytype,options);
end

f	= pps.f;
lf	= lans_md2lin(pps.f);
switch Q
	case 2,
		% go thru' grid line in each dimension	
		for q=1:pps.xdim{1}(1) 	
			lans_plotmd(f{1}(:,q,:),ptype,'-hold 1');
		end
		for q=1:pps.xdim{1}(2)	
			h(1)	= lans_plotmd(f{1}(:,:,q),ptype,'-hold 1');
		end
	case 3,
		if strcmp(pps.manifold,'3sphere')
			%_____	plot vertical grids
			for q=1:pps.xdim{1}(1)	
				h(1)	= lans_plotmd([f{2}(:,1) squeeze(f{1}(:,q,:)) f{2}(:,2)],ptype,'-hold 1');
			end
			%_____	plot horizontal grids
			if length(pps.xdim{1})==2
				for q=1:pps.xdim{1}(2)	
					h(1)	= lans_plotmd(f{1}(:,:,q),ptype,'-hold 1');
				end
			end
			%_____	plot center
			if (length(pps.x{2})==3)&(plotcenter~=0)
				h(3)	= lans_plotmd(f{2}(:,end),plotcenter,'-hold 1');
			end
		elseif strcmp(pps.manifold,'3hemisphere')
				%_____	plot vertical grids
			for q=1:pps.xdim{1}(1)	
				h(1)	= lans_plotmd([squeeze(f{1}(:,q,:)) f{2}(:,1)],ptype,'-hold 1');
			end
			%_____	plot horizontal grids
			if length(pps.xdim{1})==2
				for q=1:pps.xdim{1}(2)	
					h(1)	= lans_plotmd(f{1}(:,:,q),ptype,'-hold 1');
				end
			end
			%_____	plot center
			if (length(pps.x{2})==3)&(plotcenter~=0)
				h(3)	= lans_plotmd(f{2}(:,end),plotcenter,'-hold 1');
			end
			

		end
	otherwise,
		h(1)	= lans_plotmd(lf,ptype,'-hold 1');
end

%_____	plot gradient (normalized to unit length)
if debug
	lans_plotgrad(lf,pps.dfdx,'k-','-hold 1');
end
