%	lans_pps	- Probabilistic Principal Surface computation
%
%	[pps,Lcomp,Lavg,mse]	= lans_pps(pps,y,options)
%
%	_____OUTPUTS____________________________________________________________
%	pps	updated PPS					(structure)
%		see lans_ppsinit.m
%	Lcomp	Complete log likelihood	record			(vector)
%	Lavg	expected log likelihood	record			(vector)
%	mse	Mean Squared Error using projection -method	(vector)
%		
%	_____INPUTS_____________________________________________________________
%	pps	current PPS					(structure)
%	y %	unormalized training data in original dimension	(col vectors)
%	options ALL options					(string)
%		available experiment options (subset)
%		-iter	# iterations
%			20*
%		-k	starting iteration
%			0*
%		-log 	logfile
%			[]*
%		-path	path for logfiles
%			(curent directory)	Default
%		-times	# times results will be displayed/logged
%			0*
%		-plotdata	plot data points
%			0*
%			f	figure #
%		-ploterror	display graphical error progress
%			0*	no
%			f	figure #
%		-plotpps	plot PPS manifold (only for Q<=3)
%			0*	no
%			f	figure #
%		-showtext 	display text output
%			{0*,1}	
%		-tol	stop if Lavg varies less than -tol fraction in -win iter
%			[0,1]
%			0.01*
%		-win	window size to check for early termination
%			5*
%
%		* default
%		see lans_ppsinit,lans_pps1 for algorithm specific options
%
%	_____EXAMPLE____________________________________________________________
%	pps = lans_pps(pps,y,'-manifold 1line -L1 2 -M1 10 -iter 20 -times 20
%	-showtext 1 -ploterror 1 -plotdata 2 -plotpps 2');
%
%	_____NOTES______________________________________________________________
%	for demo, call function without parameters - pps must first be
%	initialized with lans_ppsinit - remove pps.R field to conserve memory
%	pps=rmfield(pps,'R');
%	- selection of orientation gradient (alpha clamped) performed by
%	  lans_ppspost.m
%	- training stopped if average absolute fractional change within last
%	  -win iters is less then -tol
%	  is l
%
%	_____SEE ALSO___________________________________________________________
%	lans_ppsinit lans_pps1 lans_ppsplot
%
%	(C) 2000.02.26 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________
%	- train/validate/test	not implemented
%	- MSE plot

function	[pps,Lcomp,Lavg,mse]	= lans_pps(pps,y,options)

if nargin>0
%__________ REGULAR
%____________________________________________________________
LCOMP_PLOT	= 'bo-'; LAVG_PLOT	= 'kd-'; MSE_PLOT	= 'k*-';
DATA_PLOT	= 'm.'; PPS_PLOT	= 'b-';

center		= paraget('-center',options,0);

k		= paraget('-k',options,0);		% starting iter.
K		= paraget('-iter',options,20);		% # iters.
logfile		= paraget('-log',options,[]);		% logfile name
logpath		= paraget('-path',options,pwd);		% logfile path
plotdata	= paraget('-plotdata',options,0);	% plot data points
ploterror	= paraget('-ploterror',options,0);	% plot error progress
plotpps		= paraget('-plotpps',options,0);	% plot PPS manifold
showtext	= paraget('-showtext',options,0);	% show text
s		= paraget('-times',options,0);		% # times to show
tol		= paraget('-tol',options,.01);		% tolerance
win		= paraget('-win',options,5);		% stopping window

showint		= floor(K/(s+(s==0)))+((s==0)|(s>K));	% interval to show

for ki=k+(1:K)						% iter. index
	kl	= ki-k;					% local iter. index
	[pps,Lcomp(kl),Lavg(kl)]= lans_pps1(pps,y,options);
	[py,px,result]		= lans_ppsproj(pps,y,options);
	if strcmp(paraget('-proj',options,'nn'),'prob')
		mse(kl)	= 0;	
	else
		mse(kl)	= mean(result{1});
	end
	if ~mod(kl,showint)
		% 0 common log info shared by logfile,showtext,showgraph
		lstr	= sprintf('[%3d] Lcomp = %4.2f Lavg = %4.2f MSE = %0.3f',ki,Lcomp(kl),Lavg(kl),mse(kl));	
		% 1 log
		if (~isempty(logfile))&showtext
			lans_log(lstr,logfile,logpath,'a');	
		end
		% 2 showtext
		if showtext
			d	= datestr(now);
			disp([d(end-7:end-3) lstr]);	
		end
		% 3 ploterror
		if ploterror
			% Lcomp & Lavg
			okl	= max(1,kl-1);		% previous local iter.
			oki	= max(0,ki-1);		% previous iter.
			if Lcomp(kl)<Lcomp(okl),compplot='ro-'; else compplot=LCOMP_PLOT;end
			if Lavg(kl)<Lavg(okl),avgplot='ro-'; else avgplot=LAVG_PLOT;end
			if mse(kl)>mse(okl),mseplot='ro-'; else mseplot=MSE_PLOT;end
			% Likelihood
			figure(ploterror);
			subplot(1,2,1);
			hold on;
			if showint==1
				phand	= plot([oki ki],[Lcomp(okl) Lcomp(kl)],compplot,[oki ki],[Lavg(okl) Lavg(kl)],avgplot); 
			else
				phand	= plot(ki,Lcomp(kl),compplot(1:2),ki,Lavg(kl),avgplot(1:2)); 
				
			end
			title(lstr);
			legend(phand,'Lcomp','Lavg',4);

			% Projection Error (MSE)
			subplot(1,2,2);
			hold on;
			if showint==1
				phand	= plot([oki ki],[mse(okl) mse(kl)],mseplot(1:(2+showint==1)));
			else
				phand	= plot(ki,mse(kl),mseplot(1:2));
			end
			title(lstr);


			% label
		end

		% 4 plotpps and/or plotdata
		if plotpps
			if plotdata
				figure(plotpps);
				lans_ppsplot(pps,PPS_PLOT,options,y,DATA_PLOT);
				title(lstr);
			else
				figure(plotpps);
				clf;
				lans_ppsplot(pps,PPS_PLOT,options);
				title(lstr);
			end
		end

		% 5 plotdata only
		if plotdata&(~plotpps)
			figure(plotdata);
			lans_plotmd(y,DATA_PLOT);
		end
		drawnow
	end	% mod

	if kl>win
		dLavg	= diff(Lavg(kl-win:kl))/(Lavg(kl-win:kl-1));
		dLavg	= mean(100*abs(dLavg));
		if dLavg<tol
			if showtext
				lstr=sprintf('Early exiting @ iter %d instead of %d\n',kl,K);	
				disp(lstr);
			end
			return;
		end
	end
end

%__________ REGULAR ends _______________________________________________________
else
%__________ DEMO _______________________________________________________________
clf;clc;
disp('running lans_pps.m in demo mode');
dbtype lans_pps.m 157:182

N	= 50;

randn('seed',1);
pps_o	= '-manifold 1line -alpha 1.1 -debug 1 -L1 3 -M1 10 -regularize .0';

pps_o	= '-manifold 2square -alpha .5 -debug 0 -L1 2 -M1 5 -regularize .0';

pps_o	= '-manifold 3sphere -alpha 1 -center 0 -debug 0 -L1 5 -Lfac 2 -Lratio 0 -regularize 1e-4 -eint 30 -rint 60';

pps_o	= '-manifold 3hemisphere -alpha 1 -center 0 -debug 0 -L1 5 -Lfac 2 -Lratio 0 -regularize 1e-4 -eint 30 -rint 30';

iter_o	= '-init manifold -iter 10 -method nn -times 10 -showtext 1 -ploterror 0 -plotdata 1 -plotpps 1';

data_o	= '-linear 1 -repeat 0';
options	= [pps_o ' ' iter_o];

if strcmp(paraget('-manifold',options),'3sphere')
	eint	= paraget('-eint',options);
	rint	= paraget('-rint',options);
	y	= lans_sphere([],[],[data_o ' ' pps_o]);
elseif strcmp(paraget('-manifold',options),'3hemisphere')
	eint	= paraget('-eint',options);
	rint	= paraget('-rint',options);
	y	= lans_sphere(0:eint:90,0:rint:350,'-linear 1');
elseif strcmp(paraget('-manifold',options),'2square')
	y	= lans_sphere(0:10:90,0:10:350,data_o);
elseif strcmp(paraget('-manifold',options),'1line')
	y	= lans_sphere(0,0:10:350,data_o);
end
y	= y + .1*randn(size(y));

pps	= lans_ppsinit(y,options);
lans_ppsplot(pps,'r-','',y,'m.')
pps	= lans_pps(pps,y,options);
%__________ DEMO ends __________________________________________________________
end
