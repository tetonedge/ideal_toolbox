%	lans_psurf2mix	- Translate key psurf structures to NETLAB's mix structure
%
%	[mix]	= lans_psurf2mix(psurf[,mix])
%
%	_____OUTPUTS____________________________________________________________
%	mix	NETLAB's mixture				(structure)
%		.type		type name			(string)
%		.nin		dimensionality of data		(scalar)
%		.ncentres	# components in mixture		(scalar)
%		.nparams	# free parameters		(scalar)
%		.covar_type	covariance type			(string)
%				'spherical'	.covars = row vector
%				'diag'		.covars = row vectors 
%				'full'		.covars = 3-D array
%						last index = component #
%		.priors		prior component probability	(row vector)
%		.centres	mean of components		(row vectors)
%		.covars		covariance of components	(2/3-D array)
%
%	_____INPUTS_____________________________________________________________
%	psurf	prinicipal curve structure			(structure)
%		.x	knot indices in lower dimension		(col vectors)
%		.f	knot values in original dimension	(col vectors)
%		.covars	covariance of data about .f		(2/3-D array)
%		.covartype	covariance type			(string)
%				'spherical'	.covars = row vector
%				'diagonal'	.covars = col vectors 
%				'full'		.covars = 3-D array
%						last index = component #
%		.priors	prior probability of each knot		(row vector)
%		.M	# of knots
%		.D	dimensionality of space
%	mix	NETLAB's mixture				(structure)
%
%	_____NOTES______________________________________________________________
%	- a new mix structure will be initialized if unspecified
%
%	_____SEE ALSO___________________________________________________________
%	gmm
%
%	(C) 1999.04.28 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[mix]	= lans_psurf2mix(psurf,mix)

if nargin<1
	clc;
	help lans_psurf2mix;
	break;
end

if strcmp(psurf.covartype,'diagonal')
	covartype	= 'diag';
else
	covartype	= psurf.covartype;
end

if nargin<2
	mix	= gmm(psurf.D,psurf.M,covartype);
end

%mix.nin	= psurf.D;
%mix.ncentres	= psurf.M;
%mix.covar_type	= covartype;

mix.priors	= psurf.priors;
mix.centres	= psurf.f';
if strcmp(psurf.covartype,'diagonal')
	mix.covars	= psurf.covars';
else
	mix.covars	= psurf.covars;
end
