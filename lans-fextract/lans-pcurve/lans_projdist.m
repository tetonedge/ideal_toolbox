%	lans_projdist	- Find [mse,dist] from data to nearest projection
%	
%	[mse,mdist,pdist]	= lans_projdist(data,psurf[,para])
%
%	_____OUTPUTS____________________________________________________________
%	mse	mean squared error(dist) of points to psurf.f	(scalar)
%	mdist	mean distance of points to psurf.f		(scalar)
%	pdist	distance vector of points to psurf.f		(row vector)
%		squared error = squared distance
%
%	_____INPUTS_____________________________________________________________
%	data	data points					(col vectors)
%	psurf	current principal surface			(structure)
%		see lans_psurf.m
%	para	see lanspara.m paraget.m			(string)
%
%	_____SEE ALSO___________________________________________________________
%	lans_project lans_psurf
%
%	(C) 1999.02.11 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [mse,mdist,pdist]	= lans_projdist(data,psurf,para)

if nargin<4
	para	= [];
	if nargin<1
		clc;
		help lans_projdist;
		break;
	end
end
[prsurf,prdata,mse,se]	= lans_project(data,psurf,para);
pdist	= sqrt(se);
mdist	= mean(pdist);
