%	lans_1speed	- Make a principal curve unit speed
%
%	[k] = lans_1speed(f)
%
%	_____OUTPUT_____________________________________________________________
%	k	unit speed knots				(row vector)
%
%	_____INPUT______________________________________________________________
%	f	principal curve					(col vectors) 
%	
%	_____SEE ALSO___________________________________________________________
%
%	_____NOTES______________________________________________________________
%	- Compute knots that reflect arc length of curve, i.e. unit-speed
%	- It is ASSUMED that f is sorted!
%
%	(C) 1999.02.11 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [k] = lans_1speed(f)

if nargin<1
	clc;
	help lans_1speed;
	break;
end

[d n]	= size(f);
fdiff	= f(:,2:n)-f(:,1:n-1);
fdist	= vdist(fdiff);

k(1)	= 0;
for i=2:n,
	k(i)	= k(i-1)+fdist(i-1);
end
