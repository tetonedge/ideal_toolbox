%	lans_porder	- reorder principal surface centers
%
%	[opsurf]	= lans_porder(psurf,order[,para])
%
%	_____OUTPUTS____________________________________________________________
%	opsurf	reordered principal curve structure		(structure)
%
%	_____INPUTS_____________________________________________________________
%	psurf	principal curve structure			(structure)
%	order	order vector/matrix				(matrix)
%	para	see lanspara.m					(string)	
%
%	_____NOTES______________________________________________________________
%	- used in conjunction with EM algorithm of lans_expect
%	- ONLY works on 1-D surfaces (currently)
%
%	_____SEE ALSO___________________________________________________________
%	lans_project	lans_expect
%
%	(C) 1999.04.08 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/


%	_____TO DO______________________________________________________________
%	- 2-d psurf initialization


function [opsurf]	= lans_porder(psurf,order,para)

if nargin<3
	para	= [];
end

algo	= paraget('-algo',para);

%----------	preset unordered items
opsurf		= psurf;

opsurf.x	= psurf.x(order);
opsurf.f	= psurf.f(:,order);

if algo==3
	switch opsurf.covartype
		case 'spherical'
		opsurf.covars	= psurf.covars(order);
		case 'diagonal'
		opsurf.covars	= psurf.covars(:,order);
		case 'full'
		opsurf.covars	= psurf.covars(:,:,order);
	end
	opsurf.priors	= psurf.priors(order);
end
