%	lans_fx		- Compute linearly interpolated principal curve from knots 
%
%	[nf]	= lans_fx(x,f[,k])
%
%	_____OUTPUT_____________________________________________________________
%	nf	new f corresponding to x			(col vectors)
%
%	_____INPUT______________________________________________________________
%	x	knot values of new pcurve			(row vector)
%	f	current principal curve				(col vectors)
%	k	knot values corresponding to f			(row vector)
%
%	_____NOTES______________________________________________________________
%	- 
%
%	_____SEE ALSO___________________________________________________________
%
%	(C) 1999.02.11 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[nf]	= lans_fx(x,f,k)

if nargin<3
	if nargin<1
		clc;
		help lans_fx;
		break
	end
	k	= lans_1speed(f);
end

M	= length(k);

for n=1:length(x)
	if x(n)>=k(M)
		nf(:,n)	= f(:,M);
	elseif x(n)<=k(1)
		nf(:,n)	= f(:,1);
	else
		k1	= find(k==lans_glb(k,x(n)));
		k2	= find(k==lans_lub(k,x(n)));
		if (k2-k1)~=1
			nf(:,n)	= f(:,k1+1);	
		else
			fvec	= f(:,k2) - f(:,k1);
			f1vec	= fvec/vdist(fvec);
			nf(:,n)	= f(:,k1)+(x(n)-k(k1))*f1vec;
		end
	end
end
