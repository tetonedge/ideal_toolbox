%	ldacrit		- Linear Discriminant Analysis criterion evaluator
%
%	[cval]	= ldacrit(w,sw,sb,[,options])
%
%	_____OUTPUTS____________________________________________________________
%	cval	Criterion value					(scalar)
%
%	_____INPUTS_____________________________________________________________
%	w	current principal axes				(col vectors)
%	sw	within class scatter				(matrix)
%	sb	between class scatter				(matrix)
%	options
%		-ldacrit					(string)
%			fisher1*	Fisher's LDA
%			fisher2		Fisher's LDA using total scatter
%			kuiyu1		Feature Subset Selection of fisher1
%			kuiyu2		Feature Subset Selection of fisher2
%
%			*=default
%		-lambda	1					(integer)
%			strength of constraint
%	
%
%	_____EXAMPLE____________________________________________________________
%
%
%	_____NOTES______________________________________________________________
%	
%
%	_____SEE ALSO___________________________________________________________
%
%	(C) 1998.08.07 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[cval]	= ldacrit(w,sw,sb,options)

if nargin<4
	options='';
end

criterion	= paraget('-ldacrit',options);
lambda		= paraget('-lambda',options);

m1	= (w'*sw*w);
m2	= (w'*sb*w);

switch (lower(criterion))
	case 'fisher1'
		cval	= trace(inv(m1)*m2);
	case 'fisher2'
		cval	= trace(inv(m1)*(sw+sb));
	case 'kuiyu1'
		cval	= trace(inv(m1)*m2)-lambda*trace(w'*w-eye(rank(w)));
	case 'kuiyu2'
		cval	= trace(inv(m1)*(sw+sb))-lambda*trace(w'*w-eye(rank(w)));
end
	

