%	ldaiter		- Linear Discriminant Analysis iterator for Feature Selection
%
%	[dmatrix]	= ldaiter(sw,sb[,options])
%
%	_____OUTPUTS____________________________________________________________
%	dmatrix	Criterion value					(scalar)
%
%	_____INPUTS_____________________________________________________________
%	sw	within class scatter				(matrix)
%	sb	between class scatter				(matrix)
%	options
%		-ldacrit					(string)
%			fisher1*	Fisher's LDA
%			fisher2		Fisher's LDA using total scatter
%			kuiyu1		Feature Subset Selection of fisher1
%			kuiyu2		Feature Subset Selection of fisher2
%
%			*=default
%		-lambda	1					(integer)
%			strength of constraint
%		-mtol	change in weight matrix
%	
%	_____EXAMPLE____________________________________________________________
%
%
%	_____NOTES______________________________________________________________
%	
%
%	_____SEE ALSO___________________________________________________________
%	lda.m	ldacrit.m
%
%	(C) 1998.08.07 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[dmatrix]	= ldaiter(sw,sb,options)

if nargin<3
	options='';
end

criterion	= paraget('-ldacrit',options);
lambda		= paraget('-lambda',options);
mtol		= paraget('-mtol',options);

switch (lower(criterion))
	case 'kuiyu1'
		m1	= sw;
	case 'kuiyu2'
		m1	= sw+sb;
end


%----------	initialize S
matrix	= m1*(sb);
[v,d]	= eigtuned(matrix,'-discard 0');
S	= v;

tol	= 10*mtol;


lambda	= 1;
count	= 0;

isw	= inv(sw);

while (tol>mtol)&(count<100)
	count	= count + 1;
	dmatrix	= isw*(m1-lambda*S*S'*sw);
	[v,d]	= eigtuned(dmatrix,'-discard 0');

	tol	= norm(S-v);

	S	= v;
	options	= paraset('-lambda',lambda,options);
	lambda	= lambda*1.1;
	% increase lambda
	%

	if rem(count,10)==0
		cval	= ldacrit(S,sw,sb,options);
		[cval,lambda]
		S
	end
end

