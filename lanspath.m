%	----------------------------------------
%       LANS Pattern Recognition Toolbox
%
%       (C) 2000.08.12 Kui-yu Chang
%	
%	http://lans.ece.utexas.edu/~kuiyu
%	----------------------------------------
%	lanspath.m
%
%	This is the path adder for the LANS matlab toolbox, to be invoked by
%	your startup.m script in your home directory or the matlab root
%	directory.
%
%	FOR USERS outside LANS
%	----------------------
%	Set both LANSROOT and LANSMATLAB to the directory containing the toolbox
%	e.g.	LANSROOT	~/lans
%		LANSMATLAB	~/lans
%
%	For USERS within LANS
%	---------------------
%	The following 2 environment variables must be set
%		LANSROOT	Root LANS directory, e.g.
%				/home/lans
%		LANSMATLAB	directory containing this toolbox, e.g.
%				/home/lans/toolbox/lans
%
%	e.g. if you installed the LANS toolbox in /home/lans
%
%       UNIX:		put in your shell (.cshrc, .bashrc) file:
%				setenv LANSROOT /home/lans
%				setenv LANSMATLAB $LANSROOT/toolbox/lans
%
%       Win/DOS:	append autoexec.bat with:
%				set LANSROOT=%HOMEPART%home\lans
%				set LANSMATLAB=%LANSROOT%\toolbox\lans
%
%	WinNT/2K:	use computer properties to set:
%				LANSROOT	%HOMEPART%home\lans
%				LANSMATLAB	%LANSROOT%\toolbox\lans

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

disp('Running lanspath.m ...');

%----------	Get LANS base directories from environment
lansroot		= getenv('LANSROOT');
if isempty(lansroot)
	disp('LANSROOT environment variable NOT set, please set it');
	help lanspath;
	exit;
end

lansmatlab		= getenv('LANSMATLAB');
if isempty(lansmatlab)
	disp('LANSMATLAB environment variable NOT set, please set it');
	help lanspath;
	exit;
end

s	= filesep;

%__________	LANS Documentation directory
kind	= 'lans-doc';
path(path,[lansmatlab s kind]);

%__________	LANS data directory
%	Download data from http://lans.ece.utexas.edu/~lans/bench
lansdata	= [lansroot s 'data'];
if ~isempty(length(dir(lansdata)));
	path(path,[lansdata s 'regress']);	
	path(path,[lansdata s 'classify']);
end

%__________	LANS Function subdirectories
path(path,[lansmatlab s 'lans-data']);
path(path,[lansmatlab s 'lans-common']);
path(path,[lansmatlab s 'lans-fextract']);
path(path,[lansmatlab s 'lans-fextract' s 'lans-pcurve']);
path(path,[lansmatlab s 'lans-fextract' s 'lans-psurf']);
path(path,[lansmatlab s 'lans-fextract' s 'lans-pps']);
path(path,[lansmatlab s 'lans-gendata']);
path(path,[lansmatlab s 'lans-gui']);
path(path,[lansmatlab s 'lans-io']);

path(path,[lansmatlab s 'lans-nn']);
path(path,[lansmatlab s 'lans-patrec']);
path(path,[lansmatlab s 'lans-patrec' s 'lans-pose']);
path(path,[lansmatlab s 'lans-regress']);
path(path,[lansmatlab s 'lans-stats']);
path(path,[lansmatlab s 'lans-sim']);
path(path,[lansmatlab s 'lans-visual']);
path(path,[lansmatlab s 'lans-old']);
path(path,[lansmatlab s 'lans-old' s 'lans-matcom']);

%__________	Misc. functions 
path(path,[lansmatlab s '../misc']);
