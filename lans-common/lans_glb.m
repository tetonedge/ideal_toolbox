%	lans_glb	- Find greatest lower bound (infimum) in a 1D sequence
%
%	[glb,glbidx]	= lans_glb(sdata,value[,sindex])
%
%	_____OUTPUTS____________________________________________________________
%	glb	biggest element in sdata smaller than value	(scalar)
%	glbidx	index of this element in sdata			(scalar)
%
%	_____INPUTS_____________________________________________________________
%	sdata	1D ascendingly sorted vector			(col/row vector)
%	value	value to be bounded				(scalar)
%	sindex	start index in sdata				(scalar)
%
%	_____EXAMPLE____________________________________________________________
%	lans_glb([2 5 7 8],6)	returns 5
%	lans_glb([1 2 3 4],2.9)	returns 2
%
%	_____SEE ALSO___________________________________________________________
%	lans_lub
%
%	(C) 1999.05.24 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [glb,glbidx]	= lans_glb(sdata,value,sindex)

n	= length(sdata);
if nargin<3
	sindex=n;
end

[glb glbidx]	= lans_lub(fliplr(-sdata),-value,n-sindex+1);
glb		= -glb;
glbidx		= n-glbidx+1;
