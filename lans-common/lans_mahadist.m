%	lans_mahadist	- Mahalanobis distance from a set of vector means
%
%	[mdist]	= lans_mahadist(mu,vdata[,sigmai])
%
%	_____OUTPUTS____________________________________________________________
%	mdist	Mahalanobis distance matrix (MxN)		(matrix)
%		of each vector in vdata to each vector mean
%
%	_____INPUTS_____________________________________________________________
%	mu	list of M vector centers (Mxd)			(col vectors)
%	vdata	set of N data vectors (Nxd)			(col vectors)
%	sigmai	inverse covariance matrix (dxdxM)		(3-D matrix)
%
%	_____EXAMPLE____________________________________________________________
%
%
%	_____NOTES______________________________________________________________
%	- leave out sigmai or set sigmai = 1 for standard Euclidean distances
%	- each mdist(i,{1:N}) is computed as
%		dv'*sigmai(:,:,i)*dv where
%		dv{1:N}	= mu(:,i) - vdata(:,{1:N})
%	- each row i of mdist represents the Mahalanobis distance of vdata to
%	  that particular vector mean i with inverse covariances sigmai(:,:,i)
%
%	_____SEE ALSO___________________________________________________________
%	lans_dist.m
%
%	(C) 2000.07.09 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[mdist]	= lans_mahadist(mu,vdata,sigmai)

if nargin<3
	sigmai=1;	
end
[d d M1]	= size(sigmai);

if (d==1)&(sigmai==1)
	mdist	= lans_dist(mu,vdata);
	return;
end

[d1,M]	= size(mu);
[d2,N]	= size(vdata);
if (d1~=d2)
	error('vector data dimensionality differs from that of mean vectors');
end;
if (d~=d1)
	error('Dimensionality of sigmai does not match that of mean vectors');
end;
if (M1~=M)
	error('Number of sigmai differs from noumber of mean vectors');
end

mdist	= zeros(M,N);

if d==1
	for i=1:M
		vdiff		= vset(:,i)*ones(1,N)-vdata;
		mdist(i,:)	= vdiff.*vdiff*sigmai(i);
	end
else
	for i=1:M
		vdiff		= mu(:,i)*ones(1,N)-vdata;	
		tdist		= vdiff'*sigmai(:,:,i);
		mdist(i,:)	= sum(tdist'.*vdiff);
	end
end
