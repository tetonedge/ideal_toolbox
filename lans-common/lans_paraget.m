%	lans_paraget	- Get parameter value from string 
%
%	[pval,option]	= lans_paraget(pname,option,defval)
%
%	_____OUTPUTS____________________________________________________________
%	pval	parameter value					(anything)
%	option	adjusted option					(string)
%
%	_____INPUTS_____________________________________________________________
%	pname	parameter name e.g. '-iter'			(string)
%	option	string containing list of parameters		(string)
%	defval	default values if option does not contain pname	(anything)
%
%	* default
%
%	_____EXAMPLE____________________________________________________________
%	lans_paraget('-iter','-iter 10 -D 5') returns 10
%	lans_paraget('-name','','chicken') returns 'chicken'
%
%	_____NOTES______________________________________________________________
%	for demo, call function without parameters
%	more efficient (faster) than paraget due to a simpler algo for
%	classifying numeric/string values
%	
%	_____SEE ALSO___________________________________________________________
%	paraget
%
%	(C) 2001.12.30	Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/
%	_____TO DO______________________________________________________________

function	[pval,option]	= lans_paraget(pname,option,defval)

if nargin>0
%__________ REGULAR ____________________________________________________________
%PREC	= 1e-7;
FRAC	= 0.33;	% percentage of non-numeric ASCII characters
plen	= length(pname);
if isempty(option)&nargin==3
	pval	= defval;
	if ischar(pval)
		option	= [pname ' ' pval];		% string
	else
		option	= [pname ' ' num2str(pval)];	%numeric
	end
	return;
else
	if nargin<3
		defval	= 0;
	end
	w	= findstr(pname,option);	% parameter name

	if ~isempty(w)
		s	= findstr(' ',option);		% spaces
		thes	= s(find(s>w));
		p1	= killadj(thes,1);
		p2	= killadj(thes,0);
		if length(p1)==1
			p2(2)	= length(option)+1;
		end
		pstr	= option(p1(1)+1:p2(2)-1);
		plen	= p2(2)-p1(1)-1;

		if sum(pstr>57)/plen<FRAC
			pval	= str2num(pstr);	% numeric
		else
			pval	= pstr;			% string
		end
	else						% unspecified
		pval	= defval;
		% append to option
		if ischar(pval)
			option	= [option ' ' pname ' ' pval];		% string
		else
			option	= [option ' ' pname ' ' num2str(pval)];	%numeric
		end
		return;
	end
end

%__________ REGULAR ends _______________________________________________________
else
%__________ DEMO _______________________________________________________________
clf;clc;
disp('running ****.m in demo mode');

%__________ DEMO ends __________________________________________________________
end
