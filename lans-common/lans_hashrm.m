%	lans_hashrm	- Delete/remove an entry from hash table
%
%	[hash,obj,idx]	= lans_hashrm(key,hash,<idx>)
%
%	_____OUTPUTS____________________________________________________________
%	hash	updated hash table				(structure)
%		see lans_hashnew.m
%	obj	value of deleted item object			(anything)
%	idx	original index of removed item			(scalar)
%
%	_____INPUTS_____________________________________________________________
%	key	hash table key					(string,vector)
%	hash	current hash table				(structure)
%	idx	optional index for speed-up			(scalar)
%		downside: corresponding .point{} NOT deleted
%
%	_____EXAMPLE____________________________________________________________
%	hash	= lans_hashrm('star wars',hash);
%
%	_____NOTES______________________________________________________________
%	for demo, call function without parameters
%	- does not allow adding of the same keys
%	- if specified key not found, then no action is performed
%
%	* default
%
%	_____SEE ALSO___________________________________________________________
%	lans_hashnew	lans_hashadd	lans_hashget
%
%	(C) 2000.07.30	Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/
%	_____TO DO______________________________________________________________

function	[hash,obj]		= lans_hashrm(key,hash,idx)

% 1 check if given key is contained in hash table
if nargin<3
	[obj,idx,kcode]      = lans_hashget(key,hash);
else
	[obj,idx,kcode]      = lans_hashget(key,hash,idx);
end

if isempty(idx)
	% not there anyway
	return;
else	% void that key entry
	hash.key{idx}	= [];
	hash.obj{idx}	= [];
	hash.odirty	= [idx hash.odirty];
	route		= hash.route{idx};
	hash.route{idx}	= [];
	for p=1:length(route)
		pi	= route(p);
		if p==length(route)
			% remove idx	
			hash.point{pi}(kcode(p),1)=0;
		else
			% clean up pointer
			hash.point{pi}(kcode(p),2)=0;
		end
		if nnz(hash.point{pi})==0
			% mark .point{pi} as dirty
			hash.pdirty	= [pi hash.pdirty];	
		end
	end
end
