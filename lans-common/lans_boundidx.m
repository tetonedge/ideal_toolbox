%	lans_boundidx	- Find upper, lower, and nearest bound indices
%
%	[bounds]	= lans_boundidx(value,sx)
%
%	_____OUTPUTS____________________________________________________________
%	bounds	upper bound index and lower bound index		(vector)
%		bounds(1)	= upperbound index
%		bounds(2)	= lowerbound index
%		bounds(3)	= index of closest element
%
%	_____INPUTS_____________________________________________________________
%	value	value to consider				(scalar)
%	sx	sorted data 					(row vector)
%
%	_____NOTES______________________________________________________________
%	- find the indices of values in sx upper and lower bounding the value
%	  and the index of the closest sx item to value
%
%	_____SEE ALSO___________________________________________________________
%	lans_glb	lans_lub
%
%	(C) 1998.11.18 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[dx]	= lans_boundidx(value,sx)

ubound	= lans_lub(sx,value);
lbound	= lans_glb(sx,value);

if isempty(lbound)
	lbound	= sx(1);
end
if isempty(ubound)
	ubound	= sx(length(sx));
end

dx(1)	= min(find(sx==ubound));
dx(2)	= max(find(sx==lbound));

bv	= sx(dx);
d2bv	= abs(bv-value);
dx(3)	= min(dx(find(d2bv==min(d2bv))));
