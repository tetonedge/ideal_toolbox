%	lans_findcell	- Find position of a cell in a cell
%
%	[pos]	= lans_findcell(target,bigcell)
%
%	_____OUTPUTS____________________________________________________________
%	pos	position of target in bigcell			(integer matrix)
%
%	_____INPUTS_____________________________________________________________
%	target	input cell					(string cell)
%	bigcell	cell						(string cell)
%
%	_____EXAMPLE____________________________________________________________
%
%
%	_____NOTES______________________________________________________________
%	- only works for string target
%	- returns 0 if string not found
%
%	_____SEE ALSO___________________________________________________________
%	
%
%	(C) 1999.07.02 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [pos]	= lans_findcell(target,bigcell)

s	= size(bigcell);

pos	= 0;
for i=1:prod(s)
	if strcmp(char(target),char(bigcell(i)))
		pos	= i;
		break;
	end
end
