%	lans_matinv	- Matrix inverse lemma
%
%	[theinv]	= lans_matinv(A,v[,invertA])
%
%	_____OUTPUTS____________________________________________________________
%	theinv	inv(A+v*v')					(matrix)
%
%	_____INPUTS_____________________________________________________________
%	A	dxd matrix					(matrix)
%	v	dx1 col vector					(col vector)
%	invertA	whether to invert A (default 1)		 	(binary)
%		useful for iteration
%
%	_____EXAMPLE____________________________________________________________
%	b=rand(1);v=rand(2,3);
%	dum = inv(b*eye(2) + v(:,1)*v(:,1)' + v(:,2)*v(:,2)' + v(:,3)*v(:,3)')
%
%	is approximated by
%
%	dum	= eye(2)/b;
%	for i=1:3;dum=lans_matinv(dum,v(:,i),0);end
%
%
%	_____NOTES______________________________________________________________
%	For iteration, A must be inverted (manually) and then
%	the rest set to invertA=0
%	
%	(C) 1999.04.20 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[minv]	= lans_matinv(A,v,invertA)

if nargin<3
	invertA	= 1;
end

if invertA
	Ai	= inv(A);
else
	Ai	= A;
end

num	= (Ai*v)*(v'*Ai);
den	= 1+v'*Ai*v;

minv	= Ai-num/den; 

