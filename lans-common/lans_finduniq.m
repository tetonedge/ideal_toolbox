%	lans_finduniq	- Remove CONSECUTIVE repetitions from sorted col vectors
%
%	[udata,uidx]	= lans_finduniq(sdata)
%
%	_____OUTPUT_____________________________________________________________
%	udata	unique numerical data				(col vectors)
%	uidx	STARTING indices of unique entries in sdata	(vector)
%
%	_____INPUT______________________________________________________________
%	sdata	SORTED non-unique data				(col vectors)
%
%	_____EXAMPLE____________________________________________________________
%	[udata,uidx]    = lans_finduniq([1 2 2 3 3 5])
%	returns
%		udata	= [1 2 3 5]
%		uidx	= [1 2 4 6]
%
%	_____NOTES______________________________________________________________
%	- data MUST be presorted and numerical
%
%	(C) 1999.12.03 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[udata,uidx]	= lans_finduniq(sdata)

[D N]	= size(sdata);
ddiff	= sum(sdata(:,2:N)~=sdata(:,1:N-1),1);

uidx	= [1 find(ddiff~=0)+1];
udata	= sdata(:,uidx);

if isempty(udata)
	udata	= sdata(:,1);		%	all same!!!!
end
