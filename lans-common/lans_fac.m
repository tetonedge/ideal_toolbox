%	lans_fac	- Compute matrix factorial
%
%	[factorial]	= lans_fac(n)
%
%	_____OUTPUTS____________________________________________________________
%	f	factorial					(integer matrix)
%
%	_____INPUTS_____________________________________________________________
%	n	number						(integer matrix)
%
%	_____EXAMPLE____________________________________________________________
%
%
%	_____NOTES______________________________________________________________
%	- n MUST be integer matrix
%	- use MATLAB's built-in (WAY FASTER) cumprod for 1-D factorial
%
%	_____SEE ALSO___________________________________________________________
%	cumprod
%
%	(C) 1999.08.26 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [factorial]	= lans_fac(n)

%----------	find largest number
maxno		= max(max(n));

%----------	replace negative numbers and zeros by 1
zerosi		= find(n<=0);
n(zerosi)	= ones(size(zerosi));

%----------	compute factorial 
factorial	= n;
findex		= n;

for i=maxno:-1:2
	cand		= find(findex>2);
	candidates	= findex(cand);
	findex(cand)	= candidates-1;
	factorial(cand)	= factorial(cand).*findex(cand);
end
