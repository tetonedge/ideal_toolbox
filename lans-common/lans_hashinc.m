%	lans_hashinc	- Increment an entry to hash table 
%
%	[hash,obj]	= lans_hashinc(key,inc,hash,idx)
%
%	_____OUTPUTS____________________________________________________________
%	hash	updated hash table				(structure)
%		see lans_hashnew.m
%	obj	new update value				(anything)
%
%	_____INPUTS_____________________________________________________________
%	key	hash table key					(string,vector)
%	inc	directional vector				(vector)
%	hash	current hash table				(structure)
%	idx	optional index in hashtable for speed up	(scalar)
%
%	_____EXAMPLE____________________________________________________________
%	hash	= lans_hashadd('star wars',777,hash);
%
%	_____NOTES______________________________________________________________
%	for demo, call function without parameters
%	- works only if obj is numerical
%
%	* default
%
%	_____SEE ALSO___________________________________________________________
%	lans_hashnew	lans_hashget
%
%	(C) 2000.07.30	Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/
%	_____TO DO______________________________________________________________

function	[hash,obj]	= lans_hashinc(key,inc,hash,idx)

% 1 check if given key is already contained in hash table
if nargin<4
	[obj,idx]	= lans_hashget(key,hash);
else
	obj		= hash.obj{idx};
end

if isempty(obj)
	% add key object 
	i		= length(hash.obj)+1;
	hash.key{i}	= key;
	hash.nkey(i)	= lans_word2code(key,hash.option);
	hash.obj{i}	= inc;
	obj		= inc;
else
	obj		= obj+inc;
	hash.obj{idx}	= obj;	
end
