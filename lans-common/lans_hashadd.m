%	lans_hashadd	- Add an entry to hash table 
%
%	[hash,idx]		= lans_hashadd(key,obj,hash)
%
%	_____OUTPUTS____________________________________________________________
%	hash	updated hash table				(structure)
%		see lans_hashnew.m
%	idx	index in hashtable for new entry		(scalar)
%
%	_____INPUTS_____________________________________________________________
%	key	hash table key					(string,vector)
%	obj	item object to store				(anything)
%	hash	current hash table				(structure)
%	
%	_____EXAMPLE____________________________________________________________
%	hash	= lans_hashadd('star wars',777,hash);
%
%	_____NOTES______________________________________________________________
%	for demo, call function without parameters
%	- does not allow adding of the same keys
%
%	* default
%
%	_____SEE ALSO___________________________________________________________
%	lans_hashnew	lans_hashget
%

%	(C) 2000.07.30	Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/
%	_____TO DO______________________________________________________________

function	[hash,idx]		= lans_hashadd(key,obj,hash)

% 1 check if given key is already contained in hash table
[oobj,idx,kcode]	= lans_hashget(key,hash);

if isempty(idx)		% not already present; create
	seg_no	= length(kcode);
	sp	= 1;	% segment number
	for p=1:seg_no
		route(p)	= sp;
		i(1)	= hash.point{sp}(kcode(p),1);	
		i(2)	= hash.point{sp}(kcode(p),2);	
		if p==seg_no
			% look for empty .obj
			if isempty(hash.odirty) % no space left! create new
				idx		= length(hash.obj)+1;
			else			% reuse
				idx		= hash.odirty(end);
				hash.odirty	= hash.odirty(1:end-1);
			end
			hash.obj{idx}	= obj;
			hash.key{idx}	= key;

			hash.point{sp}(kcode(p),1)	= idx;
		else	% traverse/create pointer
			sp_old	= sp;
			if i(2)~=0
				sp	= i(2);
			else
				% check if exist dirty .pointer
				if isempty(hash.pdirty)	% create new
					sp	= length(hash.point)+1;
				else			% reuse
					sp		= hash.pdirty(end);
					hash.pdirty	= hash.pdirty(1:end-1);
				end
				hash.point{sp}	= sparse(2^hash.bitseg,2);
				hash.point{sp_old}(kcode(p),2)	= sp;
			end
		end
	end
	hash.route{idx}	= route;
else			% already present; increment old object by obj
	if iscell(oobj)
		for n=1:length(obj)
			nobj{n}	= oobj{n}+obj{n};
		end
	else
		nobj	= oobj+obj;
	end
	hash.obj{idx}	= nobj;
end
