%	lans_md2lin	- Convert multi-D stored col vectors to linear array
%
%	[lx,xdim]	= lans_md2lin(x)
%
%	_____OUTPUTS____________________________________________________________
%	lx	linearly stored vectors	of DxN			(col vectors)
%	xdim	Dimensionality of x before conversion		(row vector)
%		excluding the first dimension!
%
%	_____INPUTS_____________________________________________________________
%	x	R^D vectors stored in multi-D			(multi-D)
%								(1-D cell)
%
%	_____EXAMPLE____________________________________________________________
%	x		= rand(3,5,5);
%	[lx,xdim]	= lans_md2lin(x) gives a 3x25 matrix lx and
%	xdim		= [5 5];
%
%	applying lans_lin2md(lx,xdim) recovers x iff x is not a cell
%
%	_____NOTES______________________________________________________________
%	- each R^D vector assumed to be stored at x(:,d2,d3,...)
%	- if x is a cell, then each cell is processed recursively, xdim becomes
%	  invalid
%
%	_____SEE ALSO___________________________________________________________
%	lans_lin2md
%
%	(C) 1999.11.04 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[lx,xdim]	= lans_md2lin(x)

if iscell(x)
	numcells= length(x);
	lx	= [];
	for c=1:numcells
		lx	= [lx lans_md2lin(x{c})];	
	end
	xdim	= [];
else
	xdim	= size(x);
	D	= xdim(1);
	xdim	= xdim(2:end);
	lx	= reshape(x,D,prod(xdim));
end
