%	lans_clone	- Clone matrix horizontally
%
%	[cdata]	= lans_clone(mat,x)
%
%	_____OUTPUTS____________________________________________________________
%	cdata		cloned matrix				(col vectors)
%
%	_____INPUTS_____________________________________________________________
%	mat		matrix					(col vectors)
%	x		number of times				(scalar)
%
%	_____EXAMPLE____________________________________________________________
%	lans_clone([1 2 3;4 5 6],2)=
%	1     2     3     1     2     3
%	4     5     6     4     5     6
%
%	_____SEE ALSO___________________________________________________________
%	clone1
%
%	(C) 1999.11.04 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [cdata]	= lans_clone(mat,x)

[d,n]	= size(mat);
cdata	= zeros(d,n*x);
for i=0:x-1
	chu		= i*n+1;
	zhi		= i*n+n;
	cdata(:,chu:zhi)= mat;
end
