%	lans_findidx	- Find indices of max/min values of each col
%
%	[row,ridx,uidx,ucount]	= lans_findidx(data<,oper><,options>)
%
%	_____OUTPUT_____________________________________________________________
%	row	row indices corresponding to max/min of each col (row vector)	
%		returns best k indices for k>1			(col vectors)
%	ridx	indices of rejected samples			(row vector)
%	uidx	# unique rows					(row vector)
%	ucount	# elements of each uidx				(row vector)
%
%	_____INPUT______________________________________________________________
%	data	M x N data					(matrix)
%	oper	Type of comparison				(string)
%		{max,min*}
%	options	
%		-reject	rejection threshhold
%			none if unspecified
%		-knn	K best rows
%			{1*, odd}
%
%		* default
%
%	_____EXAMPLE____________________________________________________________
%
%	_____NOTES______________________________________________________________
%	- THIS IS IMPLEMENTED in MATLAB via [k,i]	= max(xxx)
%	- in case of duplicate data, take minimum index
%	- used for K-NN classification too!
%
%	(C) 1999.12.01 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[row,ridx,uidx,ucount]	= lans_findidx(data,oper,options)

if nargin<3
	options	= [];
	if nargin<2
		oper	= 'min';
	end
end
reject	= paraget('-reject',options,[]);
k	= paraget('-knn',options,1);

[M N]	= size(data);

switch lower(oper)
	case 'min'
		if k==1
			edata		= min(data);
		else
			rlabels		= meshgrid(1:M,1:N)';
			[edata,sidx]	= sort(data);
			srows		= rlabels(sidx);
		end
		if ~isempty(reject)
			ridx	= find(edata>=reject);
		else
			ridx	= [];
		end

	case 'max'
		if k==1
			edata		= max(data);
		else
			rlabels		= meshgrid(1:M,1:N)';
			[edata,sidx]	= sort(-data);
			edata		= -edata;
			srows		= rlabels(sidx);
		end
		if ~isempty(reject)
			ridx	= find(edata<=reject);
		else
			ridx	= [];
		end
end

% handle multiple minimas per col (take lowest index one)

if k==1
	nullified	= data-ones(M,1)*edata;
	[trow,col]	= find(nullified==0);	% trow = temp. row
	[ucol,uidx]	= lans_finduniq(col');	% get rid of multiple extremas per col
	row		= trow(uidx);
else
	row		= srows(1:k,:);		% return best k indices
end

srow		= sort(row);
if nargout>2
	uidx		= lans_finduniq(srow');
	if nargout>2
		ucount		= hist(srow,uidx);
	end
end
