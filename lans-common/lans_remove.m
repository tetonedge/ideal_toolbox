%	lans_remove	- Remove an index of values from a col/row vector 
%
%	[nvector,rvector]	= lans_remove(range,vector)
%
%	_____OUTPUTS____________________________________________________________
%	nvector	remaining vector				(col vectors)
%	rvector	removed vector					(col vectors)
%
%	_____INPUTS_____________________________________________________________
%	range	indices/col vectors		(col vectors)	(row vector)
%	vector	original vector					(col vectors)
%
%	_____EXAMPLE____________________________________________________________
%	remove([1 5 8;3 6 9],'123456789')= '47'		% range 1:3,5:6,8:9
%	remove([3 7],'12345678')	= '124568'
%	remove([3 4 5 6 7],'12345678')	= '128'
%
%	_____SEE ALSO___________________________________________________________
%
%
%	(C) 1999.11.08 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [nvector,rvector]	= lans_remove(range,vector)

[d rsize]	= size(range);

[row,col]	= size(vector);

findices	= 1:col;

if d==1						%_____ plain indices given
	findices(range)	= zeros(size(range));
elseif d==2
	%_____ start,end range given in each 2-D col
	for i=1:rsize
		zrange	= range(1,i):range(2,i);
		findices(zrange) = zeros(size(zrange));
	end
end

stayi		= find(findices~=0);
removedi	= find(findices==0);
nvector		= vector(:,stayi);
rvector		= vector(:,removedi);
