%	lans_lub	- Find least upper bound (supremum) in a 1D sequence
%
%	[lub,lubidx]	= lans_lub(sdata,value[,sindex])
%
%	_____OUTPUTS____________________________________________________________
%	lub	smallest element in sdata bigger than value	(scalar)
%	lubidx	index of this element in sdata			(scalar)
%
%	_____INPUTS_____________________________________________________________
%	sdata	1D ascendingly sorted vector			(col/row vector)
%	value	value to be bounded				(scalar)
%	sindex	start index in sdata				(scalar)
%
%	_____EXAMPLE____________________________________________________________
%	lans_lub([2 5 7 8],6)	returns 7
%	lans_lub([1 2 3 4],2.9)	returns 3
%
%	_____SEE ALSO___________________________________________________________
%	lans_glb
%
%	_____SEE ALSO___________________________________________________________
%
%	(C) 1999.09.07 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [lub,lubidx]	= lans_lub(sdata,value,sindex)

if nargin<3
	sindex	= 1;
end
n		= length(sdata);
lubidx		= find(sdata(sindex:n)>value);
if ~isempty(lubidx)
	lubidx		= lubidx(1);
	lub		= sdata(lubidx);
else
	lub		= [];
	lubidx		= [];
end

% much faster (in matlab) than using while loop
