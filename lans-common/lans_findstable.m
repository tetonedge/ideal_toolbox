%	lans_findstable	- Find index when value has settled
%
%	[sidx]	= lans_findstable(sdata[,gmargin[,grace]])
%
%	_____OUTPUTS____________________________________________________________
%	sidx	stable index indicating values after this index (matrix)
%		are within margin amount of final value
%
%	_____INPUTS_____________________________________________________________
%	sdata	vector containing sequences of data		(vector)
%	gmargin	upper bound for 2nd derivative			(scalar)
%		1e-5		default
%	grace	number of indices to include in sidx		(scalar)
%		after data sequence has settled
%		0		default
%
%	_____EXAMPLE____________________________________________________________
%	lans_findstable(exp(-(1:10)),.001)	returns y
%
%	_____NOTES______________________________________________________________
%	- uses 2nd derivative info.
%	- set gmargin to 0 to locate index after which value remains constant
%	
%	(C) 1999.05.06 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[sidx]	= lans_findstable(sdata,gmargin,grace)

if nargin<3
	grace	= 0;			% no grace
	if nargin<2
		gmargin	= 1e-5;		% 0.01% default
	end
end

N	= length(sdata);
sval	= sdata(N);

%_____	compute gradients
sgrad	= gradient(sdata);		% 1st derivative
sgrad2	= abs(gradient(sgrad));		% 2nd derivative

%_____	find the settled value, if any
wsettled= find(sgrad2<=gmargin);

%_____	indexes unsettled values, i.e. variation abs(gradient/value) > gmargin
if ~isempty(wsettled)
	%_____	ensures that there are no offshoots
	dw	= diff(wsettled);
	wzero	= find(dw~=1);		% where not consecutive
	if ~isempty(wzero)		% found offshoot
		lastone	= wzero(end);	% take the last offshoot
		wsettled= wsettled((lastone+1):end);
	end
	sidx	= wsettled(1)+grace;
else
	sidx	= N;
end

%_____	ensure that length of sdata is not exceeded
sidx	= min(sidx,N);
