%	lans_lin2md	- Convert linearly stored col vectors to multi-D array
%
%	[x]	= lans_lin2md(lx,xdim)
%
%	_____OUTPUTS____________________________________________________________
%	lx	linearly stored vectors	of DxN			(col vectors)
%	xdim	Dimensionality of x before conversion		(row vector)
%		excluding the storage dimensionality (lx)	(cells of)
%
%	_____INPUTS_____________________________________________________________
%	x	R^D vectors stored in multi-D			(multi-D)
%
%	_____EXAMPLE____________________________________________________________
%	x		= rand(3,5,5);
%	[lx,xdim]	= lans_md2lin(x) gives a 3x25 matrix lx and
%	xdim		= [5 5];
%
%	lans_lin2md(lx,xdim) recovers x
%
%	_____NOTES______________________________________________________________
%	- each R^D vector assumed to be stored at x(:,d2,d3,...)
%	- size of lx is used
%	- xdim can be cells
%
%	_____SEE ALSO___________________________________________________________
%	lans_md2lin	lans_cellsize
%
%	(C) 1999.11.11 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[x]	= lans_lin2md(lx,xdim)

[D N]	= size(lx);
if iscell(xdim)
	ncell	= length(xdim);
	count	= 0;
	for c=1:ncell
		sx	= xdim{c};
		x{c}	= reshape(lx(:,count+1:count+prod(sx)),[D sx]);
		count	= count+prod(sx);
	end
else
	x	= reshape(lx,[D xdim]);
end
