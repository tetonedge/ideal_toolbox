%	lans_ininterval	- Check if a point is bounded by the interval
%
%	[verdict]	= lans_ininterval(points,interval)
%
%	_____OUTPUTS____________________________________________________________
%	verdict		1=yes,0=no				(row vector)
%
%	_____INPUTS_____________________________________________________________
%	points		points to be tested			(col vectors)
%	interval	bounds					(col vectors)
%
%	_____EXAMPLE____________________________________________________________
%	lans_ininterval([2 3 4],[2.5 4.5]) returns 
%	0 1 1
%
%	_____NOTES______________________________________________________________
%
%	(C) 1999.09.07 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[verdict]	= lans_ininterval(points,interval)

[D N]	= size(points);

verdict	= zeros(1,N);
b1	= interval(:,1);
b2	= interval(:,2);
for n=1:N
	p		= points(:,n); 	
	in1		= (sum((p>=b1)&(p<=b2))==D);
	in2		= (sum((p<=b1)&(p>=b2))==D);
	verdict(n)	= in1|in2;
end
