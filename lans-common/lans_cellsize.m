%	lans_cellsize	- Measure dimensionality of each cell component
%
%	[xdim]	= lans_cellsize(x <,skip1>)
%
%	_____OUTPUTS____________________________________________________________
%	xdim	Dimensionality of each cell component		(cell)
%
%	_____INPUTS_____________________________________________________________
%	x	multi-dimensional components stored in cell	(cell)
%	skip1	whether to skip first dimension of each cell	(binary)
%		0	default
%
%	_____EXAMPLE____________________________________________________________
%
%	_____NOTES______________________________________________________________
%
%	_____SEE ALSO___________________________________________________________
%	lans_md2lin	lans_lin2md
%
%	(C) 1999.11.11 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/


function	[xdim]	= lans_cellsize(x,skip1)

if ~iscell(x)
	error('input not a cell');
end

if nargin<2
	skip1=0;
end
C	= length(x);
for c=1:C
	thedim	= size(x{c});
	if skip1
		xdim{c}	= thedim(2:end);	
	else
		xdim{c}	= thedim;
	end
end
