%	lans_diversify	- Perturb repeating values to make it non-repetitive
%
%	[dx]	= lans_diversify(x)
%
%	_____OUTPUTS____________________________________________________________
%	dx	diversified data without repeating values	(row vector)
%
%	_____INPUTS_____________________________________________________________
%	x	sorted data possibly with repeating values	(row vector)
%		ascending or descending, smallest value >=0
%
%	_____NOTES______________________________________________________________
%	- 2 values, whose difference is less than eps, are considered equivalent
%	- a value of .01 * mindiff will be added consecutively to reoccurring
%	  values
%
%	(C) 1999.04.08 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[dx]	= lans_diversify(x)

if x(1)>x(end)
	reverse	= 1;
	x	= x(end:-1:1);
else
	reverse	= 0;
end
n	= length(x);
frac	= .01;

diffx	= diff(x);
mindist	= lans_lub(diffx,0);

where0	= find(diffx<eps);

oidx	= -1;

for i	= 1:length(where0)
	cidx	= where0(i);
	if cidx ~= oidx+1	% reset counter
		count	= frac;
	else
		count	= count+frac;
	end
	diffx(cidx)	= diffx(cidx)+count*mindist;
	oidx	= cidx;
end

dx	= [x(1) x(1:n-1)+diffx];

if reverse
	dx	= dx(end:-1:1);
end
