%	lans_replace	- Replace values in a matrix/vector
%
%	[ndata,where]	= lans_replace(oldv,newv,data)
%
%	_____OUTPUTS____________________________________________________________
%	ndata	data with replaced values			(matrix/vector)
%	where	indices of locations replaced
%
%	_____INPUTS_____________________________________________________________
%	oldv	upper bound of value to be replaced		(scalar)
%	newv	value in place of values <= oldv		(scalar)
%	data	data matrix/vector				(matrix/vector)
%
%	(C) 1998.11.15 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[ndata,where]	= lans_replace(oldv,newv,data)

where		= find(data<=oldv);
ndata		= data;
ndata(where)	= ones(size(where))*newv;
