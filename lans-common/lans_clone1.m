%	lans_clone1	- Repeat each col vectors by k times
%
%	[cdata]	= lans_clone1(data,k)
%
%	_____OUTPUTS____________________________________________________________
%	cdata	cloned data					(col vectors)
%
%	_____INPUTS_____________________________________________________________
%	data	normal						(col vectors)
%	k	number of times					(scalar)
%
%	_____EXAMPLE____________________________________________________________
%	lans_clone1([1 2 3;4 5 6],3)=
%	1     1     1     2     2     2     3     3     3
%	4     4     4     5     5     5     6     6     6
%
%	_____SEE ALSO___________________________________________________________
%	clone
%
%	(C) 1999.11.08 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/


function [cdata]	= lans_clone1(data,k)

[d,n]	= size(data);
mdata	= zeros(d*k,n);

for i=1:k
	chu	= (i-1)*d+1;
	zhi	= i*d;
	cdata(chu:zhi,:)	= data;
end

cdata	= reshape(cdata,d,k*n);
