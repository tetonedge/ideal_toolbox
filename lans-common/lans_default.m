%	lans_default	- Set default values for unspecified fields
%
%	fstruct	= lans_default(tstruct,dstruct)
%
%	_____OUTPUTS____________________________________________________________
%	fstruct	Complete structure, filled in with default	(structure)
%		values
%
%	_____INPUTS_____________________________________________________________
%	tstruct	Possibly incomplete structure			(structure)
%	dstruct	Structure with specified default values		(structure)
%
%	(C) 1999.04.27 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [fstruct]	= lans_default(tstruct,dstruct)

fname	= fieldnames(dstruct);
Nf	= length(fname);

fstruct	= tstruct;

for f=1:Nf
	if ~isfield(tstruct,fname{f})
		dvalue	= getfield(dstruct,fname{f});
		fstruct	= setfield(fstruct,fname{f},dvalue);
	end
end
