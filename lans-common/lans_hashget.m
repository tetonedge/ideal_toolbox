%	lans_hashget	- Get an entry from hash table 
%
%	[obj,idx,kcode]	= lans_hashget(key,hash,<idx>)
%
%	_____OUTPUTS____________________________________________________________
%	obj	specified object in hash table			(anything)
%	idx	numeric position of key in hash table		(scalar)
%	kcode	coded vector representation of key		(vector)
%
%	_____INPUTS_____________________________________________________________
%	key	hash table key					(string,vector)
%	hash	current hash table				(structure)
%		see lans_hashnew.m
%	idx	specified index for speed-up			(scalar)
%
%	_____EXAMPLE____________________________________________________________
%	obj	= lans_hashget('star wars',hash);
%
%	_____NOTES______________________________________________________________
%	for demo, call function without parameters
%
%	* default
%
%	_____SEE ALSO___________________________________________________________
%	lans_hashnew	lans_hashadd
%
%	(C) 2000.07.30	Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________

function	[obj,idx,kcode]	= lans_hashget(key,hash,idx)

if nargin==3
	obj	= hash.obj{idx};
	kcode	= lans_word2code(key,hash.option);	
	return;
end

obj	= [];idx	= [];	% not matched by default

% 1 translate key into code for each segment
kcode	= lans_word2code(key,hash.option);	
seg_no	= length(kcode);	% # segments

% traverse segments
sp	= 1;			% segment number
for p=1:seg_no			% check availability in each hash segment
	i(1)	= hash.point{sp}(kcode(p),1);
	i(2)	= hash.point{sp}(kcode(p),2);
	if p==seg_no		% last segment, compare key
		if i(1)~=0
			idx	= i(1);
			obj	= hash.obj{idx};
			return;
		end
	else			% traverse pointer tree if available
		if i(2)~=0
			sp	= i(2);
		else		% no route established
			return;
		end
	end
end
