%	lans_word2code	- Code a word/string into a vector
%
%	[code,option]	= lans_word2code(word,<option>)
%
%	_____OUTPUTS____________________________________________________________
%	code	encoded vector					(D-vector)
%	option	full options (set to default where applicable)	(string)
%
%	_____INPUTS_____________________________________________________________
%	word	string						(string)
%	option							(string)
%
%	* default
%
%	_____EXAMPLE____________________________________________________________
%	lans_word2code('chicken');
%
%	_____NOTES______________________________________________________________
%	for demo, call function without parameters
%	
%	_____SEE ALSO___________________________________________________________
%
%	(C) 2000.07.30	Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/
%	_____TO DO______________________________________________________________

function	[code,option]	= lans_word2code(word,option)
if nargin>0
%__________ REGULAR ____________________________________________________________
if nargin<3; option = []; end

[n_bit,option]		= lans_paraget('-bit',option,5);
[n_char,option]		= lans_paraget('-char',option,20);
[maxbit,option]		= lans_paraget('-maxbit',option,21);

wlen			= length(word);
charseg			= floor(maxbit/n_bit);			% # char/segment
n_seg			= ceil(min(n_char,wlen)/charseg);	% # segments
for s	= 1:n_seg
	sidx	= charseg*(s-1)+1;
	eidx	= min(sidx+charseg-1,wlen);
	segment	= word(sidx:eidx);
	c	= dec2bin(mod(segment+0,2^n_bit));
	wbin	= reshape(c',1,prod(size(c)));
	code(s)	= bin2dec(wbin);
end
%__________ REGULAR ends _______________________________________________________
else
%__________ DEMO _______________________________________________________________
clf;clc;
disp('running lans_word2code.m in demo mode');

%__________ DEMO ends __________________________________________________________
end
