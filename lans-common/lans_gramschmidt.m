%	lans_gramschmidt- Compute orthogonal vectors from orthonormal space
%
%	[h]	= lans_gramschmidt(svec)
%
%	_____OUTPUTS____________________________________________________________
%	h	D orthogonal vectors spanning R^D		(col vectors)
%
%	_____INPUTS_____________________________________________________________
%	svec	seed vectors (MUST BE linearly independent)	(col vectors)
%
%	_____NOTES______________________________________________________________
%	- equivalent but slower than matlab's qr function!
%	- first q cols of h contains normalized version of svec (assumed QxD)
%	- svec MUST be orthogonal, otherwise results varies!
%	- orthogonal CORRECTION is performed for the seed vectors
%
%	_____SEE ALSO___________________________________________________________
%	orth	qr	(MATLAB function)
%
%	(C) 2000.07.09 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[h]	= lans_gramschmidt(svec)

%rseed	= 1;
%rand('state',rseed);

[D,N]	= size(svec);

%_____	check for linear dependence in seed vectors
if N>=D|(rank(svec)<N)
	error('Supplied seed colvectors are linearly dependent !!');
end

%_____	performs correction if svec are not orthogonal
svec	= orth(svec);

[x,dum]	= lans_eigsort(cov(rand(D,D)));
h	= lans_unitvec(svec);
for i=N+1:D
	xi	= x(:,i-N);

	vsum	= zeros(D,1);
	for j=1:i-1
		vsum	= vsum+xi'*h(:,j)*h(:,j);
	end
	h(:,i)	= xi-vsum;
	h(:,i)	= h(:,i)/norm(h(:,i));
end
