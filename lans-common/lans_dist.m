%	lans_dist	- Compute distances between 2 sets of column vectors
%
%	[dmat]	= lans_dist(v1,v2,options)
%		  lans_dist(v1,v2)
%		  lans_dist(v1,options)
%		  lans_dist(v1);
%
%	_____OUTPUTS____________________________________________________________
%	dmat	distance matrix					(matrix)
%
%	_____INPUTS_____________________________________________________________
%	v1	first set of points				(col vectors)
%	v2	second set of points				(col vectors)
%	options							(string)
%		-metric
%			Euclidean*	Euclidean
%			Euclidean2	squared Euclidean (faster)
%
%		* default
%
%	_____EXAMPLE____________________________________________________________
%	v1	D x N1
%	v2	D x N2
%		then
%	dmat	is N1 x N2
%
%	_____NOTES______________________________________________________________
%	- for interdistances among themselves, specify just v1
%	- Some error may be present due to subtration of the middle term
%	- options is case insensitive
%
%	_____SEE ALSO___________________________________________________________
%
%
%	(C) 1999.11.24 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [dmat]	= lans_dist(v1,v2,options)

if nargin<3
	options	= '';
	if nargin==2
		if ischar(v2)
			options	= v2;
			v2	= v1;
		end
	else
		v2	= v1;
	end
end

[D1 N1]	= size(v1);
[D2 N2] = size(v2);

if D1~=D2
	error('Dimensions of col vectors v1 and v2 must be the same')
else
	D	= D1;
end

metric	= paraget('-metric',options,'Euclidean2');

switch lower(metric)
	case 'euclidean',
		v12	= sum(v1.*v1);		%  1 x N1
		v22	= sum(v2.*v2);		%  1 x N2
		midterm	= -2*v1'*v2;		% N1 x N2
		dmat	= v12'*ones(1,N2)+ones(N1,1)*v22+midterm;
		dmat	= sqrt(dmat);
	case 'euclidean2',
		v12	= sum(v1.*v1);		%  1 x N1
		v22	= sum(v2.*v2);		%  1 x N2
		midterm	= -2*v1'*v2;		% N1 x N2
		dmat	= v12'*ones(1,N2)+ones(N1,1)*v22+midterm;
end

% get rid of small negative error terms
dmat	= dmat.*(dmat>0);
