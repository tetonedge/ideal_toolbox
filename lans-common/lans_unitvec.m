%	lans_unitvec	- Make col vectors unit-length (Euclidean)
%
%	[ux,mx]	= lans_unitvec(x)
%
%	_____OUTPUTS____________________________________________________________
%	ux	vectors with unitlength		DxN(xP)		(col vectors)
%	mx	magnitude of each vector	1xN(xP)		(row vector)
%
%	_____INPUTS_____________________________________________________________
%	x	col vectors			DxN(xP)		(col vectors)
%
%	_____NOTES______________________________________________________________
%	- can standardize for P input planes of size DxN 
%
%	(C) 1999.04.26 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[ux,mx]	= lans_unitvec(x)

smat	= size(x);
D	= smat(1);
N	= smat(2);

if (length(smat)>2)
	P	= smat(3);
	ux	= zeros(D,N,P);
	mx	= zeros(1,N,P);
	for p=1:P	
		mx(1,:,p)	= vdist(x(:,:,p),0);
		ux(:,:,p)	= x(:,:,p)./(ones(D,1)*mx(1,:,p));

	end
else
	mx	= vdist(x,0);
	ux	= x./(ones(D,1)*mx);
end
