%	lans_assign	- Assign default values for unspecified/zero variables
%
%	avar	= lans_assign(svar,dvar<,replacezero>)
%
%	_____OUTPUTS____________________________________________________________
%	avar	assigned variable				(variable)
%
%	_____INPUTS_____________________________________________________________
%	svar	specified variable				(variable)
%	dvar	default variable (assigned to avar if dvar empty)
%								(variable)
%	replacezero						(dummy)
%		replace svar=0 with dvar if 3rd input arguement specified
%       _____SEE ALSO___________________________________________________________
%	paraget
%
%	(C) 1999.07.19 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [avar]	= lans_assign(svar,dvar,replacezero)

if isempty(svar)
	avar	= dvar;	
else
	if nargin==3
		if svar==0
			avar	= dvar;
		else
			avar	= svar;
		end
	else
		avar	= svar;
	end
end
