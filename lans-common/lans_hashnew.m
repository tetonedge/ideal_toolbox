%	lans_hashnew	- Create a new empty 1-D hash table 
%
%	[hash]		= lans_hashnew(N,<option>)
%	[hash]		= lans_hashnew(filename,<option>)
%
%	_____OUTPUTS____________________________________________________________
%	hash	hashtable with N entries			(structure)
%		.key	actual key comprising of words		(Nx1 cell)	
%		.obj	stored item object			(Nx1 cell)
%
%		INTERNAL USE
%		.point	sparse matrix pointer			(cell of Nx2)
%			up to total = ceil(char*bit/30) cells
%			.point{c}(:,1)
%			= 0	(hash key of this length unoccupied)
%			> 0	(occupied by 1 hash key)	
%			.point{c}(:,2)
%			= 0	(no hash key of longer length encountered)
%			> 0	exists hash key with same prefix
%				gives index c2 to the holder .point{c2}
%		.route	.point traversal for each .key/.obj	(cell of vector)
%		.odirty	dirty .obj locations			(vector)
%		.pdirty	dirty .point cells (to be reused)	(vector)
%		.char	# characters per pointer segment	(scalar)
%		.bitseg	# bits per pointer segment		(scalar)
%		.option saved options				(string)
%			
%
%	_____INPUTS_____________________________________________________________
%	N	# of hash keys					(scalar)
%	
%	option							(string)
%		-bit	# bits used to encode each hash char	(scalar)
%			5*	alphabets a...z (case insensitive)	
%			7	ASCII coding
%		-char	max hash string size			(scalar)
%			20*	20 7-bit chars	
%			larger numbers will be transformed via modulus
%			longer hash string may not be unique
%		-maxbit	# bits for each segment			(scalar)
%			21*	default
%			matlab:30
%			matcom:29	(realistic 21)
%	filename							(string)
%		text file containing lines of hashtable keys
%
%	_____EXAMPLE____________________________________________________________
%
%
%	_____NOTES______________________________________________________________
%	for demo, call function without parameters
%
%	* default
%
%	_____SEE ALSO___________________________________________________________
%	lans_hashadd
%	lans_word2code
%
%	(C) 2000.12.20	Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/
%	_____TO DO______________________________________________________________

function	[hash]		= lans_hashnew(N,option)

if nargin>0
%__________ REGULAR ____________________________________________________________
if nargin<2;
	option	= '-bit 5 -char 20 -maxbit 21';
end
n_bit	= lans_paraget('-bit',option,5);
n_char	= lans_paraget('-char',option,20);
maxbit	= lans_paraget('-maxbit',option,21);
% MATLAB 30
% MATCOM 29
% MATCOM 21	(realistically)

if ischar(N)
	filename	= N;
	fid		= fopen(filename);
	K=0;
	while 1
		line	= fgetl(fid);
		if ~isstr(line)
			break;
		else
			K		= K+1;
			key{K}	= line;
		end
	end
	fclose(fid);
	hash	= lans_hashnew(K);
	for k=1:K
		hash	= lans_hashadd(key{k},k,hash);
	end
else
	charseg		= floor(maxbit/n_bit);	% # characters per segment
	bitseg		= charseg*n_bit;		% # bits per segment

	hash.key		= cell(N,1);
	hash.obj		= cell(N,1);

	hash.point{1}	= sparse(2^(bitseg),2);
	hash.route	= cell(N,1);

	hash.odirty	= N:-1:1;				% so that lower idx entries get filled
	hash.pdirty	= [];

	hash.char		= charseg;			% # chars per segment
	hash.bitseg	= bitseg;				% # bits  per segment
	hash.option	= option;
end

%__________ REGULAR ends _______________________________________________________
else
%__________ DEMO _______________________________________________________________
clc;
disp('running lans_hashnew.m in demo mode');

h	= lans_hashnew(3);
h	= lans_hashadd('junyu',[150 45],h);
h	= lans_hashadd('junjing',[166 55],h);
h	= lans_hashadd('qin',[160 50],h);

[h,obj]	= lans_hashrm('qin',h);

h	= lans_hashadd('junyu',[1 -1],h);

obj	= lans_hashget('junyu',h)
obj	= lans_hashget('qin',h)
obj	= lans_hashget('junjing',h)


%__________ DEMO ends __________________________________________________________
end
