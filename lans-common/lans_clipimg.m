%	lans_clipimg	- Clip img matrix regions into a vector 
%
%	[cimg,cindex,dim]	= lans_clipimg(img,mask)
%
%	_____OUTPUTS____________________________________________________________
%	cimg	clipped image					(vector)
%	cindex	1-D indices of retained region			(vector)
%	dim	dimensions of unclipped image			(2-vector)
%		dim(1)	= # rows dim(2)	= # cols
%
%	_____INPUTS_____________________________________________________________
%	img	image						(matrix)
%	mask	binary mask for clipping			(matrix)
%
%	_____NOTES______________________________________________________________
%	- works for matrix too, naturally.
%	- to recover full clipped img matrix from cimg vector
%		img		= zeros(dim)
%		img(cindex)	= cimg;
%
%	_____SEE ALSO___________________________________________________________
%	cumprod
%
%	(C) 1999.02.21 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [cimg,cindex,dim]	= lans_clipimg(img,mask)

dim	= size(img);
cindex	= find(mask~=0);
cimg	= img(cindex);
