%	lans_intercept	- Find intercept of piecewise linear curve
%
%	[intercept]	= lans_intercept(pwlcurve,imask,ivalue)
%
%	_____OUTPUTS____________________________________________________________
%	intercept	values					(col vectors)
%
%	_____INPUTS_____________________________________________________________
%	pwlcurve	ordered points on piecewise linear curve(col vectors)
%	imask		1/0 mask specifiying known value	(col vector)
%	ivalue		known values				(col vectors)
%
%	_____EXAMPLE____________________________________________________________
%	lans_intercept([0 1;0 1],[1;0],[0.5;0]) gives the "y" intecept at x=0.5
%	i.e. [0.5;0.5]
%
%	_____NOTES______________________________________________________________
%	- pwlcurve ordered as x;y
%	- only works with 2-D, i.e. y = mx + c
%
%	(C) 1999.09.07 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [intercept]	= lans_intercept(pwlcurve,imask,ivalue)

N	= size(pwlcurve,2);
known	= find(imask==1);

intercept	= [];

for n=1:N-1		% traverse all N-1 segments
	if lans_ininterval(ivalue(known),pwlcurve(known,n:n+1));
		% make points
		xrange	= pwlcurve(1,n:n+1);
		yrange	= pwlcurve(2,n:n+1);
		% make linear equation
		m	= diff(yrange)/diff(xrange);
		c	= yrange(1)-m*xrange(1);
		if known==1	% know x, need y
			x	= ivalue(known);
			i	= [x;m*x+c];
		else		% know y, need x
			y	= ivalue(known);
			i	= [(y-c)/m;y];
		end
	intercept	= [intercept i];
	end
end
