%	a2of		- Analog to 1-of-c binary conversion (c=#classes)
%
%	[ddata]	= a2of(adata)
%
%	_____OUTPUT_____________________________________________________________
%	ddata	: 1 of n binarized vectors			(col vectors)
%
%	_____INPUT______________________________________________________________
%	adata	: analog values					(col vectors)
%
%	_____EXAMPLE____________________________________________________________
%	a2of([1 2 2 1 0])=
%
%	0     0     0     0     1
%	1     0     0     1     0
%	0     1     1     0     0
%
%	(C) 1998.02.02 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [ddata]	= a2of(adata)

n	= length(adata);
gadata	= group(adata);
classes	= lans_finduniq(gadata);
c	= length(classes);

ddata	= zeros(c,n);

for i=1:c
	where=find(adata==classes(i));
	ddata(i,where)=ones(size(where));
end

