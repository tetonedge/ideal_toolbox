%	lans_group	- Group data based on outputs
%
%	[gdata,C]	= lans_group(in,out,options)
%			  lans_group(in,out)
%			  lans_group(data,options)
%			  lans_group(data)
%
%	_____OUTPUTS____________________________________________________________
%	gdata	class-grouped data cell				(cell)
%		gdata{k}.in	= input data of class k		col vectors
%		gdata{k}.out	= output value of class k	scalar
%		gdata{k}.id	= id of class k			scalar
%				(optional)
%		...
%	C	# classes					(scalar)
%
%	_____INPUTS_____________________________________________________________
%	in	input data					(col vectors)
%	out	output data					(col vectors)
%	data	LANS format data with .in and .out fields	(structure)
%	options
%		-sort		sort direction			string
%			{ascending*, descending}
%
%		-wastespc	whether to waste space by storing all output
%				samples (same) per class
%			{0*,1}
%
%		* default
%
%	_____EXAMPLE____________________________________________________________
%
%
%	_____NOTES______________________________________________________________
%	for demo, call function without parameters
%	- cell size of gdata shows how many classes are available
%	- stores 1 class vector for each class by default
%	- id field is optional
%
%	_____SEE ALSO___________________________________________________________
%
%
%	(C) 2000.03.09 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[gdata,C]	= lans_group(data,varargin)

if nargin>0
%__________ REGULAR ____________________________________________________________
if nargin==3
	in	= data;
	out	= varargin{1};
	options	= varargin{2};
end
if nargin==2
	if ischar(varargin(1))
		in	= data.in;
		out	= data.out;
		options	= varargin{1};
	else
		in	= data;
		out	= varargin{1};
		options	= [];
	end
end
if nargin==1
	in	= data.in;
	out	= data.out;
	if isfield(data,'id');
		id	= data.id;
	end
	options	= [];
end

dir		= paraget('-sort',options,'ascending');
wastespc	= paraget('-wastespc',options,1);

[D N]	= size(in);
if strcmp(dir,'ascending')
	[s_out,s_idx]	= sortrows(out');
else
	[s_out,s_idx]	= sortrows(-out');
	s_out		= -s_out;	
end

s_out		= s_out';
s_in		= in(:,s_idx);
if isfield(data,'id')
	s_id		= id(s_idx);
end

[clabel,uidx]	= lans_finduniq(s_out);
C		= length(uidx);

for i=1:C-1
	gdata{i}.in	= s_in(:,uidx(i):(uidx(i+1)-1));	
	theone		= ones(1,wastespc*size(gdata{i}.in,2));
	gdata{i}.out	= clabel(:,i)*theone;	
	if isfield(data,'id')
		gdata{i}.id	= s_id(uidx(i):uidx(i+1)-1);	
	end
end

gdata{i+1}.in	= s_in(:,uidx(end):end);
theone		= ones(1,wastespc*size(gdata{i+1}.in,2));
gdata{i+1}.out	= clabel(i+1)*theone;
if isfield(data,'id')
	gdata{i+1}.id	= s_id(uidx(end):end);
end

%__________ REGULAR ends _______________________________________________________
else
%__________ DEMO _______________________________________________________________
clf;clc;
disp('running lans_group.m in demo mode');
irisdata= lans_load('iris');
gdata	= lans_group(irisdata.in,irisdata.out);
lans_plot(gdata,'-legend 1 -colors random');
%__________ DEMO ends __________________________________________________________
end
