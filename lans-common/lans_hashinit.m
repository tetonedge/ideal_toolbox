%	lans_hashinit	- Initialize a numerical hash structure
%
%	[lhash]	= lans_hashinit(d1,d2,...)
%
%	_____OUTPUTS____________________________________________________________
%	lhash	LANS hash structure				(struct)
%		.list	list of values in each dimension	(cell)
%		.idx	indices of values in each dimension	(cell)
%		.max	max # of elements in each cell dimension(cell)
%		.point	current hash pointer/index		(cell)
%			all initialized to 1
%		.read	current pointed values			(cell)
%
%		.amax	max # of elements (in all dimensions)	(scalar)
%		.apoint	current absolute pointer		(scalar)
%
%	_____INPUTS_____________________________________________________________
%	dn	each dimension of hash table			(row vector)
%	...
%	
%	_____NOTES______________________________________________________________
%	- use mainly as a fault-recovery counter for lans_sim
%	- .cpoint faciliates indexing array access, i.e. .cpoint{:} as in
%	    sex(vhash.cpoint{:})=tex
%	   which cannot be done with sex(vhash.point)
%	   however� .max,.point,.read is still needed for arithmatic operations
%	   because the cell class cannot perform arithmetic operations
%	
%
%	_____SEE ALSO___________________________________________________________
%	lans_hashinc lans_sim
%
%	(C) 1999.10.21 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[lhash]	= lans_hashinit(varargin)

D			= length(varargin);

lhash.list		= varargin;
lhash.idx		= [];
lhash.max		= [];
lhash.point		= num2cell(ones(1,D));
lhash.read		= [];

lhash.amax		= [];
lhash.apoint		= 1;

for d=1:D
	lhash.idx{d}	= 1:length(lhash.list{d});
	lhash.max{d}	= lhash.idx{d}(end);
	lhash.read{d}	= lhash.list{d}(1);
end

lhash.amax		= prod(cat(1,lhash.max{:}));
