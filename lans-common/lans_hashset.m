%	lans_hashset	- Modify an entry item in hash table 
%
%	[hash,oobj,idx]= lans_hashset(key,obj,hash,idx)
%
%	_____OUTPUTS____________________________________________________________
%	hash	current hash table				(structure)
%		see lans_hashnew.m
%	oobj	old object					(anything)
%	idx	numeric position of key in hash table		(scalar)
%
%	_____INPUTS_____________________________________________________________
%	key	hash table key					(string,vector)
%	obj	new object					(anything)
%	hash	current hash table				(structure)
%		see lans_hashnew.m
%	idx	optional index for speedup			(scalar)
%
%	_____EXAMPLE____________________________________________________________
%	[hash,oobj,nkey]	= lans_hashset('star wars','sucks',hash);
%
%	_____NOTES______________________________________________________________
%	for demo, call function without parameters
%
%	* default
%
%	_____SEE ALSO___________________________________________________________
%	lans_hashnew	lans_hashadd
%
%	(C) 2000.07.30	Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/
%	_____TO DO______________________________________________________________

function	[hash,oobj,idx]= lans_hashset(key,obj,hash,idx)

if nargin<4
	[oobj,idx]	= lans_hashget(key,hash);
end

oobj		= hash.obj{idx}
hash.obj{idx}	= obj;
