%	som1		- Online self-organizing map (1 iteration) 
%
%	function [mamap,wmse]	= som1(data1,npara,nele,mamap,majump,mapos)
%
%	INPUTS
%	======
%	data1	: one data point				(col vector)
%	npara	: see getnpara.m				(row vector)
%	nele	: # elements in each map dimension		(col vector)
%		  * limited to 1D and 2D
%	mamap	: starting map vectors				(marray)
%	majump	: radix for mamap				(col vector)
%	mapos	: precomputed absolute pos of units		(col vectors)
%
%	OUTPUTS
%	=======
%	mamap	: ending map					(marray)
%	wmse	: locally weighted MSE				(scalar)
%
%	* closed units not yet handled
%
%	(C) 2000.06.28 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/


function [mamap,wmse]	= som1(data1,npara,nele,mamap,majump,mapos)

getnpara;

[d,dum]	= size(data1);
[dk,nk]	= size(mapos);

%----------	find winner(nearest) unit
rdata1	= data1*ones(1,prod(nele));	% replicate
madist2	= vdist2(rdata1,mamap);		% dist of all units to data1
minabs	= min(find(madist2==min(madist2)));	% abs index of nearest unit
minpos	= ma2pos(minabs,nele);		% position  of nearest unit

%----------	find weights for neighbours of winner unit
pdist2	= vdist2(minpos*ones(1,prod(nele)),mapos);	% in map space

if clos
	pdist2	= min([pdist2;clos-pdist2]);
end

facpower= pdist2/((width*fdist)^2);
fac	= exp(-facpower);		% row vector

%----------	update all units toward data by factor of fac and mu
mamap	= mamap + mu*(ones(d,1)*fac).*(rdata1-mamap);

%==========	compute average distortion of newly found map
%----------	find winner(nearest) unit
rdata1	= data1*ones(1,prod(nele));	% replicate
madist2	= vdist2(rdata1,mamap);		% dist of all units to data1
minabs	= find(madist2==min(madist2));	% abs index of nearest unit
minpos	= ma2pos(minabs,nele);		% position  of nearest unit

%----------	find weights for neighbours of winner unit
pdist2	= vdist2(minpos*ones(1,prod(nele)),mapos);

facpower= pdist2/(2*(width*fdist)^2);
fac	= exp(-facpower);		% row vector

%----------	compute weighted mse
wmse	= mean(madist2.*fac);
