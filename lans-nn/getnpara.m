%	getnpara	- Get parameters from npara vector
%
%	requires the vector npara to be present
%
%	(C) 2000.06.28 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

clos	= npara(1);	%	0:open
			%	k:total length2 of curve

kernel	= npara(2);	%	1:Gaussian
			%	2:

width	= npara(3);	%	width of neighbourhood function (kernel)	
			%	= % of fdist

fdist	= npara(4);	%	furthest dist2 of current ksofm

mu	= npara(5);	%	step size
