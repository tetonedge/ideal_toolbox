%	somke		- Batch self-organizing map (k epochs) 
%
%	function [mamap,mse,majump]	= somke(data,npara,snpara,nele,mamap,majump)
%
%	INPUTS
%	======
%	data	: data vectors					(col vectors)
%	npara	: see getnpara.m				(row vector)
%	nele	: # elements in each map dimension		(col vector)
%		  * limited to 1D and 2D
%	+mamap	: starting map vectors				(marray)
%	+majump	: radix for mamap				(col vector)
%
%	OUTPUTS
%	=======
%	mamap	: ending map					(marray)
%	swmse	: sum locally wighted mse			(scalar)
%	majump	: radix (if no initial mamap given)		(col vector)
%
%	* closed units not yet handled
%	+ optional 3 set of parameters
%
%	(C) 2000.06.28 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [mamap,mse,majump]	= somke(data,npara,snpara,nele,mamap,majump)

[d,n]	= size(data);
k	= prod(nele);

if nargin<5,					% no starting map given
	[mamap,majump]	= mamake(nele,d);
	rx		= randperm(n);
	mamap		= data(:,rx(1:k));
end
mapos	= maapos(nele);

getnpara;
getsnpara;
widths	= width;
mus	= mu;
mse	= .1;
omse	= 1;

	%__________	Mulier's method (very fast!)
	P	= prod(nele);
	m	= (1-P/(n*miter))/(P-P/(n*miter));

for epoch=0:miter-1
	%__________	Mulier's method (very fast!)
	mu	= max(muf,mus/(m*epoch*n+1));
	width	= widths*max(widthf,mu);
	putnpara;

	[mamap,mse]	= som1e(data,npara,snpara,nele,mamap,majump,mapos);
	tol	= abs((mse-omse)/omse);

	clf
	plotv(data,'*');
	hold on;
	plotkmap(nele,majump,mamap,clos);
	title(sprintf('[%de] mse = %.4f tol=%.4f mu=%.4f sigma=%.4f',epoch+1,mse,tol,mu,width));
	drawnow;

	if tol>mtol,
		omse	= mse;
		mse	= 0;
	else
		break;
	end
end
