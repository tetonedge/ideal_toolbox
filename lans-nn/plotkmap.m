%	plotkmap	- Plot 2D KSOFM (dist colormap if |w| >2) 
%
%	function []	= plotkmap(nele,majump,mamap,clos)
%
%	INPUTS
%	======
%	nele	: # elements in each map dimension		(col vector)
%		  * limited to 1D and 2D
%	majump	: radix for mamap				(col vector)
%	mamap	: starting map vectors				(marray)
%	+clos	: 0,k						(scalar)
%
%	* currently only works for 2D data vectors
%	+ optional, defaults to 0
%
%	(C) 2000.06.28 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[]	= plotkmap(nele,majump,mamap,clos)

if nargin<4
	clos	= 0;
end

[d,n]	= size(mamap);
dim	= length(nele);

if (d==2)			%	if input space == 2D
	if dim==1
		kplotv(mamap,'o-');
		if clos~=0
			plotv(mamap(:,[n 1]),'o-');
		end
	elseif dim==2
		for i=1:nele(1)	
			startpos= [i 1]';
			endpos	= [i nele(2)]';
			startabs= ma2abs(startpos,majump);
			endabs	= ma2abs(endpos,majump);
			knots	= mamap(:,startabs:endabs);	
			hold on;
			plotv(knots,'o-');
		end		
		for j=1:nele(2)
			startpos=[1 j]';
			endpos	=[nele(1) j]';
			startabs= ma2abs(startpos,majump);
			endabs	= ma2abs(endpos,majump);
			knots	= mamap(:,startabs:nele(2):endabs);
			hold on;
			plotv(knots,'o-');
		end
	end
else
	%plot color map based on Euclidean distance (future)
end
