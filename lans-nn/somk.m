%	somk		- Online self-organizing map (k iterations) 
%
%	function [mamap,swmse,majump]	= somk(data,npara,snpara,nele,mamap,majump)
%
%	INPUTS
%	======
%	data	: data vectors					(col vectors)
%	npara	: see getnpara.m				(row vector)
%	nele	: # elements in each map dimension		(col vector)
%		  * limited to 1D and 2D
%	+mamap	: starting map vectors				(marray)
%	+majump	: radix for mamap				(col vector)
%
%	OUTPUTS
%	=======
%	mamap	: ending map					(marray)
%	swmse	: sum locally wighted mse			(scalar)
%	majump	: radix (if no initial mamap given)		(col vector)
%
%	* closed units not yet handled
%	+ optional 3 set of parameters
%
%	(C) 2000.06.28 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [mamap,swmse,majump]	= somk(data,npara,snpara,nele,mamap,majump)

[d,n]	= size(data);

if nargin<5,					% no starting map given
	[mamap,majump]	= mamake(nele,d);
	mamap		= .9*rand(size(mamap));
end
mapos	= maapos(nele);

getnpara;
if clos~=0
	fdist	= ceil(nele(1)/2);
	clos	= nele(1)^2;
else
	fdist	= vdist(ones(size(nele)),nele);
end

getsnpara;
widths	= width;
mus	= mu;
swmse	= .1;
oswmse	= 1;

	%__________	Mulier's method (very fast!)
	P	= prod(nele);
	m	= (1-P/miter)/(P-P/miter);

for iter=0:(miter-1)
	%__________	typical exponential
%	mu	= mus*((muf/mus)^(iter/miter));
%	width	= widths*mu;

	%__________	Mulier's method (very fast!)
	mu	= max(muf,mus/(m*iter+1));
	width	= widths*max(widthf,mu);
%	fixed width makes it more like Pcurve!!!!!

	putnpara;

	riter	= rem(iter,n)+1;
	[mamap,wmse]	= som1(data(:,riter),npara,nele,mamap,majump,mapos);
	if riter~=1
		swmse	= swmse + wmse;
	else
		tol	= abs((swmse-oswmse)/oswmse);
		clf
		kplotv(data,'*');
		hold on;
		plotkmap(nele,majump,mamap,clos);
		title(sprintf('[%d] wmse = %.4f tol=%.4f mu=%.4f sigma=%.4f',iter,swmse/n,tol,mu,width));
		drawnow;
		if tol>mtol,
			oswmse	= swmse;
			swmse	= 0;
		else
			break;
		end
	end
end
