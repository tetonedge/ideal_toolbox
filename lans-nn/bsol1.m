%	bsol1		- Batch self-organizing line (1 epoch/iteration)
%
%	function [nk,nf,mse]	= bsol1(data,para,k,f)
%
%	INPUTS
%	======
%	data	: data						(col vectors)
%	para	: see getpara.m					(row vector)
%	k	: starting self-organizing knots		(row vector)
%	f	: starting self-organizing line			(col vectors)
%
%	OUTPUTS
%	=======
%	nk	: new self-organizing knots			(row vector)
%	nf	: new self-organizing line			(col vectors)
%	mse	: mse of each point to its nearest projection	(scalar)
%		  (NOT mean distance, but mean squared distance)
%
%	CURRENTLY ONLY OPEN VERSION IMPLEMENTED	20 Sep 1997
%
%	(C) 1997.09.20 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [nk,nf,mse]	= bsol1(data,para,k,f)

[d n]	= size(data);
getpara;

kn		= length(k);
ki		= 1:kn;		%list of nodes
halfneigh	= span*kn/2;	% half the dist of the neighbourhood

%----------	for each knot
for i=1:kn
	neigh	= find(abs(ki-i)<halfneigh);	%list of neighbouring nodes	

	if length(neigh)==0
		neigh=i;	
	end
	%----------	Compute distance from each datapoint to all knots
	psum	= zeros(d,1);
	count	= 0;
	for j=1:n
		x	= data(:,j);		%a data point
		xmat	= x*ones(1,kn);		%replicate x kn times
		x2k	= vdist2(xmat,f);	%dist from x to knots
		mdist	= min(x2k);		%min dist from x to knots
		nearest	= find(x2k==mdist);	%nearest knot to x	

		% check if knot is in neighborhood
		isin	= 0;
		if length(nearest)>1
			for each=1:length(nearest)
			isin	= isin+~isempty(find(neigh==nearest(each)));
			end
		else
			isin	= ~isempty(find(neigh==nearest));	
		end
	
		%if nearest knot is in neighborhood=> include in sum
		if isin
			psum	= psum+x;	
			count	= count+1;
		end
	end
	nf(:,i)	= psum/count;
end

%----------	compute the distance along the knots
mdist	= vdist(nf(:,2:kn),nf(:,1:kn-1));
nk(1)	= 0;
for i=2:kn
	nk(i)=nk(i-1)+mdist(i-1);
end

%----------	compute mse
mse	= projdist(data,nk,nf,para);
