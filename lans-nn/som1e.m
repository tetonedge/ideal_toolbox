%	som1e		- Batch Self Organizing Map (1 epoch) 
%
%	function [mamap,mse]	= som1e(data,npara,snpara,nele,mamap,majump,mapos)
%
%	INPUTS
%	======
%	data	: data vectors					(col vectors)
%	npara	: see getnpara.m				(row vector)
%	snpara	: see getsnpara.m				(row vector)
%	nele	: # elements in each map dimension		(col vector)
%		  * limited to 1D and 2D
%	+mamap	: starting map vectors				(marray)
%	+majump	: radix for mamap				(col vector)
%	mapos	: precomputed absolute pos of units		(col vectors)
%		 e.g. for 1-D 10 units
%		mapos = [1 2 3 4 5 6 7 8 9 10]
%
%	OUTPUTS
%	=======
%	mamap	: ending map					(marray)
%	mse	: mean squared error				(scalar)
%
%	* closed units not yet handled
%	+ optional 3 set of parameters
%
%	(C) 2000.06.28 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [mamap,mse]	= som1e(data,npara,snpara,nele,mamap,majump,mapos)

[d,n]	= size(data);
[d,k]	= size(mamap);
getnpara;
getsnpara;

for unit=1:k
	unitpos		= ma2pos(unit,nele);

	% compute dist from unit to all other units
	pdist2		= vdist2(unitpos*ones(1,k),mapos);
	if clos~=0
		pdist2	= min([pdist2;(nele(1)+1)^2-pdist2]);
	end

	% computer neighborhood in ascending distance from unit
	[spdist2,ind]	= sort(pdist2);
	neighborhood	= ind(1:max(1,floor(width*k)));
	psum	= zeros(d,1);
	count	= 0;
	for sample=1:n
		rd	= data(:,sample)*ones(1,k);
		mdist	= vdist2(rd,mamap);
		winner	= min(find(mdist==min(mdist)));	
		if ~isempty(find(neighborhood==winner))
			psum	= psum + data(:,sample);	
			count	= count + 1;
		end
	end
	if (count~=0)
		nmamap(:,unit)=psum/count;
	else
		nmamap(:,unit)=mamap(:,unit);
	end
end

mamap	= nmamap;

sse	= 0;
for sample=1:n
	rdata	= data(:,sample)*ones(1,k);
	sse	= sse+min(vdist2(rdata,mamap));
end
mse	= sse/n;
