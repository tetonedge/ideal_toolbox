%	lans_menuaxis	- Attach log/linear axis selection menu to figure
%
%	lans_menuaxis(thefig)
%
%	_____OUTPUTS____________________________________________________________
%
%	_____INPUTS_____________________________________________________________
%	thefig	The figure number to attach to			(integer)
%	+ internal uses
%
%	_____EXAMPLE____________________________________________________________
%	lans_menuaxis(1)	attaches a axis menu to figure 1
%
%	_____NOTES______________________________________________________________
%	- for demo, call function without parameters 
%
%	_____SEE ALSO___________________________________________________________
%
%	(C) 2000.03 17 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________

function	lans_menuaxis(thefig,theaxis,theaxistype);
if nargin>0
%__________ REGULAR ____________________________________________________________
AXISNAME	= {'X','Y','Z'};
AXISTYPE	= {'Linear','Log'};

if nargin==1	% initialize
	menuhandle	= uimenu(thefig,'label','Axis');

	for a=1:length(AXISNAME)
		thename	= ['Axis ' AXISNAME{a}];
		submenu(a)	= uimenu(menuhandle,'label',thename);
		for i=1:2
			subsub(a,i)	= uimenu(submenu(a),'label',AXISTYPE{i},'callback',...
		sprintf('lans_menuaxis(%d,%d,%d)',thefig,a,i));
		end
	end
	resetaxis	= uimenu(menuhandle,'label','All Linear','separator','on','callback',sprintf('lans_menuaxis(%d,%d)',thefig,1));
	resetaxis	= uimenu(menuhandle,'label','All Log','separator','on','callback',sprintf('lans_menuaxis(%d,%d)',thefig,2));
elseif nargin==2		% Callback
	theaxistype	= theaxis;
	for a=1:length(AXISNAME)
		lans_menuaxis(thefig,a,theaxistype);
	end
elseif nargin==3
	figure(thefig);
	switch theaxis
		case 1
			set(gca,'XScale',AXISTYPE{theaxistype});
		case 2
			set(gca,'YScale',AXISTYPE{theaxistype});
		case 3
			set(gca,'ZScale',AXISTYPE{theaxistype});

	end
end

%__________ REGULAR ends _______________________________________________________
else
%__________ DEMO _______________________________________________________________
figure(1);
clf;clc;
disp('running lans_menuaxis.m in demo mode');
plot3(rand(1,10),rand(1,10),rand(1,10))     
lans_menuaxis(1)
%__________ DEMO ends __________________________________________________________
end

