%	lans_plotgrad	- Plot gradient in multi-dimensional space
%
%	[]	= lans_plotgrad(f,dfdq <,plottype> <,options>);
%
%	_____INPUTS_____________________________________________________________
%	f	vector func of Q latent variables u_q in R^D	(col vectors)
%		D x N
%	dfdq	gradient at each point in f w.r.t. u_q		(D x Q x N)
%		dfdq	= [dvecf/du_1 ... dvecf/du_Q] for each N
%	ptype	plot type					(2 char string)
%	options							(string) 
%		-plotdim	{2,3}	2 default
%
%	_____NOTES______________________________________________________________
%	- for demo, call function without parameters
%	- does not plot f, just dfdq imposed on current figure (held)
%	- to plot @ single point, i.e. f=Dx1, dfdq=DxQ, reshape dfdq to DxQx1
%	- MATLAB's quiver and quiver3 automatically scales the gradients to
%	  fit within grid
%	  
%
%	_____SEE ALSO___________________________________________________________
%	lans_gradient lans_plotmd lans_plotcovars quiver quiver3
%
%	(C) 1999.11.09 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[]	= lans_plotgrad(f,dfdq,ptype,options);

if nargin>0
%__________ REGULAR ____________________________________________________________
if nargin<4
	options	= [];
	if nargin<3
		ptype	= 'k-';
		if nargin<1
			clc;
			help lans_plotgrad;
			break;
		end
	end
end

plotdim	= paraget('-plotdim',options,2);

[D,N]	= size(f);
%_____	if plotdim unspecified
if isempty(findstr(options,'-plotdim')) 
	if D==3
		plotdim=3;
	end
end	

smat	= size(dfdq);			% size matrix
D	= smat(1);
if (length(smat)>2)			% multiple samples	(Q>1)
	Q	= smat(2);		% determine # of quivers
	N	= smat(3);
else
	if N==1				% single sample		(Q>1)
		Q		= smat(2);
	else				% multiple samples	(Q=1)
		Q		= 1;
%		dfdx(:,1,:)	= dfdq;
%		dfdq		= dfdx;
	end
end

[row,col]	= lans_fit2page(D,plotdim);

%_____	Plot gradient vectors/quivers in each dimensions
for j=1:floor(D/plotdim)
	if D>3
		subplot(row,col,j);
	end
	hold on;
	d1	= plotdim*(j-1)+1;
	if plotdim==2
		d2	= plotdim*j;
		if Q==1
			quiver(f(d1,:),f(d2,:),dfdq(d1,:),dfdq(d2,:),ptype);

		else
			for q=1:Q
				quiver(f(d1,:),f(d2,:),dfdq(d1,q,:),dfdq(d2,q,:),ptype);
			end
		end
	else	% plotdim = 3;
		d2	= d1+1;
		d3	= plotdim*j;
		for q=1:Q
			grad	= squeeze(dfdq(:,q,:));
			quiver3(f(d1,:),f(d2,:),f(d3,:),grad(d1,:),grad(d2,:),grad(d3,:),ptype);
		end
	end
end
%__________ REGULAR ends _______________________________________________________
else
%__________ DEMO _______________________________________________________________
clf;clc;
disp('running lans_plotgrad.m in demo mode');
N	= 20;
rand('state',1);
r	= rand(1,N);
t	= sort(2*pi*r);
tsurf.x	= t;
tsurf.f	= [t;sin(t)];
[f1 f2]	= lans_gradient(tsurf,'-gradient wavg');
lans_plotmd(tsurf.f,'bo-');
lans_plotgrad(tsurf.f,f1,'m-');
lans_plotgrad(tsurf.f,f2,'r-');
axis square;
%__________ DEMO ends __________________________________________________________
end
%
