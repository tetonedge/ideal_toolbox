%	lans_plotproj	- Plot projections of points onto a curve
%
%	[h]	= lans_plotproj(prcurve,data[,ptype]);
%
%	_____OUTPUTS____________________________________________________________
%	h	handles corresponding to projection plots	(col vector)
%
%	_____INPUTS_____________________________________________________________
%	prcurve	projection position on manifold			(col vectors)
%	data	original data to be projected			(col vectors)
%	ptype	plot type					(string)
%
%	_____NOTES______________________________________________________________
%	- plot held by default
%	do a project.m before plotting data and curves
%
%	_____SEE ALSO___________________________________________________________
%	lans_plotmd lans_project lans_ppsproj
%
%	(C) 2000.02.06 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [h]	= lans_plotproj(curve,data,ptype);

if nargin<3
	ptype	= 'r-';
end

[d,n]	= size(data);
[df,nf]	= size(curve);

if nf~=n
	error('# curve knots not equal to # data points');
end

for k=1:n
	theline	= [data(:,k) curve(:,k)];
	h=lans_plotmd(theline,ptype,'-hold 1');
end
