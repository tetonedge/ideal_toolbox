%	lans_equalize	- Stretch histogram to fill all ranges
%
%	[eimg]	= lans_equalize(img);
%
%	_____OUTPUTS____________________________________________________________
%	eimg	equalized image					(matrix)
%
%	_____INPUTS_____________________________________________________________
%	img	image						(matrix)
%
%	_____NOTES______________________________________________________________
%
%	_____SEE ALSO___________________________________________________________
%
%	(C) 1999.02.17 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [eimg]	= lans_equalize(img)

k	= 256;

b	= max(max(img));
a	= min(min(img));

if (b-a)~=0
	p	= (k-1)/(b-a);
else
	p	= 1;
end
l	= -a*p;

eimg	= floor(p*img+l+0.5);
