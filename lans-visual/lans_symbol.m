%	lans_symbol	- Generate small # of unique plot color/symbols 
%
%	[scell]	= lans_symbol(n<,darker>)
%
%	_____OUTPUTS____________________________________________________________
%	scell	n unique plot symbols					(cell)
%		each symbol 4 chars long
%		char	1	color
%			2	symbol type
%			3-4	line type
%
%	_____INPUTS_____________________________________________________________
%	n	desired # of symbols					(scalar)
%	darker	Generate darker colors if set to 1			(binary)
%		red blue magenta black
%		rbmk
%
%	_____NOTES______________________________________________________________
%	- see lans_color for more colors
%
%	_____EXAMPLE____________________________________________________________
%	lans_gensymbol(5) returns
%	'ro- '    'g+: '    'b*-.'    'ms--'    'cx- '
%
%	_____SEE ALSO___________________________________________________________
%	lans_color
%
%	(C) 2000.03.01 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[scell]	= lans_symbol(n,dark)

if nargin<2
	dark	= 0;
end

%----------	set up color table
linetable		= {'- ',': ','-.','--'};
nlines			= length(linetable);

symboltable		= 'o+*sxdv^<>ph.';
nsymbols		= length(symboltable);

if dark==0
	colortable		= 'rgbcmywk';
else
	colortable		= 'rgbcmywk';
end
ncolors			= length(colortable);

%----------	try all different combinations
scell	= {};
k	= 1;
l	= 1;
s	= 1;
c	= 1;

while k<n+1
	thesym	= [colortable(c) symboltable(s) char(linetable(l))]; 
	scell(k)= {thesym};
	l	= l+1;l=l-(l>nlines)*nlines;
	s	= s+1;s=s-(s>nsymbols)*nsymbols;
	c	= c+1;c=c-(c>ncolors)*ncolors;
	k	= k+1;
end
