%	lans_scatline	- Plot original points with regressed lines in d-D 
%
%	[]	= lans_scatline(data,data1[,label[,options]]);
%
%	_____INPUTS_____________________________________________________________
%	data	original data, plotted as points		(col vectors)
%	data1	regressed data, plotted as lines		(col vectors)
%		[]	if none
%
%	label	class						(scalar)
%		0	(single class)				default
%	options	see lanspara.m					(string)
%		-plotdim
%		-hold
%		-markline
%		-clos
%		-box
%		-grid
%
%	_____NOTES______________________________________________________________
%	- for demo, call function without parameters
%	- need to append '-clos' to options for plotting closed curves
%
%	_____SEE ALSO___________________________________________________________
%	lans_plotmd
%
%	(C) 1998.11.23 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[]	= lans_scatline(data,data1,label,options);

if nargin>0

%--------------------	regular
%----------	default options
if nargin<4
	options	= [];
	if nargin<3
		label	= 0;
	end
end

%----------	get parameters
clos	= paraget('-clos',options);
markline= paraget('-markline',options);

%----------	set up color table
symboltable		= '.ox+*sdv^<>ph';
nsymbols		= length(symboltable);

colortable		= 'rgbmcyk';
ncolors			= length(colortable);

linetable		= '- - - - - - : -.--';
nlines			= length(linetable)/2;

%----------	plot data
userchoice	= paraget('-hold',options);
if label==0					% just one class
	if userchoice~=1
		options		= paraset('-hold',0,options);
	end
	label		= 1;
else
	options		= paraset('-hold',userchoice,options);
end;

ctype		= mod(label-1,ncolors)+1;	% cycle thru colors
stype		= mod(label-1,nsymbols)+1;	% cycle thru symbol types
ltype		= mod(label-1,nlines)+1;	% cycle thru line types

pointcolor	= [colortable(ctype) symboltable(stype)];	
linetype	= [colortable(ctype) linetable(2*ltype-1:2*ltype)];

lans_plotmd(data,pointcolor,options);			% plot data points

if any(any(data1))
	options		= paraset('-hold',1,options);
	if clos
		data1=[data1 data1(:,1)];	
	end
	if markline
		linetype	= 'r-';
	end

	lans_plotmd(data1,linetype,options);		% plot regressed lines

	if markline
		linetype(2)	= 'x';
		lans_plotmd(data1,linetype,options);
	end
end

else
%--------------------	demo
clf;clc;
disp('running lans_scatline.m in demo mode');
d	= 9;
n	= 10;
x	= rand(d,n);
lans_scatline(x,x,1,'-plotdim 3 -box 1 -grid 1');

%--------------------	demo ends
end;
