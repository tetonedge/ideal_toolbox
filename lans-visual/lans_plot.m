%	lans_plot	- Plot D-dimensional vector across multiple classes
%
%	[h]	= lans_plot(data, p, options)
%		  lans_plot(data, p)
%		  lans_plot(data, options)
%		  lans_plot(data)
%
%	_____OUTPUTS____________________________________________________________
%	h	handles correpsonding to each plot type		(col vector)
%
%	_____INPUTS_____________________________________________________________
%	data							(cell/structure)
%		.in	main DxN data
%		.out	output/label
%	
%	p	plot properties					(cell/structure)
%		.symbol	plot symbol				(3string)
%		.color	colormap				1 x 3
%		.legend	text label				(string)
%
%	options							(string)
%		-axis		axis constraint			(string)
%				normal*
%				equal
%				strict
%				(see matlab axis command)	
%		-plotaxis	whether to plot axis
%				{0,1*}
%		-dark		darker?
%				{0*,1}
%		-hold		whether to hold plot
%				{0*,1}
%		-box		whether to frame plote
%				{0,1*}
%		-grid		whether to add grid
%				{0*,1}
%		-plotdim	whether to plot 2-D or 3-D subplots
%				{2*,3}
%		-legend		whether to put legend
%				{0*,1}
%		-plotline	specify line plot instead of scatter plot
%				{0*,l}
%				l: linewidth
%		-xticklabel	whether to enumerate x axis
%				{0,1*}
%		-yticklabel	whether to enumerate y axis
%				{0,1*}
%		-zticklabel	whether to enumerate z axis
%				{0,1*}
%	
%	_____NOTES______________________________________________________________
%	- an improved version of lans_plotmd
%	- for demo, call functions without parameters
%	- plots a 3-D graph for 3-D vdata1 unless plotdim is specified
%	- hold applies only to active subplot
%	- will not create subplots if d<=3
%	- BUG with matlab 5.2 : legend symbol width not updated with plot
%	- create legend with, e.g.
%		legend(h,char({'1','2','3','4','5','6'})) 
%
%	_____SEE ALSO___________________________________________________________
%	lans_plotmd
%	axis
%
%	(C) 2000.06.29 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[h]	= lans_plot(data,varargin);

if nargin>0
%__________ REGULAR ____________________________________________________________
% DEFAULT Plot type
MARKER_BASE	= 5;
dp.symbol	= '.';
dp.color	= [1 0 0];
dp.legend	= '';

genp	= 0;
if nargin<2
	options	= [];
	genp	= 1;
else
	if length(varargin)==2
		p	= varargin{1};
		options	= varargin{2};
	else
		if ischar(varargin{1})
			options	= varargin{1};
			genp	= 1;
		elseif isstruct(varargin{2})
			p	= varargin{2};
			options	= [];
		else
			error('Second arguement incorrect');
		end
	end
end

%__________	get parameters
theaxis		= paraget('-axis',options,'normal');
axisit		= paraget('-plotaxis',options,1);
boxit		= paraget('-box',options,1);
gridit		= paraget('-grid',options,0);
legendit	= paraget('-legend',options,0);
plotdim		= paraget('-plotdim',options,2);
plotline	= paraget('-plotline',options,0);
holdplot	= paraget('-hold',options,0);
xticklabel	= paraget('-xticklabel',options,1);
yticklabel	= paraget('-yticklabel',options,1);
zticklabel	= paraget('-zticklabel',options,1);

if genp	
	if iscell(data)
		nclass	= length(data);
		cmap	= lans_color(nclass,options);
		symbol	= lans_symbol(nclass);
		for c=1:nclass
			% generate custom colors & symbols
			% for multiple classes
			if plotline
				dp.symbol	= symbol{c}(1:3);
			else
				dp.symbol	= symbol{c}(1:2);
			end
			dp.color	= cmap(c,:)';
%			dp.legend	= num2str(c);
			if ischar(data{c}.out(1))
				dp.legend	= data{c}.out(1);
			else
				dp.legend	= num2str(data{c}.out(1));
			end
			p{c}		= dp;
		end
	else
		p	= dp;
	end
end
%__________	Check if multiple classes
if iscell(data)
	nclass	= length(data);
	options	= paraset('-legend',0,options);
	h	= zeros(nclass,1);
	for c=1:nclass
		if c==1
			options	= paraset('-hold',1,options);
		end
		h(c)	= lans_plot(data{c},p{c},options);
	end
	if legendit
		lstr	= [];
		for c=1:nclass
			lstr{c}	= p{c}.legend;
		end
		legend(h,char(lstr),4);
	end
	return;
end
%__________	Plot
[D,N]		= size(data.in);
if (D==3)
	if isempty(options)
		plotdim	= 3;
	elseif isempty(findstr(options,'-plotdim'))
		plotdim	= 3;		
	end
end
[row,col]	= lans_fit2page(D,plotdim);

%__________	plot plotdim-D (default 2-D) planes of the data
for j	= 1:floor(D/plotdim)
	if D>3
		subplot(row,col,j);
	end
	if holdplot
		hold on;
	else
		hold off;
	end
	d1	= plotdim*(j-1)+1;		% begin
	if plotdim==2
		d2	= plotdim*j;
		if plotline
			h=plot(data.in(d1,:),data.in(d2,:),p.symbol,'Color',p.color,'LineWidth',plotline,'MarkerSize',plotline*MARKER_BASE);
		else
			h=plot(data.in(d1,:),data.in(d2,:),p.symbol,'Color',p.color);
		end
		xlabel(sprintf('x_{%d}\n',d1),'VerticalAlignment','top','FontAngle','Italic','FontName','Times');
		ylabel(sprintf('x_{%d}\n',d2),'VerticalAlignment','middle','FontAngle','Italic','FontName','Times');
	else
		d2	= d1+1;
		d3	= plotdim*j;
		if plotline
			h	= plot3(data.in(d1,:),data.in(d2,:),data.in(d3,:),p.symbol,'Color',p.color,'LineWidth',plotline,'MarkerSize',plotline*MARKER_BASE);
		else
			h	= plot3(data.in(d1,:),data.in(d2,:),data.in(d3,:),p.symbol,'Color',p.color);
		end
		view(3);
		xlabel(sprintf('x_{%d}\n',d1),'VerticalAlignment','baseline','FontAngle','Italic','FontName','Times');
		ylabel(sprintf('x_{%d}\n',d2),'VerticalAlignment','baseline','FontAngle','Italic','FontName','Times');
		zlabel(sprintf('x_{%d}\n',d3),'VerticalAlignment','baseline','FontAngle','Italic','FontName','Times');
	end

	comaxis	= ['axis ' theaxis];
	eval(comaxis);
	if ~xticklabel,set(gca,'XTickLabel',[]),end;
	if ~yticklabel,set(gca,'YTickLabel',[]),end;
	if ~zticklabel,set(gca,'ZTickLabel',[]),end;

	if axisit
		axis on;
	else
		axis off;
	end
	if boxit
		box on;
	else
		box off;
	end
	if gridit
		grid on;
	else
		grid off;
	end
end

if D>3
	subplot(row,col,1);
end
%__________ REGULAR ends _______________________________________________________
else
%__________ DEMO _______________________________________________________________
clf;clc;
disp('running lans_plot.m in demo mode');
irisdata= lans_load('iris');
gdata   = lans_group(irisdata);
lans_plot(gdata,'-legend 1 -colors random');

%__________ DEMO ends __________________________________________________________
end

