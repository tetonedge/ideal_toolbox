%	lans_plotgrid	- Plot 2-D grid arranged D-dimensional vector 
%
%	[]	= lans_plotgrid(gdata,gtype,options)
%
%	_____INPUTS_____________________________________________________________
%	gdata	2-D grid arranged D-dimensional vector		(col vectors)
%	gtype	plot type					(string)
%	options	see lanspara.m					(string)
%		-axis
%		-box
%		-grid
%		-plotdim
%		-hold
%
%	_____NOTES______________________________________________________________
%	- for demo, call functions without parameters
%
%	_____SEE ALSO___________________________________________________________
%	lans_plotmd
%
%	(C) 1999.08.10 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________

function	[]	= lans_plotgrid(gdata,gtype,options);

if nargin>0
%__________ REGULAR ____________________________________________________________
if nargin<3
	options	= ['-hold 1'];
	if nargin<2		% nothing specified
		gtype	= 'bo-';
	end
else
	options	= ['-hold 1' options];
end

gdim	= size(gdata);

if length(gdim)<3
	lans_plotmd(gdata,gtype,options);
else
	%__________	plot horizontal strips
	for x=1:gdim(2)
		lans_plotmd(gdata(:,x,:),gtype,options);
	end
	%__________	plot vertical strips
	for y=1:gdim(3)
		lans_plotmd(gdata(:,:,y),gtype,options);
	end
end
%__________ REGULAR ends _______________________________________________________
else
%__________ DEMO _______________________________________________________________
clf;clc;
disp('running lans_plotgrid.m in demo mode');
y	= lans_grid(1:5,1:5);
lans_plotgrid(y,'bo-');

%__________ DEMO ends __________________________________________________________
end;
