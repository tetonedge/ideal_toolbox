%	lans_plotseg	- Plot disjoint segments
%
%	function	[h]	= lans_plotseg(ps1,ps2 <,ptype> <,options>);
%
%	_____OUTPUTS____________________________________________________________
%	h	handle to corresonponding segments		(col vector)
%
%	_____INPUTS_____________________________________________________________
%	ps1	point set 1 : starting points			(col vectors)
%	ps2	point set 2 : ending points			(col vectors)
%	ptype	2 char string defining plot type		(row vector)
%	options	see lanspara.m					(string)
%		-plotdim
%		-hold
%
%	_____SEE ALSO___________________________________________________________
%	lans_plotmd lans_plotaxis
%
%	(C) 1999.11.15 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[h]	= lans_plotseg(ps1,ps2,ptype,options);

[D,N]		= size(ps1);

if nargin<4
	options=[];
	if nargin<3
		ptype	= 'k-';
		if nargin<2
			ps2	= zeros(size(ps1));
		end
	end
end

for n=1:N
	h	= lans_plotmd([ps1(:,n) ps2(:,n)],ptype,[options ' -hold 1']);
end
