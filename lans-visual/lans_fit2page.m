%	lans_fit2page	- Compute subplot rows/cols given d-D data 
%
%	[row,col]	= lans_fit2page(d[,dim[,ratio]])
%
%	_____OUTPUTS____________________________________________________________
%	row	# subplot rows						(scalar)
%	col	# subplot cols						(scalar)
%
%	_____INPUTS_____________________________________________________________
%	d	# dimensions in data					(scalar)
%	dim	# dimensions in each subplot				(scalar)
%		{2,3}	default = 2
%	ratio	of row to col						(scalar)
%			default = 11/8.5
%
%	_____EXAMPLE____________________________________________________________
%	[row,col]	= lans_fit2page(10) returns [3;2]
%
%	(C) 1996-2003 Kui-yu Chang
%   2003.09.16
%	http://www.lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[row,col]	= lans_fit2page(d,dim,ratio)

%-----	type of plots
if nargin<3
	%-----	US Letter row to col ratio
	ratio	= 11/8.5;
	if nargin<2
		dim = 2;
		if nargin<1
			clc;
			help lans_fit2page;
			return;
		end
	end
end

%-----	ideal size for 2-D plots
x	= ceil(sqrt(d/(ratio*dim)));
y	= ceil(x*1.3);

%-----	try decreasing x by 1
if (x-1)*y>=floor(d/dim)
	x	= x-1;
end

if x*(y-1)>=floor(d/dim)
	y	= y-1;
end

if (x*y==2)		% for 2 subplots, choose horizontal plot
	row	= x;
	col	= y;
else
	row	= y;
	col	= x;
end
