%	lans_color	- Generate N distinct RGB colors
%
%	[cmap]	= lans_color(N,options)
%
%	_____OUTPUTS____________________________________________________________
%	cmap	RGB colors N x 3				(col vectors)
%
%	_____INPUTS_____________________________________________________________
%	N	desired # of colors				(scalar)
%	options							(string)
%		-colors	Generation method
%			random	randomly sampled within RGB cube
%			uniform* uniform distributed in RGB cube
%		-brightness	Brightness attenuation
%			1*	brighhter
%			0	dark
%		-map	Type of colormap (see graph3d)
%			hsv*
%
%		* default
%
%	_____EXAMPLE____________________________________________________________
%
%	_____SEE ALSO___________________________________________________________
%	lans_symbol
%	graph3d
%
%	(C) 1999.12.02 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________
%	darkness/brightness

function	[cmap]	= lans_color(N,options)

if nargin<2,options=[];end

maptype	= paraget('-map',options,'hsv');
colors	= paraget('-colors',options,'uniform');
fac	= paraget('-brightness',options,1);

com	= ['map=' maptype '(N);'];
eval(com);

switch lower(colors)
	case 'uniform',
		cmap	= map;
	case 'random',
		ridx	= randperm(N);
		cmap	= map(ridx,:);
end
