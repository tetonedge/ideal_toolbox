%	lans_plotmd	- Plot d-dimensional vector 
%
%	[h]	= lans_plotmd(vdata1)
%		  lans_plotmd(vdata1,ptype1)
%		  lans_plotmd(vdata1,ptype1,options)
%		  lans_plotmd(vdata1,ptype1,vdata2,ptype2)
%		  lans_plotmd(vdata1,ptype1,vdata2,ptype2,options)
%
%	_____OUTPUTS____________________________________________________________
%	h	handles correpsonding to vdata1,vdata2		(col vector)
%
%	_____INPUTS_____________________________________________________________
%	vdatai	D-dimensional vector				(col vectors)
%
%	ptypei	plot type					(3string)
%		'r*'						default
%	options							(string)
%		-axis
%		-box
%		-grid
%		-plotdim
%		-hold
%
%	_____NOTES______________________________________________________________
%	- USE newer version lans_plot
%	- for demo, call functions without parameters
%	- plots a 3-D graph for 3-D vdata1 unless plotdim is specified
%	- hold applies only to active subplot
%	- will not create subplots if d<=3
%
%	_____SEE ALSO___________________________________________________________
%	lans_plot
%
%	(C) 1999.09.24 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[h]	= lans_plotmd(vdata1,ptype1,vdata2,ptype2,options);

if nargin>0
%--------------------	regular
%----------	default options
if nargin<5
	if nargin<4
		if nargin<3
			options	= [];
			if nargin<2		% nothing specified
				ptype1	= 'r*';
			end
		else
			options	= vdata2;
		end
	else
		options	= [];
	end
end

%----------	get parameters
axisit		= paraget('-axis',options);
boxit		= paraget('-box',options);
gridit		= paraget('-grid',options);
plotdim		= paraget('-plotdim',options);
holdplot	= paraget('-hold',options);

[D,N]		= size(vdata1);
if (D==3)
	if isempty(options)
		plotdim	= 3;
	elseif isempty(findstr(options,'-plotdim'))
		plotdim	= 3;		
	end
end
[row,col]	= lans_fit2page(D,plotdim);


%---------	plot plotdim (default 2-D) planes of the data
for j	= 1:floor(D/plotdim)
	if D>3
		subplot(row,col,j);
	end
	if holdplot
		hold on;
	else
		hold off;
	end
	d1	= plotdim*(j-1)+1;		% begin
	if plotdim==2
		d2	= plotdim*j;
		if nargin>3
			h=plot(vdata1(d1,:),vdata1(d2,:),ptype1,vdata2(d1,:),vdata2(d2,:),ptype2);
		else
			h=plot(vdata1(d1,:),vdata1(d2,:),ptype1);
		end
		xlabel(sprintf('x_{%d}\n',d1),'VerticalAlignment','top','FontAngle','Italic','FontName','Times');
		ylabel(sprintf('x_{%d}\n',d2),'VerticalAlignment','middle','FontAngle','Italic','FontName','Times');
	else
		d2	= d1+1;
		d3	= plotdim*j;
		if nargin>3
			h=plot3(vdata1(d1,:),vdata1(d2,:),vdata1(d3,:),ptype1,vdata2(d1,:),vdata2(d2,:),vdata2(d3,:),ptype2);
		else
			h=plot3(vdata1(d1,:),vdata1(d2,:),vdata1(d3,:),ptype1);
		end
		view(3);
		xlabel(sprintf('x_{%d}\n',d1),'VerticalAlignment','baseline','FontAngle','Italic','FontName','Times');
		ylabel(sprintf('x_{%d}\n',d2),'VerticalAlignment','baseline','FontAngle','Italic','FontName','Times');
		zlabel(sprintf('x_{%d}\n',d3),'VerticalAlignment','baseline','FontAngle','Italic','FontName','Times');
	end

	axis equal;
	if boxit
		box on;
	else
		box off;
	end
	if gridit
		grid on;
	else
		grid off;
	end
	if axisit
		axis on;
	else
		axis off;
	end
		
end

if D>3
	subplot(row,col,1);
end
%--------------------	regular ends
else
%--------------------	demo
clf;clc;
disp('running lans_plotmd.m in demo mode');
D	= 9;
N	= 10;
x	= rand(D,N);
lans_plotmd(x,'bo',x,'r-','-plotdim 3 -grid 1');
%--------------------	demo ends
end;
