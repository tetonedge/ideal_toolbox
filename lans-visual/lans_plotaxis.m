%	lans_plotaxis	- Plot axes
%
%	function	[]	= lans_plotaxis(ax <,center><,ptype><,options>);
%
%	_____INPUTS_____________________________________________________________
%	ax	end points					(col vectors)
%	center	center point					(col vectors)
%	ptype	2 char string defining plot type		(row vector)
%	options	see lanspara.m					(string)
%		-plotdim
%		-hold
%
%	_____SEE ALSO___________________________________________________________
%	lans_plotmd
%
%	(C) 1999.11.11 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[]	= lans_plotaxis(ax,center,ptype,options);

[d,n]		= size(ax);

if nargin<4
	options=[];
	if nargin<3
		ptype	= 'g-';
		if nargin<2
			center	= zeros(d,1);
		end
	end
end

mask		= [ones(d,1) zeros(d,1)];
if max(size(center))==1
	center	= (ones(d,1)*center)*ones(1,n);
elseif size(center,2)==1
	center	= center*ones(1,n);
end

axplot	= lans_clone1(ax,2);
plotmask= lans_clone(mask,n);
toplot	= axplot.*plotmask;

[d,n]	= size(toplot);
toplot(:,2:2:end)	= center;
lans_plotmd(toplot,ptype,options);
