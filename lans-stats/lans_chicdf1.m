%	lans_chicdf1	- Chi-square cdf (1-D)
%
%	[cdf]	= lans_chicdf1(x,n)
%
%	_____OUTPUT_____________________________________________________________
%	cdf	cdf @ x						(vector)
%
%	_____INPUT______________________________________________________________
%	x	value	x>0					(vector)
%	n	degrees of freedom				(scalar)
%
%	_____EXAMPLE____________________________________________________________
%	lans_chicdf1(1,1)=0.6827
%
%	_____NOTES______________________________________________________________
%	equivalent to lans_gammacdf1 with
%		alpha	= (n-2)/2
%		beta	= 2
%
%	_____SEE ALSO___________________________________________________________
%	lans_chiicdf1	lans_chipdf1
%
%	(C) 2000.01.21 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [cdf]	= lans_chicdf1(x,n)

cdf	= lans_gammacdf1(x,(n-2)*.5,2);

%cdf	= gammainc(x*.5,n*.5);		%stand alone version
