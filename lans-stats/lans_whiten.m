%	lans_whiten	- Spherize (whiten) data to zero mean and unit covariance
%	
%	[sdata,m,C]	= lans_whiten(data)
%
%	_____OUTPUTS____________________________________________________________
%	sdata		spherized data				(col vectors)
%	m		mean vector of data			(col vector)
%	C		covariance of data			(col vector)
%
%	_____INPUTS_____________________________________________________________
%	data		d-dimensional data			(col vectors)
%
%	_____NOTES______________________________________________________________
%	- Assumes data is multivariate Gaussian
%	- sdata
%		- unit covariance
%		- affine invariant
%		  (orthogonally invariant projections also affine invariant)
%		-  has variables ordered in terms of importance
%		   e.g. 1st variable is PCA 1, etc.
%		- plots look similar to PCA plot, except that each PCA axis is
%		  scaled to unit eigenvalue
%	- spherize data so that
%		- projections have unit variance
%		- orthogonal projections are uncorrelated
%		- allows correlation (linear) among dimensions
%
%	- sdata may not be of full rank!! (<d)
%
%	- code added to ensure that first dimension of eigenvector is >0
%	  this is to take care of the eigs.m discrepencies between matlab and
%	  matcom
%
%	(C) 2000.04.19 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/



function	[sdata,m,C]	= lans_whiten(data)

[d n]		= size(data);

C		= cov(data');
[evector,evalue]= lans_eigsort(C);

V	= (evalue^(-.5))*evector';

m	= mean(data')';
mdup	= m*ones(1,n);
cdata	= data-mdup;
sdata	= V*cdata;
