%	lans_gammapdf1	- Gamma pdf (1-D)
%
%	[pdf]	= lans_gammapdf1(x,alpha,beta)
%
%	_____OUTPUT_____________________________________________________________
%	pdf	pdf @ x						(vector)
%
%	_____INPUT______________________________________________________________
%	x	value	x>0					(vector)
%	alpha	value	alpha>-1				(scalar)
%	beta	value	beta>0					(scalar)
%
%	_____EXAMPLE____________________________________________________________
%	lans_gammapdf1(1,1,1)=0.3679
%	f(x)	=  x^alpha*exp(-x/beta)/[gamma(1+alpha)*beta^(1+alpha)	alpha>-1
%			or
%				x^(k-1)*exp(-x/b)/[gam(k)*b^k]  for x>0			k>0
%
%  E(x)	= kb
%	Var(x)= k(b^2)
%
%	_____NOTES______________________________________________________________
%
%	_____SEE ALSO___________________________________________________________
%	gammacdf1	gammaicdf1
%
%	(C) 2001.03.08 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [pdf]	= lans_gammapdf1(x,alpha,beta)

den	= gamma(1+alpha)*beta^(1+alpha);
pdf	= (x.^alpha).*exp(-x/beta)/den;
