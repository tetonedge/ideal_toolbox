%	lans_chipdf1	- Chi-square pdf (1-D)
%
%	[pdf]	= lans_chipdf1(x,n)
%
%	_____OUTPUT_____________________________________________________________
%	pdf	pdf @ x						(vector)
%
%	_____INPUT______________________________________________________________
%	x	value	x>0					(vector)
%	n	degrees of freedom				(scalar)
%
%	_____EXAMPLE____________________________________________________________
%	chi2pdf1(1,1)=0.2420
%
%	_____NOTES______________________________________________________________
%	equivalent to lans_gammapdf1 with
%		alpha	= (n-2)/2
%		beta	= 2
%
%	_____SEE ALSO___________________________________________________________
%	lans_chi2cdf1	lans_chi2icdf1
%
%	(C) 2000.01.21 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [pdf]	= lans_chi2pdf1(x,n)

pdf	= lans_gammapdf1(x,(n-2)*.5,2);

%num	= exp(-x*.5).*(x.^((n-2)*.5));
%den	= gamma(n*.5)*(2^(n*.5));
%pdf	= num/den;
