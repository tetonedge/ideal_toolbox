%	lans_gausstest	- Standard multivariate Gaussian test (d-D)
%
%	[res]	= lans_gausstest(x,label)
%
%	_____OUTPUT_____________________________________________________________
%	res	result						(vector)
%		result(1)	= Friedman's index	
%		result(2)	= % of points < chi2icdf(0.5,d)
%	_____INPUT______________________________________________________________
%	x	multi-dimensional data				(col vectors)
%	label	display chi-square plot using plot : label	(scalar)
%
%	_____SEE ALSO___________________________________________________________
%	whiten
%
%	_____NOTES______________________________________________________________
%	- the inputs are assumed preprocessed with 0 mean and unit covariance
%	- for demo, call function without parameters
%	- incompatible with MATLAB's legendre.m, use my version; plegendre.m
%
%	(C) 2000.01.21 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [res]	= lans_gausstest(x,label)

if nargin>0
%--------------------	regular
if nargin<2
	label	= 0;
end;

J	= 8;

%----------	set up color table
symboltable		= '.ox+*sdv^<>ph';
nsymbols		= length(symboltable);

colortable		= 'rgbmcyk';
ncolors			= length(colortable);

ctype		= mod(label-1,ncolors)+1;	% cycle thru colors
stype		= mod(label-1,nsymbols)+1;	% cycle thru symbol types
pointcolor	= [colortable(ctype) symboltable(stype)];	

[d,n]	= size(x);
y	= vdist2(x,0);
ys	= sort(y);

jrange	= 1:n;
j	= (jrange - .5)/n;

x2	= lans_chiicdf1(j,d);

IJ	= 0;

for j=1:J
	l	= plegendre(j,(2*lans_chicdf1(y,d)-1));
	isum	= mean(real(l(1,:)));
	IJ	= IJ + (2*j+1)*isum*isum;
end;	

IJ	= IJ/2;

res(1)	= IJ;
res(2)	= length(find(ys<lans_chiicdf1(.5,d)))/n;

if label~=0
	plot(x2,ys,pointcolor);
	grid;
	axis equal;
end;
%--------------------	regular ends
else
%--------------------	demo
disp('running gausstest.m in demo mode');
d	= 8;
n	= 500;
data	= randn(d,n);

res	= lans_gausstest(data,1);
axis square;
title(sprintf('Independent Multivariate Gaussian d=%d n=%d',d,n));

%--------------------	demo ends
end;
