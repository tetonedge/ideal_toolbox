%	lans_basis	- Initialize basis/mixture structure
%
%	[bas]	= lans_basis(type,cen,<name1>,<para1>,<name2>,<para2>,...)
%
%	_____OUTPUTS____________________________________________________________
%	bas	LANS basis structure				(structure)
%		.type	Basis type				(string)
%			'Gaussian-spherical'	
%			'Gaussian-diagonal'	
%			'Gaussian-full'	
%			'Gaussian-clamped'	
%		.cen	centers					(col vectors)
%		.name	name of corresponding para		(lin cell)
%			can be arbitrary
%		.para	cell of parameters para1,para2,...	(lin cell)
%		
%	_____INPUTS_____________________________________________________________
%	type	Type of basis					(string)
%	cen	basis centers					(vectors)
%	<name1>	name of 1st parameter				(string)
%	<para1>	1st parameters					(any)
%	<name2>	name of 2nd parameter				(string)
%	<para2>	2nd parameters					(any)
%	...
%
%	_____EXAMPLE____________________________________________________________
%	bas	= lans_basis('Gaussian-spherical',[0;0],'covariance',1);
%
%	_____NOTES______________________________________________________________
%	- may be used for mixture models, where each basis acts as a
%	  mixture component, whose prior may be individually specified
%	  in the .para subfield
%	- order of optional parameters NOT important, as long as each is
%	  appropriately named (case is not important)
%	- Assuming N bases, the following convention for parameters are used:	
%	- Append the variable name 'unnormalized' for no normalization, e.g.
%	  for Radial Basis Function (RBF) networks
%
%	cen		D x N
%	gaussian-	variance
%	---------	--------
%	spherical	1 x N
%	diagonal	D x N
%	full		D x D x N
%			
%	clamped		variance	alpha	manifold	
%			1 x N		1	D x Q x N
%
%	_____SEE ALSO___________________________________________________________
%	lans_pdf
%
%	(C) 1999.10.13 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________

function	[bas]	= lans_basis(type,cen,varargin)

bas.type	= type;
bas.cen		= cen;
npara		= length(varargin);

if ~lans_iseven(npara)
	error('Each parameter must be preceded by a name string');
else
	bas.name	= varargin(1:2:npara-1);
	bas.para	= varargin(2:2:npara);
end
