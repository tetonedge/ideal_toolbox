%	lans_eigsort	- Sorted eigen-decomposition for MATLAB and compilables
%	
%	[E,D]	= lans_eigsort(dmatrix[,options])
%
%	_____OUTPUTS____________________________________________________________
%	E	Sorted eigenvector				(col vectors)
%	D	Sorted eigenvalue				(matrix)
%
%	_____INPUTS_____________________________________________________________
%	dmatrix	data matrix					(matrix)
%	options							(string)
%		-discard 1	discard zero eigen-pair
%		-largest k	take largest k
%				rank(dmatrix)			(default)
%		-prec x		precision
%
%	_____NOTES______________________________________________________________
%	- sort the eigenvectors to ensure no discrepancies exist between MATLAB
%	  and MATCOM generated code
%	- Also sorts eigenvector/values in descending order (largest first)
%	- set options to '-discard 1' to discard zero eigenvalues
%
%
%	(C) 1996-2003 Kui-yu Chang
%   2003.09.16
%	http://www.lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[E,D]	= lans_eigsort(dmatrix,options)

if nargin<2
	options='';
end

discard	= paraget('-discard',options);
K	= paraget('-largest',options,rank(dmatrix));
prec	= paraget('-prec',options);

OPTIONS.disp=0;
[ev,ed]	= eigs(dmatrix, K, 'LM', OPTIONS);

%-----	this is to ensure that MATLAB and MATCOM eigs.m results matches'
%-----	Find the 1st negative entry and then negates the eigenvector

for i=1:rank(ed)
	e	= ev(:,i);
	found	= 0;
	index	= 0;
	while (~found)&(index<length(e))
		index	 = index+1;
		if e(index)~=0
			found	= 1;
			if e(index)<0			% if first non-zero value
				ev(:,i)	= -ev(:,i);	% is -ve, negate vector
			end
		end
	end	
end

%-----	sort the eigenvalues/vectors
devalue		= diag(ed);
[dumb,ind]	= sort(-devalue);

D	= diag(devalue(ind));
E	= ev(:,ind);

if discard
	keep		= length(find(D>prec));
	D		= D(1:keep);
	E		= E(:,1:keep);
end
