%	lans_likelihood	- Compute complete & average likelihood for mixture model
%
%	[Lc,Lcmean]	= lans_likelihood(Rjn,mix,data)
%
%	_____OUTPUT_____________________________________________________________
%	Lc	Complete likelihood				(scalar)
%	Lcmean	Averaged incomplete likelihood			(scalar)
%
%	_____INPUT______________________________________________________________
%	Rjn	Posterior Matrix (MxN)				(matrix)
%	mix	mixture model					(structure)
%		see gmm.m
%	data	data						(col vectors)
%
%	_____NOTES______________________________________________________________
%	- requires NETLAB gmmactiv.m
%
%	_____SEE ALSO___________________________________________________________
%	gmmactiv
%
%	(C) 1998.11.25 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [Lc,Lcmean]	= lans_likelihood(Rjn,mix,data)

[d,N]	= size(data);
%-----	The average Log Likelihood
pyj	= gmmactiv(mix,data')';

pj	= mix.priors'*ones(1,N);

pjpyj	= lans_replace(eps,1,pj.*pyj);	% remove small values

lnpjpyj	= log(pjpyj);

lc	= Rjn * lnpjpyj';
Lcmean	= sum(sum(lc));

%-----	Bishop's method of computing p(y|j)p(j) sum over j,n
%-----	i.e. sum of log p(y) over n
prob		= mix.priors*pyj;

Lc		= sum(log(prob));
