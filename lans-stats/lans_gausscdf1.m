%	lans_gausscdf1	- Gaussian cdf (1-D)
%
%	[cdf]	= lans_gausscdf1(x[,mu[,sigma]])
%
%	_____OUTPUT_____________________________________________________________
%	cdf	cdf @ x						(vector)
%
%	_____INPUT______________________________________________________________
%	x	value						(vector)
%	mu	mean						(scalar)
%	sigma	standard deviation				(scalar)
%
%	_____EXAMPLE____________________________________________________________
%	lans_gausscdf1(1)=0.8413
%
%	_____SEE ALSO___________________________________________________________
%	lans_gaussicdf1	lans_gausspdf1
%
%	_____NOTES______________________________________________________________
%	In loops this function is slower than
%		precomputing
%			v	= sqrt(2)/2
%		and then computing each
%			.5*erf(x*v)+.5;
%
%	(C) 2000.01.21 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/


function [cdf]	= lans_gausscdf1(x,mu,sigma)

if nargin < 3,
	sigma = 1;
	if nargin < 2;
		mu = 0;
	end
end

cdf = 0.5 * erfc( - (x - mu) / (sigma * sqrt(2)));
