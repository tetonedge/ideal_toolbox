%	lans_pdf	- Compute multiple pdf w.r.t. multiple bases
%
%	[pdf]	= lans_pdf(x,bas<,bias>)
%
%	_____OUTPUTS____________________________________________________________
%	pdf	activation values of x at each basis		(matrix)
%		(arranged in the same topology as x)
%		if x is 5x4 grid, then pdf is Lx5x4
%		if x is DxN, then pdf is LxN
%
%	_____INPUTS_____________________________________________________________
%	x	input vectors					(vectors)
%	bas	set of basis					(structure)
%	bias	add 1 additional row denoting the bias		(scalar)
%
%	_____EXAMPLE____________________________________________________________
%
%	_____NOTES______________________________________________________________
%	- basis in bas must NOT be arranged in any topology, i.e. must be linear
%	- capable of handling
%		bas.type		bas.name
%		--------		--------
%		gaussian-spherical	variance
%		gaussian-diagonal	variance
%		gaussian-full		variance
%		gaussian-clamped	variance alpha omanifold tmanifold
%			- specify either orthogonal (omanifold)
%			  or tangential (tmanifold) column space 
%			- tmanifold or omanifold need not be orthonormal, as
%			  long as they span the space. They will be made
%			  orthonormal here by the matlab function orth
%	- Unnormalized pdf computed if bas contains a 'unnormalized' variable
%
%	_____SEE ALSO___________________________________________________________
%	lans_basis	orth
%
%	(C) 1999.10.14 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________
%	for demo, call function without parameters

function	[pdf]	= lans_pdf(x,bas,bias)
if nargin>0
%__________ REGULAR ____________________________________________________________
if nargin<3
	bias	= 0;
end
nonormal	= lans_findcell('unnormalized',bas.name);

[lx,xdim]	= lans_md2lin(x);	% convert x to col vectors
[D N]		= size(lx);		% N = # locations
[D L]		= size(bas.cen);	% L = # basis

% initialize all activations (no bias)
pdf		= zeros(L+bias,N);

switch(lower(bas.type))
	case 'gaussian-spherical',
		wsig	= lans_findcell('variance',bas.name);
		for l=1:L
			mu	= bas.cen(:,l);
			sig	= bas.para{wsig}(l);
			if nonormal
				pdf(l,:)= lans_gausspdf(lx,mu,sig,nonormal);
			else
				pdf(l,:)= lans_gausspdf(lx,mu,sig);
			end
		end
	case 'gaussian-diagonal',
		wsig	= lans_findcell('variance',bas.name);
		for l=1:L
			mu	= bas.cen(:,l);
			sig	= bas.para{wsig}(:,l);
			if nonormal
				pdf(l,:)= lans_gausspdf(lx,mu,sig);
			else
				pdf(l,:)= lans_gausspdf(lx,mu,sig);
			end
		end
	case 'gaussian-full',
		wsig	= lans_findcell('variance',bas.name);
		for l=1:L
			mu	= bas.cen(:,l);
			sig	= bas.para{wsig}(:,:,l);
			if nonormal
				pdf(l,:)= lans_gausspdf(lx,mu,sig);
			else
				pdf(l,:)= lans_gausspdf(lx,mu,sig);
			end
		end
	case 'gaussian-clamped'
		wsig	= lans_findcell('variance',bas.name);
		walpha	= lans_findcell('alpha',bas.name);
		ovec	= lans_findcell('omanifold',bas.name);	% orthogonal 
		tvec	= lans_findcell('tmanifold',bas.name);	% tangential
		%-----
		alpha	= bas.para{walpha};
		if ovec			% orthogonal specified
			for l=1:L
				mu	= bas.cen(:,l);
				sig	= bas.para{wsig}(l);
				O	= bas.para{ovec}(:,:,l);
				% ensure orthonomality
				O	= orth(O);
				EET	= eye(size(O,1))-O*O';
				if nonormal
					pdf(l,:)= lans_gausspdf(lx,mu,sig,nonormal,alpha,EET);
				else
					pdf(l,:)= lans_gausspdf(lx,mu,sig,alpha,EET);
				end
			end
		elseif tvec		% tangential specified
			for l=1:L
				mu	= bas.cen(:,l);
				sig	= bas.para{wsig}(l);
				T	= bas.para{tvec}(:,:,l);
				% ensure orthonomality
				T	= orth(T);
				EET	= T*T';
				if nonormal
					pdf(l,:)= lans_gausspdf(lx,mu,sig,nonormal,alpha,EET);
				else
					pdf(l,:)= lans_gausspdf(lx,mu,sig,alpha,EET);
				end
			end
		else
			error('Either tmanifold or omanifold must be specified for Gaussian-clamped basis');
		end
	otherwise,
		error('Unknown basis type');
end

if bias
	pdf(L+1,:)	= bias*ones(1,N);
end

pdf	= lans_lin2md(pdf,xdim);

%__________ REGULAR ends _______________________________________________________
else
%__________ DEMO _______________________________________________________________
clf;clc;
disp('running lans_pdf.m in demo mode');
disp('Demo unimplemented');
%__________ DEMO ends __________________________________________________________
end
