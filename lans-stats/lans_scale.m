%	lans_scale	- Scale data to lie within specified range
%	
%	[sdata]	= lans_scale(data<minv,maxv>)
%
%	_____OUTPUTS____________________________________________________________
%	sdata	scaled data				(col vectors)
%
%	_____INPUTS_____________________________________________________________
%	data	D-dimensional data			(col vectors)
%	minv	minimum value				(scalar)
%		default		-1
%	maxv	maximum value				(scalar)
%		default		1
%
%	_____NOTES______________________________________________________________
%
%	(C) 1999.08.10 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/


function	[sdata]	= lans_scale(data,minv,maxv)

if nargin<3
	maxv	= 1;
	if nargin<2
		minv	= -1;
	end
end

[D N]	= size(data);

ub	= max(data')';
lb	= min(data')';

a	= (maxv-minv)./(ub-lb);
b	= maxv-a.*ub;

sdata	= (a*ones(1,N)).*data + b*ones(1,N);
