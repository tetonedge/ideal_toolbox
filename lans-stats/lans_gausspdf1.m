%	lans_gausspdf1	- Evaluates Gaussian pdf (1-D) at multiple scalar points
%
%	[pdf]	= lans_gausspdf1(x<,mu><,sigma>)
%
%	_____OUTPUT_____________________________________________________________
%	pdf	pdf @ x						(vector)
%
%	_____INPUT______________________________________________________________
%	x	value						(vector)
%	mu	mean						(scalar)
%		0	default
%	sigma	standard deviation				(scalar)
%		1	default
%
%	_____EXAMPLE____________________________________________________________
%	lans_gausspdf1(1)=0.2420
%
%	_____SEE ALSO___________________________________________________________
%	gausscdf1	gaussicdf1
%
%	(C) 1999.08.01 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [pdf]	= lans_gausspdf1(x,mu,sigma)

if nargin < 3,
	sigma = 1;
	if nargin < 2;
		mu = 0;
	end
end

fac	= 1/(sqrt(2*pi)*sigma);
ev	= -((x-mu).^2)/(2*sigma*sigma);
pdf	= fac*exp(ev);
