%	lans_stand	- Standardize data to zero mean and unit variance
%	
%	[sdata,m,s]	= lans_stand(data)
%
%	_____OUTPUTS____________________________________________________________
%	sdata		standardized data			(col vectors)
%	m		mean vector of data			(col vector)
%	s		standard deviation vector of data	(col vector)
%
%	_____INPUTS_____________________________________________________________
%	data		d-dimensional data			(col vectors)
%
%	_____NOTES______________________________________________________________
%	Assumes dimensions are mutually independent (and Gaussian) by
%	standardizing each dimensions individually
%
%	(C) 1999.08.28 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[sdata,m,s]	= lans_stand(data)

[d n]	= size(data);

m	= mean(data')';
s	= std(data')';

cdata	= data-m*ones(1,n);		%centered @ mean

sdata	= cdata./(s*ones(1,n));		%and zero variance
