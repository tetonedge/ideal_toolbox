%	lans_gammacdf1	- Gamma cdf (1-D)
%
%	[cdf]	= lans_gammacdf1(x,alpha,beta)
%
%	_____OUTPUT_____________________________________________________________
%	cdf	cdf @ x						(vector)
%
%	_____INPUT______________________________________________________________
%	x	value	x>0					(vector)
%	alpha	value	alpha>-1				(scalar)
%	beta	value	beta>0					(scalar)
%
%	_____EXAMPLE____________________________________________________________
%	lans_gammacdf1(1,1,1)=
%
%	_____SEE ALSO___________________________________________________________
%	gammaicdf1	gammapdf1
%
%	(C) 2000.01.21 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [cdf]	= lans_gammacdf1(x,alpha,beta)

cdf = gammainc(x/beta,alpha+1);
