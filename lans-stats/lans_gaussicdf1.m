%	lasn_gaussicdf1	- Inverse Gaussian cdf (1-D)
%
%	[x]	= lans_gaussicdf1(p[,mu[,sigma]])
%
%	_____OUTPUT_____________________________________________________________
%	x	x value giving cdf p				(vector)
%
%	_____INPUT______________________________________________________________
%	p	percentile	0<p<1				(vector)
%	mu	mean						(scalar)
%		0						default
%	sigma	standard deviation				(scalar)
%		1						default
%
%	_____NOTES______________________________________________________________
%	Using MATCOM's own erfinv function won't work on Linux (xena)
%	need to use MATLAB's own erfinv.m code, named erfinv2.m in stats/
%
%	_____EXAMPLE____________________________________________________________
%	lans_gaussicdf1(.95)=1.6449
%
%	_____SEE ALSO___________________________________________________________
%	lans_gausscdf1	lans_gausspdf1
%
%	(C) 2000.01.21 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [x]	= lans_gaussicdf1(p,mu,sigma)

x	= zeros(size(p));
wzero	= find(p==0);
x(wzero)= zeros(size(wzero));
wones	= find(p==1);
x(wones)= ones(size(wones))*inf;
fullx	= x;
therest	= find(p~=0&p~=1);
prest	= p(therest);
	
if ~any(prest)
	return;
end

if nargin < 3,
	sigma = 1;
	if nargin < 2;
		mu = 0;
	end
end

x	= sqrt(2)*sigma*erfinv2(2*prest-1)+mu;

xin	= x;
x	= fullx;
x(therest)=xin;
