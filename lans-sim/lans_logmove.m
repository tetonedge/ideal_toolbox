%	lans_logmove	- Move log file to 'done' directory indicates completion
%
%	[status,result]	= lans_logmove(logfile[,spath])
%
%	_____OUTPUTS____________________________________________________________
%	status	0 if successful					(binary)
%	result	result from command
%
%	_____INPUTS_____________________________________________________________
%	logfile	log filename (NO PATH)				(string)
%	spath	root simulation path				(string)
%		defaults to pwd
%
%	_____NOTES______________________________________________________________
%	- '.log' will be appended to logfile if unspecified in logfile
%	- '$HNAME' will be prepended to logfile, no need to specify
%	- works only for UNIX
%	- no action under other platforms
%	- assumes that logfile resides at spath/log, to be moved to
%	  spath/log/done
%
%	_____SEE ALSO___________________________________________________________
%	lans_log lans_matmove
%
%	(C) 1999.06.03 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________

function [status,result]= lans_logmove(logfile,spath)

s	= filesep;
if nargin<2
	spath	= 'log';
else
	spath	= [spath s 'log'];
end

hostname	= getenv('HNAME');
if ~isempty(hostname)
	hostname	= [hostname '_'];
elseif isunix
	hostname	= [unix('hostname -s') '_'];
end

suffix	= '.log';
if ~strcmp(logfile(end-3:end),suffix)
	logfile	= [spath s hostname logfile suffix];
else
	logfile	= [spath s hostname logfile];
end

%_____	Move logfile to 'done' directory
if isunix
	com	= ['mv ' logfile ' ' spath s 'done'];
	[status,result]	= unix(com);
else
	status	= 0;
end
