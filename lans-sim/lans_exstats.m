%	lans_exstats	- Compute statistics for an ex structure	
%
%	[ex]	= lans_exstats(ex)
%
%	_____OUTPUTS____________________________________________________________
%	ex	Experiment data structure			(structure)
%		see lans_exinit.m
%
%	_____INPUTS_____________________________________________________________
%	ex	Experiment data structure			(structure)
%		see lans_exinit.m
%
%	_____NOTES______________________________________________________________
%
%	_____SEE ALSO___________________________________________________________
%	lans_expush	lans_exinit
%
%	(C) 1999.06.05 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________

function [ex]	= lans_exstats(ex)

ex.log	= '';
for m=1:ex.nrec
	ex.krec_avg{m}	= mean(ex.krec{m});
	ex.krec_std{m}	= std(ex.krec{m});
	if ex.klog(m)
		ex.log	= [ex.log sprintf(ex.logprec{m},ex.krec_avg{m}(end))];
	end
end
