%	lans_log	- Append log file
%
%	[result]	= lans_log(lstr,logfile<,spath><,mode>)
%
%	_____OUTPUTS____________________________________________________________
%	result	1 if successful					(binary)
%
%	_____INPUTS_____________________________________________________________
%	lstr	string to write					(string)
%	logfile	filename to append lstr to			(string)
%	spath	root simulation path				(string)
%		defaults to pwd
%	mode	file open mode (see fopen)			(string)
%		{w,r,a*}
%		* default
%
%	_____NOTES______________________________________________________________
%	- creates log file in {pwd}/log
%	- log file in the form of
%		{hostname}_{logfile}.log
%	  where
%		hostname:	short name of machine
%		logfile	: 	supplied logfile name
%	- creates a new file if lstr is empty
%	- under UNIX, writes to file [getenv('HNAME') logfile '.log']
%	- each logged line is preceded by time-stamp, e.g.
%		Mon 23:19] 
%	- use unix('date ...') for LINUX because linux MATLAB gives wrong time 
%	- a linefeed is added automatically, NO need to manually add linefeed
%	- logfile is named {logfile}.log
%
%	_____SEE ALSO___________________________________________________________
%	lans_exinit lans_expush
%
%	(C) 1999.12.05 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________

function [result]	= lans_log(lstr,logfile,spath,mode)

s	= filesep;
if nargin<4
	mode	= 'a';
	if nargin<3
		spath	= pwd; 
	end
end
spath	= [spath s 'log'];

hostname	= getenv('HNAME');
if ~isempty(hostname)
	hostname	= [hostname '_'];
end

suffix	= '.log';
if ~strcmp(logfile(end-3:end),suffix)
	logfile	= [spath s hostname logfile suffix];
else
	logfile	= [spath s hostname logfile];
end

%_____	Compute date
if strcmp(computer,'LNX86')
	%_____	uses UNIX date because LINUX MATLAB returns incorrect date
	[s,timestamp]	= unix('date +"%a %H:%M"');
	timestamp	= timestamp(1:end-1);	%kills linefeed
else
	timestamp	= [datestr(now,'ddd') ' ' datestr(now,'HH:MM')];
end

%_____	Write lines
fid	= fopen(logfile,mode);
fprintf(fid,'%s] %s\n',timestamp,lstr);
fclose(fid);

result	= 1;
