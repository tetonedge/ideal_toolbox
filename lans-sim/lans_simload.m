%	lans_simload	- Plot results of lans_sim saved data
%
%	[lsim,lvar,vhash,sex]	= lans_simload(dfile)
%
%	_____OUTPUTS____________________________________________________________
%	lsim,lvar,vhash,sex					(structure)
%		see lans_sim.m
%
%	_____INPUTS_____________________________________________________________
%	dfile	Full/relative path data file			(string)
%
%	_____EXAMPLE____________________________________________________________
%
%	_____NOTES______________________________________________________________
%	- currently only loads file
%	
%	- append '.mat' if not already specified
%	- look for dfile in pwd, mat/, mat/done/
%	- print error message if file not found in any of the above locations
%
%	_____SEE ALSO___________________________________________________________
%	lans_sim
%
%	(C) 1999.06.14 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________
%	system of plotting different values

function	[lsim,lvar,vhash,sex]	= lans_simload(dfile)

%_____	Append .mat if not specified
if isempty(findstr(dfile,'.mat'))
        dfile   = [dfile '.mat'];
end

%_____	Check if dfile exists
if isempty(dir(dfile))
	vfile	= ['mat/' dfile];	
	if isempty(dir(vfile))
		vfile	= ['mat/done/' dfile];
		if isempty(dir(vfile))
			error('File not found in pwd, mat/ or mat/done ');
		end
	end
else
	vfile	= dfile;
end

load(vfile);		%_____	load verified file/path
