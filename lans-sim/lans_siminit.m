%	lans_siminit	- Initialize empty LANS simulation (lsim) structure
%
%	[lsim]	= lans_siminit(path,prefix,ExternalProcedure)
%
%	_____OUTPUTS____________________________________________________________
%	lsim	LANS simulation structure			(struct)
%	.path		simulation root	path			(string)
%	.prefix		prefix for data & log data filenames	(string)
%	.matfile	full path/filename of data file		(string)
%	.p_ext		external flow control .m program	(string)
%	.readme		information string			(string)
%
%	_____INPUTS_____________________________________________________________
%	ExternalProcedure	Name of external procedure	(string)
%
%	_____EXAMPLE____________________________________________________________
%
%
%	_____NOTES______________________________________________________________
%	- set inputs to [] if default desired
%
%	_____SEE ALSO___________________________________________________________
%	lans_ex
%
%	(C) 1999.06.04 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________
%	create template p_ext.m

function [lsim]	= lans_siminit(lpath,lprefix,ExternalProcedure)

s	= filesep;

lsim.path	= lans_assign(lpath,pwd);
lsim.prefix	= lans_assign(lprefix,'');
lsim.matfile	= [lsim.path s 'mat' s lsim.prefix '.mat'];
lsim.p_ext	= [];
lsim.readme	= [];


if exist(ExternalProcedure)==2
	lsim.p_ext	= ExternalProcedure;
else
	estr	= sprintf('External procedure %s not found in working directory or path',ExternalProcedure);
	error(estr);
end
