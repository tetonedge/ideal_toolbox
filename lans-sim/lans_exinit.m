%	lans_exinit	- Initialize experiment (ex) structure
%
%	[ex]	= lans_exinit(logvar[,k[,nrun[,pstring]]])
%
%	_____OUTPUTS____________________________________________________________
%	ex	LANS experiment data structure			(structure)
%		.str	setup string				(string)
%			see lanspara.m
%		.nrec	# variables logged			(scalar)
%		.nrun	# lines/runs logged			(scalar)
%		.nk	# epochs/iterations logged		(scalar)
%		.k	list of snapshot epochs/iterations	(row vector)
%		.mon	variable to monitor for early stopping	(string)
%		.moni	index of mon				(scalar)
%		.kout	names of output variables to log	(cell matrix)
%		.klog	log tags (default all 1's)		(row vector)
%			1 for a variable => it will be logged
%		.loghead	log header precision		(cell)
%		.logprec	log precision			(cell)
%		.head	header for logged .krec_avg variables	(string)
%		.log	stable values of .krec_avg variables	(string)
%			based on .klog mask
%		.krec	cell of matrices			(row cell)
%			each matrix is R x K x D
%				R	# of runs
%				K	# of epoch snapshots
%				D	Dimensionality of kout
%		.krec_avg
%			average .krec over all runs 	(row cell)
%			each matrix is 1 x K x D
%		.krec_std
%			std dev. of .krec over all runs	(row cell)
%			each matrix is 1 x K x D
%		.pos	current position pos(1)=run pos(2)=k	(vector)
%								(scalar)
%
%	_____INPUTS_____________________________________________________________
%	kout	cell of variable names to log			(string)	
%	logvar	variable to log					(structure)
%		.name	names of variable			(cell)
%		.hprec	precision of header string		(cell)
%		.prec	precision of values			(cell)
%		.use	turns log on/off for selected variables	(row vector)
%	k	list of snapshot epochs/iterations		(row vector)
%		1:25	default
%	nrun	number of runs					(scalar)
%		10	default
%	pstring	setup parameter string				(string)
%		-tol	tolerance level
%		-mon	name of variable to monitor
%
%	_____EXAMPLE____________________________________________________________
%	Consider an experiment
%		- repeated 10 times/runs
%		- each run allowed up to 100 epochs and snapshots are taken
%		  at 10 epoch intervals
%		- At each snapshot, the following values are recorded
%			'mse', 'smoo', 'tmse'
%	lvar.name	= {'mse','tmse','R'};
%	lvar.hprec	= {'%6s ','%6s ','%6s '};
%	lvar.prec	= {'%0.4f ','%0.4f ','%3.2f '};
%	lvar.use	= [1 1 1];
%	lans_exinit(lvar,0:10:100,10)
%		returns
%	     str: 'parameter string to be recorded'
%	    nrec: 3
%	    nrun: 10
%	      nk: 11
%	       k: [0 10 20 30 40 50 60 70 80 90 100]
%	     mon: 'not set yet'
%	    moni: 0
%	    kout: {1x3  cell  }
%	    klog: [1 1 1]
%           head: '   mse   tmse      R '
%            log: '0.0000 0.0000 0.00 '
%        loghead: {1x3  cell  }
%        logprec: {1x3  cell  }
%            log: {1x3  cell  }
%	    krec: {1x3  cell  }
%	krec_avg: {1x3  cell  }
%	krec_std: {1x3  cell  }
%	     pos: [1 1]
%
%	_____NOTES______________________________________________________________
%	- ex.pos used only by lans_expush
%	- set '-mon var' to use var for early stopping w.r.t. -tol TOL
%
%	_____SEE ALSO___________________________________________________________
%	lans_expush
%
%	(C) 1999.06.05 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________

function [ex]	= lans_exinit(logvar,k,nrun,pstring)

NAME	= 1;
LOG	= 2;

if nargin <4
	pstring	= '';
	if nargin<3
		nrun	= 10;
		if nargin<2
			k	= 1:25;
		end
	end
end

nrec	= length(logvar.name);
nk	= length(k);

ex.str	= 'parameter string to be recorded';
ex.nrec	= nrec;
ex.nrun	= nrun;
ex.nk	= length(k);
ex.k	= k;
if ~isempty(pstring)
	ex.mon	= paraget('-mon',pstring);
	i=1;
	while i<ex.nrec
		thestr	= char(logvar.name{i});
		if strcmp(ex.mon,thestr)
			break
		else
			i = i+1;
		end
		if i>ex.nrec
			error('Specified monitor variable unavailable');
		end
	end
	ex.moni	= i;
	ex.str	= pstring;
else
	ex.mon	= 'not set yet';
	ex.moni	= 0;
end
ex.kout	= logvar.name;
ex.klog	= logvar.use;

hstr	= '';lstr	= '';
ex.head	= hstr;
ex.log	= lstr;
for m=1:nrec
	ex.loghead{m}	= logvar.hprec{m};
	ex.logprec{m}	= logvar.prec{m};
	ex.krec{m}	= zeros(nrun,nk);
	ex.krec_avg{m}= zeros(1,nk);
	ex.krec_std{m}= zeros(1,nk);
	if ex.klog(m)
		hstr	= [hstr sprintf(ex.loghead{m},ex.kout{m})];
		lstr	= [lstr sprintf(ex.logprec{m},ex.krec_avg{m}(end))];
	end
end
ex.head	= hstr;
ex.log	= lstr;

ex.pos	= [1 1];
