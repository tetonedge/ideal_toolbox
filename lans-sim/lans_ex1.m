%	lans_ex1	- Run 1 experiment (repeated K times)
%
%	[ex]	= lans_ex1(ex,lsim[,ldata])
%
%	_____OUTPUTS____________________________________________________________
%	ex	filled ex structure				(structure)
%
%	_____INPUTS_____________________________________________________________
%	ex	initialized empty ex structure			(structure)
%	lsim	LANS sim structure				(structure)
%		see lans_siminit
%	ldata	LANS data structure				(structure)
%		see lans_load
%
%	_____NOTES______________________________________________________________
%	- Important global variables to utilize in in-line functions:
%		ldata		LANS data structure		(structure)
%		r		current run			(scalar)
%		k		current iteration		(scalar)
%		ex		current ex structure		(struct)
%	- ldata need not be supplied, determined by init stage
%
%	_____SEE ALSO___________________________________________________________
%	lans_exinit lans_siminit lans_expush lans_load
%
%	(C) 1999.06.06 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________

function [ex]	= lans_ex1(ex,lsim,ldata)

thecom	= ['run ' lsim.p_ext];
thecom	= [lsim.p_ext];

for r=1:ex.nrun	
	%_____	Manipulate Data and initialize any structure
	exstage	= 'init';eval(thecom);

	%_____	b4 iteration
	exstage	= 'test';eval(thecom);
	for m=1:ex.nrec
		results{m}	= eval(ex.kout{m});
	end
	ex	= lans_expush(0,results,ex);

	%_____	iterate upto K times
	for k=1:ex.k(end)
		exstage	= 'train';eval(thecom);
		exstage	= 'validate';eval(thecom);
		exstage = 'test';eval(thecom);
		exstage	= 'debug';eval(thecom);

		for m=1:ex.nrec
			results{m}	= eval(ex.kout{m});
		end
		[ex,stop]	= lans_expush(k,results,ex);
		if stop, ex	= lans_exfill(ex);break;end;
	end
	exstage	= 'save';
	eval(['run ' lsim.p_ext]);
end	%r

%_____	Compute Stats and log results
ex	= lans_exstats(ex);
exstage	= 'save';eval(thecom);
exstage	= 'log';eval(thecom);
