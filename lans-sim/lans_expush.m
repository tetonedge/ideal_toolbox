%	lans_expush	- Push data onto an ex structure 
%
%	[ex,stop]	= lans_expush(k,cdata,ex)
%
%	_____OUTPUTS____________________________________________________________
%	ex	Experiment data structure			(structure)
%		see lans_exinit.m
%	stop	If stopping is warranted			(binary)
%
%	_____INPUTS_____________________________________________________________
%	k	Current iteration/epoch				(scalar)
%	cdata	Cell of data					(row cell)
%	ex	Experiment data structure			(structure)
%		see lans_exinit.m
%
%	_____NOTES______________________________________________________________
%	- cdata is pushed <=> k is in ex.k
%	- assumes an up-to-date and ordered ex.pos
%	- cdata MUST contain the same number & order of data as kept in ex
%	- for stopping ex.pstring must contain fields
%		-tol
%		-mon
%		-moni
%
%	_____SEE ALSO___________________________________________________________
%	lans_exinit	lans_exfill
%
%	(C) 1999.06.04 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________

function [ex,stop]	= lans_expush(k,cdata,ex)

if isempty(find(ex.k==k))
	stop	= 0;
	return;		% not time for log yet
end

if length(cdata)~=ex.nrec
	error('Number of supplied cell data not equal to that in record');
end

%_____	put data into current position
pos	= ex.pos;
for rec=1:ex.nrec
	ex.krec{rec}(pos(1),pos(2))	= cdata{rec};
end

%_____	check for early stopping if specified/desired
%	at least on 2nd epoch & not already on last epoch
if (nargout>1)&(pos(2)>1)&(pos(2)<ex.nk)
	if ~ex.moni
		error('Stopping/monitoring variable not set!');
	end
	tol	= paraget('-tol',ex.str);	
	oldv	= ex.krec{ex.moni}(pos(1),pos(2)-1);
	thev	= abs(cdata{ex.moni}/oldv-1);
	if thev<tol
		stop	= 1;
	else
		stop	= 0;
	end
else
	stop	= 0;
end

%_____	increment position
pos(2)	= pos(2)+1;
if pos(2)>ex.nk
	pos(1)	= pos(1)+1;
	pos(2)	= 1;
end
ex.pos	= pos;
