%	lans_sim	- Run experiments with different parameters
%
%	[sex]	= lans_sim(lsim,lvar,ex,ldata)
%
%	_____OUTPUTS____________________________________________________________
%	sex	Simulated ex array structure 			(struct-array)
%
%	_____INPUTS_____________________________________________________________
%	lsim	LANS sim structure				(structure)
%		see lans_siminit
%	lvar	LANS variable structure				(structure)
%		see lans_varinit
%	ex	initialized empty ex structure			(structure)
%		see lans_exinit
%	ldata	LANS data structure				(structure)
%		see lans_load
%
%	_____NOTES______________________________________________________________
%	- error recovery based on existence of a previously saved .mat file
%	  containing vhash counter structure
%
%	_____SEE ALSO___________________________________________________________
%	lans_siminit lans_exinit lans_hashinit lans_varinit lans_load
%
%	(C) 1999.10.21 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________
%	incorporate generic skipping conditions 

function [sex]	= lans_sim(lsim,lvar,ex,ldata)

sep	= ' | ';
%__________	Fault recovery
if length(dir(lsim.matfile))			% looks for old .mat file
	eval(['load ' lsim.matfile]);		% load previous variables
	vstart	= vhash.apoint+1;		% start from saved point+1
	vhash	= lans_hashinc(vhash);		% "	
	lstr	= 'Recovered from crash...';
	lans_log(lstr,lsim.prefix,lsim.path,'a');	% write logfile header
else
	vhash	= lans_hashinit(lvar.list{:});		% starts afresh
	vstart	= 1;

	sex(vhash.max{:})	= ex;			% init multi-D ex
	lstr    = [sprintf(cat(2,lvar.hprec{:}),lvar.label{:}) sep ex.head];
	lans_log(lstr,lsim.prefix,lsim.path,'w');	% write logfile header
end

%__________	Loop
for v = vstart:vhash.amax
	%_____	skip if first 2 are equal (special case ONLY)
	skip	= vhash.read{1}==vhash.read{2};

	if ~skip
	%_____	set parameters
	for p=1:length(lvar.name)
		ex.str	= paraset(['-' lvar.name{p}],vhash.read{p},ex.str);
	end

	%_____	run 1 experiment (ex.nrun runs)
	tex			= lans_ex1(ex,lsim,ldata);

	%_____	save matfile
	sex(vhash.point{:})	= tex;
	eval(['save ' lsim.matfile ' sex lsim lvar vhash']);

	%_____	save logfile
	lstr	= [sprintf(cat(2,lvar.prec{:}),vhash.read{:}) sep tex.log];
	lans_log(lstr,lsim.prefix,lsim.path,'a');	% append logfile
	
	end

	%_____	increment experiment (new set of parameters)
	if v<vhash.amax
		vhash	= lans_hashinc(vhash);	
	end
end

%__________	Indicate completion by moving logfile & matfile to /done
lans_logmove(lsim.prefix,lsim.path);
lans_matmove(lsim.prefix,lsim.path);
