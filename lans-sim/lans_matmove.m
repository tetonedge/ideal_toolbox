%	lans_matmove	- Move mat file to 'done' directory
%
%	[status,result]	= lans_matmove(matfile[,spath])
%
%	_____OUTPUTS____________________________________________________________
%	status	0 if successful					(binary)
%	result	result from command
%
%	_____INPUTS_____________________________________________________________
%	matfile	data filename (NO PATH!)			(string)
%	spath	root simulation path				(string)
%		defaults to pwd
%
%	_____NOTES______________________________________________________________
%	- '.mat' will be appended if unspecified in matfile
%	- works only for UNIX
%	- no action under other platforms
%	- assumes that matfile resides at spath/mat, to be moved to
%	spath/mat/done
%
%	_____SEE ALSO___________________________________________________________
%	lans_logmove
%
%	(C) 1999.06.04 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________

function [status,result]= lans_matmove(matfile,spath)

s	= filesep;
if nargin<2
	spath	= 'mat';
else
	spath	= [spath s 'mat'];
end

suffix	= '.mat';
if ~strcmp(matfile(end-3:end),suffix)
	matfile	= [spath s matfile suffix];
else
	matfile	= [spath s matfile];
end

%_____	Move matfile to 'done' directory
if isunix
	com		= ['mv ' matfile ' ' spath s 'done'];
	[status,result]	= unix(com);
else
	status	= 0;
end
