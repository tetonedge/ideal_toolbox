%	lans_exfill	- Fill remaining cols of a run with stopped value
%
%	[ex]	= lans_exfill(ex)
%
%	_____OUTPUTS____________________________________________________________
%	ex	Experiment data structure			(structure)
%		see lans_exinit.m
%
%	_____INPUTS_____________________________________________________________
%	ex	Experiment data structure			(structure)
%		see lans_exinit.m
%
%	_____NOTES______________________________________________________________
%	- uses ex.pos to determine current position
%	- pos is reset to next run 1st position after fill
%	- assumes 1-D record values
%
%	_____SEE ALSO___________________________________________________________
%	lans_expush	lans_exinit
%
%	(C) 1999.06.04 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________

function [ex]	= lans_exfill(ex)

crun	= ex.pos(1);
cpos	= ex.pos(2);

flen	= ex.nk-cpos+1;

for rec=1:ex.nrec
	fval				= ex.krec{rec}(crun,cpos-1);
	ex.krec{rec}(crun,cpos:end)	= fval*ones(1,flen);
end

ex.pos	= [crun+1 1];
