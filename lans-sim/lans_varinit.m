%	lans_varinit	- Initialize LANS variable structure
%
%	[lvar]	= lans_varinit(var1,var2,...)
%
%	_____OUTPUTS____________________________________________________________
%	lvar	LANS multiple variables information structure	(structure)
%		.name	Actual names of variables		(cell)
%		.label	Labels, overiding .names for logging	(cell)
%		.hprec	precision for printing headers		(cell)
%		.prec	precision for printing values		(cell)
%		.list	spans/values of each variables		(cell)
%		.use	selects variables to log		(row vector)
%
%	_____INPUTS_____________________________________________________________
%	var1	names of variables				(string)
%	var2
%	...
%
%	_____EXAMPLE____________________________________________________________
%	variant	= lans_varinit('mse','tmse','trmse')
%
%	_____NOTES______________________________________________________________
%	- container for a set of variables
%	- useful for varying variable values
%
%	_____SEE ALSO___________________________________________________________
%	lans_exinit	lans_sim
%
%	(C) 1999.06.06 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________

function [lvar]  = lans_varinit(varargin)

N	= length(varargin);

lvar.name		= varargin;
lvar.label		= varargin;
[lvar.hprec{1:N}]	= deal('%6s ');
[lvar.prec{1:N}]	= deal('%1.4f ');
[lvar.list{1:N}]	= deal(0);
lvar.use		= ones(1,N);

