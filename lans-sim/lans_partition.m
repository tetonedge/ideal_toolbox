%	lans_partition	- Partition LANS data to train/validate/test sets
%
%	[pdata]	= lans_partition(ldata,frac<,options><,priors>)
%
%	_____OUTPUTS____________________________________________________________
%	pdata	Partitioned data				(cell/cell)
%		pdata{1}	training data			cell/vector
%		pdata{2}	validation data			cell/vector
%		pdata{3}	test data			cell/vector
%
%	_____INPUTS_____________________________________________________________
%	ldata	general LANS data format			(structure)
%		.in						col vectors
%		.out						col vectors
%
%	frac	fraction of train, validation, test sets	(vector)
%		[trainfrac validationfrac testfrac]
%		will be normalized to sum to 1
%		for holdout frac = [hidx K] where
%			hidx	= holdout partition idx
%				1 <= hidx <= ceil(N/k);
%			K	= holdout size
%
%	options	format options					(string)
%		-part	partition options
%			
%		 	random*		random sampling without replacement
%		 	stratify	random sampling within each class
%					ensures equal ratio of classes
%			holdout		Leave K samples as test (deterministic)
%					e.g. for 10 samples, frac=[4 2] means
%					holding the 5th and 6th sample as test,
%					and the rest as training
%					K=1 generalizes to Leave-1-Out
%
%		* default
%
%	priors	prior probability distribution of classes
%
%	_____EXAMPLE____________________________________________________________
%
%
%	_____NOTES______________________________________________________________
%	- frac can be in fraction or percentage
%	for demo, call function without parameters
%
%	_____SEE ALSO___________________________________________________________
%	lans_load	lans_group
%
%
%	(C) 1999.12.02 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________
%	ensure at least 1 sample of each class in each partition
%	allow the saving of current state, and using given state to generate
%	samples, and later recovering the current state
%	copy over everyfield of ldata to pdata{1:3}
%	different priors

function	pdata = lans_partition(ldata,frac,options,priors)

if nargin>0
%__________ REGULAR ____________________________________________________________
if nargin<3, options=[];end;

part	= paraget('-part',options,'random');	
groupit	= paraget('-group',options,0);

[D N]	= size(ldata.in);
[Q N]	= size(ldata.out);

nfrac	= frac/sum(frac);	% normalized fractions

%_____	permute
switch(lower(part))
	case 'random',
		idx1	= 1;
		%_____	figure out portions
		por	= nfrac*N;
		for r=1:3
			aidx	= sum(por(1:r));
			idx2	= floor(aidx);
			idx2	= min(idx2,N);
			range{r}= idx1:idx2;
			idx1	= ceil(aidx)+lans_isint(aidx);
		end
		ridx	= randperm(N);
		for p=1:3
			pdata{p}.in	= ldata.in(:,ridx(range{p}));
			pdata{p}.out	= ldata.out(:,ridx(range{p}));
		end
	case 'stratify',
		% recursively call itself to partition each class!
		gdata	= lans_group(ldata.in,ldata.out,'-wastespc 1');
		nclass	= length(gdata);

		coptions= paraset('-part','random',options);
		if ~exist('priors')
			priors	= 1/nclass;
		end
		dum.in	= [];
		dum.out	= [];
		pdata	= {dum,dum,dum};
		for c=1:nclass			% for each class
			clear tdata;
			tdata	=lans_partition(gdata{c},nfrac,coptions,priors);
			for p=1:3
				if ~isempty(tdata{p})
					pdata{p}.in	= [pdata{p}.in tdata{p}.in];
					pdata{p}.out	= [pdata{p}.out tdata{p}.out];
				else
					pdata{p}	= [];
				end
			end
		end
	case 'holdout'
		hidx		= frac(1);
		K		= frac(2);	% hold-out size
		if (hidx<1)|(hidx*K>N)
			error('holdout index exceed data size');	
		end
		s1		= (hidx-1)*K+1;
		s2		= min(N,(hidx-1)*K+K);

		inrange		= [1:s1-1 s2+1:N];
		pdata{1}.in	= ldata.in(:,inrange);
		pdata{1}.out	= ldata.out(:,inrange);
		pdata{3}.in	= ldata.in(:,s1:s2);
		pdata{3}.out	= ldata.out(:,s1:s2);
end	% switch(lower(part))

% nullify zero partitiions
for p=1:3
	if ~isempty(pdata{p})
		if length(pdata{p}.in)==0
			pdata{p}	= [];
		end
	end
end

%__________ REGULAR ends _______________________________________________________
else
%__________ DEMO _______________________________________________________________
clf;clc;
disp('running lans_partition.m in demo mode');
data	= lans_load('iris');
pdata	= lans_partition(data,[.8 .1 .1],'-part stratify');
gdata	= lans_group(pdata{1});
lans_plot(gdata,'-legend 1 -colors uniform');
%__________ DEMO ends __________________________________________________________
end
