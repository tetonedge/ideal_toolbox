%	lans_imgscale	- Scale and center a binary image w.r.t. bounding shape
%
%	[sbimg,scale]	= lans_imgscale(bimg)
%			  lans_imgscale(bimg,bshape)
%			  lans_imgscale(bimg,bshape,res)
%
%	_____OUTPUTS____________________________________________________________
%	sbimg	scaled binary image 0/1				(binary matrix)
%	scale	fraction of scaling used to achieve invariance	(scalar)
%
%	_____INPUTS_____________________________________________________________
%	bimg	binary image 0/1				(binary matrix)
%		1	denoting object
%		0	denoting background
%	bshape	bounding shape					(string)
%		'disc'			default
%		'square'
%	res	resolution of final scaled image		(scalar)
%		128	default
%
%	_____NOTES______________________________________________________________
%	- works as follows
%	  1 find centroid of object (1's)
%	  2 bound object with smallest bounding-shape centered at it's centroid
%		e.g. for the 'disc' bounding shape, draw the smallest circle
%		centered @ object's centroid completelycontaining the object
%		* an efficient search method based on the bounding box is used
%	  3 scale bounding-shape to desired resolution, thereby scaling the
%	    object contained therein
%	  4 create resulting image from the scaled bounding shape, i.e. find
%	    "bounding box" of the bounding shape
%	- This scaling method is not size-invariant, but is best-suited for pose
%	  estimation, where traditional scale and translational invariance is
%	  not appropriate. For example, if we specify a mass threshold for
%	  which to scale all pose images to, frontal views of planes may be
%	  scaled to exceed the size of the 
%	

%	- works only on gray-scale images with light objects (1) and black
%	  background (0)
%	- for disc, the bounding square is first computed, which is then bounded
%	  by a circle. This circle is then scaled in step 3.
%
%	_____SEE ALSO___________________________________________________________
%	lans_invariant	lans_centroid	lans_imgcrop
%
%	(C) 1999.10.11 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	to do
%	fix square bounding shape
%	fix possibly non square cbimage

function	[sbimg]	= lans_imgscale(bimg,bshape,res,debug)

if nargin>0
%__________ REGULAR ____________________________________________________________
if nargin<4
	debug	= 0;
	if nargin<3
		res	= 128;
		if nargin<2
			bshape	= 'disc';
		end
	end
end

%----------	Pre-process and create co-ordinates
bimg		= double(bimg);
[row,col]	= size(bimg);
[x,y]   	= meshgrid(1:col,1:row);
xcen		= .5*(col+1);
ycen		= .5*(row+1);

%----------	1 find centroid of image object
centroid	= lans_centroid(bimg);

%----------	2 find bounding box
vsum	= sum(bimg);
vnonzero= find(vsum>0);
xrange	= [min(vnonzero) max(vnonzero)];
xsize	= xrange(2)-xrange(1)+1;
bboxxcen= sum(xrange)/2;

hsum	= sum(bimg')';
hnonzero= find(hsum>0);
yrange	= [min(hnonzero) max(hnonzero)];
ysize	= yrange(2)-yrange(1)+1;
bboxycen= sum(yrange)/2;

bbox	= [xrange(1) xrange(2) xrange(2) xrange(1);
yrange(1) yrange(1) yrange(2) yrange(2)];
bbox	= bbox + [-.5 .5 .5 -.5;-.5 -.5 .5 .5];
bbox	= [bbox bbox(:,1)];

%----------	3 find discs of radius rmax, rmin centered at object centroid

%	Min bounding disc centered @ centroid
rmin	= max(max(abs(centroid*ones(1,length(bbox)-1)-bbox(:,1:4))));

if strcmp(bshape,'disc')
	%	Max bounding disc centered @ centroid
	rmax	= max(vdist(centroid*ones(1,length(bbox)-1),bbox(:,1:4)));

	%__________	4 compute new bbox2 bounding disc rmin w.r.t. centroid
	%		  bbox2 now bounds the disc of radius rmin centered @
	%		  object centroid
	x1	= floor(centroid(1)-rmin);
	x2	= ceil(centroid(1)+rmin);
	y1	= floor(centroid(2)-rmin);
	y2	= ceil(centroid(2)+rmin);

	bbox2	=[x1 x2 x2 x1;y1 y1 y2 y2];
	bbox2	= bbox2+[-.5 .5 .5 -.5;-.5 -.5 .5 .5];	% pixel correction
	bbox2	= [bbox2 bbox2(:,1)];

	%__________	5 scan inwards from corners of bbox to bbox2
	%		to find smallest bounding disc
	%		(handle cases when bbox2 exceeds current image range)
	tbimg	= bimg;
	if x1<1
		tbimg	= [zeros(row,-x1+1) tbimg];
		x2	= x2-x1+1;
		x1	= 1;
	end
	[dumb,tcol]	= size(tbimg);
	if x2>tcol
		tbimg	= [tbimg zeros(row,x2-tcol)];
		[dumb,x2]	= size(tbimg);
	end

	[dumb,tcol]	= size(tbimg);
	if y1<1
		tbimg	= [zeros(-y1+1,tcol);tbimg];
		y2	= y2-y1+1;
		y1	= 1;
	end
	[trow,dumb]	= size(tbimg);
	if y2>trow
		tbimg	= [tbimg;zeros(y2-trow,tcol)];
		[y2,dumb]= size(tbimg); 
	end

	%-----	create effective image block of size 2*rmin
	eimg		= tbimg(y1:y2,x1:x2);
	[erow,ecol]	= size(eimg);

	%-----	subdivide into 4 blocks
	bound	= ceil(erow/2);
	b1	= eimg(1:bound,1:bound);
	b2	= eimg(1:bound,end-bound+1:end);
	b3	= eimg(end-bound+1:end,1:bound);
	b4	= eimg(end-bound+1:end,end-bound+1:end);

	%-----	concentrate all 4 blocks into lower right
	bll	= b3+b1(end:-1:1,:);
	bur	= b2+bll';
	blr	= b4+bur(end:-1:1,:);
	slen	= length(blr);

	%-----	Search inwards for touching edge using mask
	found	= 0;
	r	= ceil(rmax)+1;
	
	while ~found
		r	= r-1;
		piemask	= zeros(ceil(rmax),ceil(rmax));
		piex	= meshgrid(1:ceil(rmax))-.5;
		piey	= piex';
		piemask	= sqrt(piex.*piex+piey.*piey)>=r;
		found	= sum(sum(piemask(1:slen,1:slen).*blr));	
	end	
	rbest	= min(r+1,rmax);


%----------	6 scale up bounding shape to image size
%	scale	= 2*rbest/(min(col,row)-1);
	scale	= 2*rbest/res;
elseif strcmp(bshape,'square')
	scale	= 2*rmin/res;
end

%__________	7 fill/crop bimg to bound a disc of radius rmin @ centroid
%- operates on bimg,x,y
%- rmin may be bigger or smaller than res
cbimg	= lans_imgcrop(bimg,centroid,rbest*2,rbest*2);
csize	= size(cbimg);
scale	= 2*rbest/res;

%size(cbimg)
% cater to non-square cbimg !!!!!!!!!!!!!!!!!!

[x,y]	= meshgrid(1:size(cbimg,2),1:size(cbimg,1));
cen1	= lans_centroid(cbimg);
x	= x-cen1(1);
y	= y-cen1(2);

[xt,yt]	= meshgrid(1:res,1:res);
cen2	= [(res+1)/2 (res+1)/2];
xt	= (xt-cen2(1))*scale;
yt	= (yt-cen2(2))*scale;

%__________	8 interpolate
sbimg	= interp2(x,y,cbimg,xt,yt,'*linear');
sbimg	= (sbimg>0);

%__________	9 replace NaN with zeros
sbimg	= lans_killnan(sbimg,0);
scale	= 1/scale;

%--------------------	10 Debug	
if debug
	clf;
	subplot(2,1,1);
	image(bimg*255);
	colormap 1-gray;
	lans_plotmd(bbox,'b-','-hold 1');
	hold on;
	plot(bboxxcen,bboxycen,'b+');
	lans_plotmd(centroid,'g+','-hold 1');
	if strcmp(bshape,'square')
		x1	= centroid(1)-rmin;
		x2	= centroid(1)+rmin;
		y1	= centroid(2)-rmin;
		y2	= centroid(2)+rmin;
		bbox2	= [x1 x2 x2 x1;y1 y1 y2 y2];
		bbox2	= bbox2+[-.5 .5 .5 -.5;-.5 -.5 .5 .5];
		bbox2	= [bbox2 bbox2(:,1)];
		lans_plotmd(bbox2,'g-','-hold 1');
	else
		lans_plotcovars(rmin*rmin,centroid,'m-',64);
	
		lans_plotcovars(rmax*rmax,centroid,'r-',64);
		lans_plotcovars(rbest*rbest,centroid,'k--',64);

		lans_plotmd(bbox2,'c-','-hold 1');
	end

	axis fill;axis tight;axis square;axis off

%	legend('Bbox','Bbox center','centroid','bdisc_{min}','bdisc_{max}','bdisc',0);
	subplot(2,1,2);
	image(sbimg*255);hold on;
	colormap 1-gray;
	centroid= lans_centroid(sbimg>.5);
	lans_plotmd(centroid,'g+','-hold 1');
	if strcmp(bshape,'square')
		x1	= centroid(1)-col/2;
		x2	= centroid(1)+col/2;
		y1	= centroid(2)-row/2;
		y2	= centroid(2)+row/2;
		bbox2	=[x1 x2 x2 x1;y1 y1 y2 y2];
		bbox2	= bbox2+[-.5 .5 .5 -.5;-.5 -.5 .5 .5];
		bbox2	= [bbox2 bbox2(:,1)];
		lans_plotmd(bbox2,'g-');
	else
		lans_plotcovars(res*res/4,centroid,'g-',64);
	end
	tstr	= sprintf('scaled centroid = [%2.2f, %2.2f]',centroid);
	title(tstr)

	axis fill;axis tight;axis square;axis off
end	%**********	debug
%__________ REGULAR ends _______________________________________________________
else
%__________ DEMO _______________________________________________________________
clf;clc;
disp('running lans_imgscale.m in demo mode');
%simg	= imread('/home/kuiyu/acad/proj/pose/dat/0raw/b747_y010_x050.jpg','jpg');
simg	= imread('/home/kuiyu/acad/proj/pose/dat/9test/f18bw.jpg','jpg');
if size(simg,3)==3
	%gimg	= rgb2gray(simg);
else
	gimg	= simg;
end
bimg	= gimg>30;
lans_imgscale(bimg,'disc',128,1);

%__________ DEMO ends __________________________________________________________
end
