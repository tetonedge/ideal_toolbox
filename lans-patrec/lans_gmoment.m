%	lans_gmoment	- Compute geometric moments of a 2-D image 
%
%	[m_pq]	= lans_gmoment(img[,p,q])
%
%	_____OUTPUTS____________________________________________________________
%	m_pq	geometric moment of order p+q			(scalar)
%
%	_____INPUTS_____________________________________________________________
%	img	row by col image of reals			(matrix)
%	p	order of x					(scalar)
%	q	order of y					(scalar)
%
%	_____NOTES______________________________________________________________
%	- zero order moment assumed if p,q unspecified 
%	- x,y assumed to start from 1
%	- x,y follows raster scanning convention
%	- xcentroid	= m_10/m_00
%	- ycentroid	= m_01/m_00
%	- handles non square images
%
%	_____SEE ALSO___________________________________________________________
%	lans_centroid
%
%	(C) 1999.09.28 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[m_pq]	= lans_gmoment(img,p,q)

if nargin~=3
	p=0;q=0;
end

[row,col]	= size(img);
[x,y]   	= meshgrid(1:col,1:row);
x		= x.^p;
y		= y.^q;

gmom		= x.*y.*double(img);
m_pq		= sum(sum(gmom));
