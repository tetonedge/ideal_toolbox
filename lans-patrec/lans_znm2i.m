%	lans_znm2i	- Compute cumulative count of order n and repetition m 
%
%	[c]	= lans_znm2i(n[,m])
%
%	_____OUTPUTS____________________________________________________________
%	c	cumulative count of moments			(integer)
%
%	_____INPUTS_____________________________________________________________
%	n	order						(integer)
%	m	repetition					(integer,integer)
%
%	_____NOTES______________________________________________________________
%	- if m unspecified, c returns up to all moments of order n
%	- n even :
%			c=.25*n^2+.5*n+floor(m/2+1)
%	  n odd  :	
%			c=.25*n^2+.5*n+.25+floor(m/2+1)
%
%	_____SEE ALSO___________________________________________________________
%	lans_zi2nm	lans_zmoment
%
%	(C) 1999.03.01 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[c]	= lans_znm2i(n,m)

if nargin<2
	m	= ~lans_iseven(n);
end

if length(m)<2
	if (n<0)|~lans_isint(n)|~lans_isint(m)|m>n
		error('Inputs n,m must be non-negative integers s.t. m<=n');
		break; 
	end

	if lans_iseven(n)
		presum	= .5*n*(.5*n+1);
		alln	= .5*n+1;
		if ~lans_iseven(m)
			error('m must be even for even n');
			break; 
		end
	else
		presum	= (n+1)*(n+1)*.25;
		alln	= .5*(n-1)+1;
		if lans_iseven(m)
			error('m must be odd for odd n');
			break; 
		end
	end
	
	if nargin<2
		c	= presum + alln;
	else
		wm	= floor(m/2+1);
		c	= presum + wm;	
	end
else
	for mthis	= 1:length(m)
		c(mthis)= lans_znm2i(n,m(mthis));
	end
end
