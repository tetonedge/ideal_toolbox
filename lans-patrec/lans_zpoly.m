%	lans_zpoly	- Compute Zernike polynomial over unit circle
%
%	[V_nm,mag,phase]	= lans_zpoly(n,m,p,theta)
%
%	_____OUTPUTS____________________________________________________________
%	V_nm	Zernike polynomial of order n repetition m	(complex matrix)
%	mag	magnitude |V_nm|				(real matrix)
%	phase	angle in radians				(real matrix)
%
%	_____INPUTS_____________________________________________________________
%	n	order						(integer>=0)
%	m	repetition					(integer)
%	p	input magnitude					(real matrix)
%	theta	angle						(real matrix)
%
%	_____NOTES______________________________________________________________
%	- |m|<=n and n-|m| even
%
%	_____SEE ALSO___________________________________________________________
%	lans_zmoment	lans_fac
%
%	(C) 1999.02.17 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[V_nm,mag,phase]	= lans_zpoly(n,m,p,theta)

R_nm	= zeros(size(p));

a	= (n+abs(m))/2;
b	= (n-abs(m))/2;
total	= b;

for s=0:total
	num	= ((-1)^s)*lans_fac(n-s)*(p.^(n-2*s));
	den	= lans_fac(s)*lans_fac(a-s)*lans_fac(b-s);
	R_nm	= R_nm + num/den;
end

mag	= R_nm;
phase	= m*theta;
V_nm	= mag.*exp(i*phase);
