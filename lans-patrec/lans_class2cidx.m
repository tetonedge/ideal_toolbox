%	lans_class2cidx	- Assign unique integer label to class (non-numeric)
%
%	[cidx,ctable]	= lans_class2cidx(cdata<,ctable>)
%
%	_____OUTPUTS____________________________________________________________
%	cidx	converted class indices in integers	(row)
%	ctable	converstion lookup table		(cell)
%		e.g.ctable{1}='red',ctable{2}='blue'
%		For numerical data vector ctable(:,c)=c-th original vector label
%
%	_____INPUTS_____________________________________________________________
%	cdata	data class labels			(cell/col vectors)
%	ctable	converstion lookup table		(cell)
%		(not used)
%
%	_____EXAMPLE____________________________________________________________
%	[cidx,ctable]=lans_class2cidx({'A','B','A','A','B','B','C','B','C'});
%	[cidx,ctable]=lans_class2cidx({'5','3','4','2','10','B','C','B','C'});
%	[cidx,ctable]=lans_class2cidx([1 2 1 3;1 1 1 1]); 
%	[cidx,ctable]=lans_class2cidx([5 2 2 3 5]);
%
%	_____NOTES______________________________________________________________
%	- less computation if ctable is supplied.
%	- text class sorted lexically/alphabetically
%	- if cdata=cell, then cidx=cell
%	- original class recovered via 	ctable(cidx) or lans_cidx2class
%
%	_____SEE ALSO___________________________________________________________
%	lans_cidx2class
%
%	(C) 1999.12.03 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________
%	append new classes to ctable when not already present in input ctable
%	lans_cidx2class

function [cidx,ctable]	= lans_class2cidx(cdata,ctable)

[D,N]	= size(cdata);
cidx	= ones(1,N);

% convert cells to integer vectors and sort
% if not cell, sort numerical class labels
if iscell(cdata)	% string
	% string/vector classes embedded in cells
	% convert cell to +ve integers
	if iscellstr(cdata)
		% convert string cell to sorted col vectors
		rowvec		= (char(cdata)+0);
	else
		% numeric cell
		rowvec		= cat(1,cdata{:});
	end
	[srowvec,idx1]	= sortrows(rowvec);
else
	% numerical classes
	[srowvec,idx1]	= sort(cdata');	
end

[uniqcols,idx2]	= lans_finduniq(srowvec');

% class boundaries
idx2		= [idx2 N];
C		= size(uniqcols,2);
for c=1:C
	cidx(idx1(idx2(c):idx2(c+1)))	= c;
end

%_____	ctable
if iscell(cdata)
	if iscellstr(cdata)
		ctable		= cellstr(char(uniqcols'))';
	else
		ctable		= uniqcols;
	end
else
	% numerical scalar classes
	ctable	= uniqcols;
end
