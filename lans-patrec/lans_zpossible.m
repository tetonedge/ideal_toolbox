%	lans_zpossible	- Compute repetitions for Zernike moments of order n
%
%	[m]	= lans_zpossible(n)
%
%	_____OUTPUTS____________________________________________________________
%	m	list of possible repetitions			(vector)
%
%	_____INPUTS_____________________________________________________________
%	n	order						(integer>=0)
%
%	_____NOTES______________________________________________________________
%	- # moments @ order n = floor(n/2)+1
%
%	_____SEE ALSO___________________________________________________________
%	lans_zmoment
%
%	(C) 1999.09.28 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[m]	= lans_zpossible(n)

if lans_iseven(n)
	m	= 0:2:n;
else
	m	= 1:2:n;
end
