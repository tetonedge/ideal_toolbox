%	lans_pose2cart	- Convert pose angles to cartesion eye positions
%
%	[cart]	= lans_pose2cart(pose<,r>);
%
%	_____OUTPUTS____________________________________________________________
%	cart	Cartesian co-ordinates about a unit sphere	(col vectors)
%
%	_____INPUTS_____________________________________________________________
%	pose	view positions [elevation;rotation]		(col vectors)
%	r	sphere radius					(integer+)
%		*1						default
%
%%		* default
%
%	_____EXAMPLE____________________________________________________________
%
%	_____NOTES______________________________________________________________
%	-works for 3-D only
%	-assumes unit radius by default
%	-latitude lies in the interval [0,360)
%		- cannot be determined at absolute poles (defaults to 0)
%	-longitude lies in the interval [-90,90]
%
%	_____SEE ALSO___________________________________________________________
%	lans_cart2pose	lans_ppsinit.m	cart2sph
%
%	(C) 1999.12.16 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [cart]	= lans_pose2cart(pose,r);

[pose,thedim]	= lans_md2lin(pose);

if nargin<2
	r=1;
end

[dum N]	= size(pose);

elev	= pose(1,:)*pi/180;	
rota	= pose(2,:)*pi/180;
r	= r*ones(1,N);

[x,y,z]	= sph2cart(rota,elev,r);

cart	= [x;y;z];

cart	= lans_lin2md(cart,thedim);
