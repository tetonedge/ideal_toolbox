%	lans_rotate	- Rotate 2-D image by an arbitrary angle
%
%	[rimg]	= lans_rotate(img,angle,options)
%
%	_____OUTPUTS____________________________________________________________
%	rimg	rotate image					(matrix)
%
%	_____INPUTS_____________________________________________________________
%	img	input image					(matrix)
%	angle	rotation in degrees				(scalar)
%		+ve	counter clockwise	
%		-ve	clockwise
%	options	
%
%	_____EXAMPLE____________________________________________________________
%	lans_
%
%	_____NOTES______________________________________________________________
%	for demo, call function without parameters
%	- currently works only on b/w image
%	- uses interp2
%
%	_____SEE ALSO___________________________________________________________
%	lans_poseimg
%	interp2
%
%	(C) 1999.12.26 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________

function	[rimg]	= lans_rotate(img,angle,options)

if nargin>0
%__________ REGULAR ____________________________________________________________
[y_size,x_size]	= size(img);

[x,y]	= meshgrid(1:x_size,1:y_size);
x	= x-mean(mean(x));
y	= y-mean(mean(y));
[theta,r]=cart2pol(x,y);

theta	= theta+angle*pi/180;

[x1,y1]	= pol2cart(theta,r);

rimg	= interp2(x,y,img,x1,y1);
rimg	= lans_killnan(rimg);

%__________ REGULAR ends _______________________________________________________
else
%__________ DEMO _______________________________________________________________
clf;clc;
disp('running lans_rotate.m in demo mode');
img	= zeros(10,20);
img(6,:)=1;
alpha	= 20;

subplot(1,2,1);
image(255*img)
title('original');
colormap gray;

subplot(1,2,2);
rimg	= lans_rotate(img,alpha);
image(255*rimg);
tstr	= sprintf('rotated by %3d degrees',alpha);
title(tstr);
%__________ DEMO ends __________________________________________________________
end
