%	lans_posefe	- Extract features from pose images
%
%	[pobj]	= lans_posefe(dpath,prefix,xlist,ylist,pstring)
%
%	_____OUTPUTS____________________________________________________________
%	pobj	LANS pose object				(col vector)
%		see lans_poseobj
%
%	_____INPUTS_____________________________________________________________
%	dpath	full path of pose image				(string)
%	prefix	prefix of image file				(string)
%	xlist	list of rotation angles about xaxis		(row vector)
%	ylist	list of rotation angles about yaxis		(row vector)
%	pstring	parameter string				(string)
%		-angfix angle string in filename
%			'x000_y000'
%		-method	Feature Extraction method
%			'zmoment'	Zernike moments (complex)
%		-name	name of object
%		-nfeatures	number of features desired
%			50	(approx. for some methods)
%		-suffix	graphic format
%			'.jpg'
%		-thold	threshold for binarizing image
%			10
%
%	_____EXAMPLE____________________________________________________________
%	lans_posefe('/home/kuiyu/acad/proj/pose/data/0raw/','b747',10,10,'')
%
%	_____NOTES______________________________________________________________
%	for demo, call function without parameters
%	- specify several values of xlist and ylist to extract in batch mode
%	- xlist =  -90,-80,...,0,10,...,90 or 270,280,...,0,10,...,90
%	- ylist =  0,10,...,350
%
%	_____SEE ALSO___________________________________________________________
%	lans_poseobj	lans_poseimg
%
%	(C) 1999.06.17 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[pobj]	= lans_posefe(dpath,prefix,xlist,ylist,pstring)

if nargin>0
%__________ REGULAR ____________________________________________________________
%_____	filename formats and settings
angfix	= lans_assign(paraget('-angfix',pstring),	'x000_y000');
method	= lans_assign(paraget('-method',pstring),	'zmoment');
name	= lans_assign(paraget('-name',pstring),		'noname');
nf	= lans_assign(paraget('-nfeatures',pstring),	50);
suffix	= lans_assign(paraget('-suffix',pstring),	'.jpg');
thold	= lans_assign(paraget('-thold',pstring),	10);

%_____	modulus of angle ranges
xlist	= mod(xlist,360);
ylist	= mod(ylist,360);

%_____	initialize pose object
switch (method)
	case 'zmoment',
		zmorder	= lans_zi2nm(nf);
		nf	= lans_znm2i(zmorder)-1;	% 0th zmoment not used
		pobj	= lans_poseobj(name,method,xlist,ylist,nf);
	otherwise,
end

%_____	Extract features for each pose
for xi=1:length(xlist)
	for yi=1:length(ylist)
		xang	= xlist(xi);
		yang	= ylist(yi);
		%_____	prepare filename
		xstr	= num2str(xang);
		ystr	= num2str(yang);
		while length(xstr)<3,xstr =['0' xstr];end;
		while length(ystr)<3,ystr =['0' ystr];end;
		fname	= [prefix '_x' xstr '_y' ystr suffix];
		pname	= [dpath filesep fname];

		%_____	read/threshold/scale image
		rgbimg	= imread(pname,'jpg');
		gimg	= rgb2gray(rgbimg);
		simg	= lans_imgscale(gimg>thold,'disc');

		%_____	Extract features	
		switch (method)
			case 'zmoment',
				zmorder		= lans_zi2nm(nf);
				[A_nm,zmlist]	= lans_zmoment(simg,1:zmorder);
				pobj.f(xi,yi,:)	= A_nm;
				if (xi==yi==1)	% need only assign once
					pobj.misc	= {'zmlist',zmlist};
				end
			otherwise,

		end
	end	% yang
end	% xang

%__________ REGULAR ends _______________________________________________________
else
%__________ DEMO _______________________________________________________________
clf;clc;
disp('running lans_posefe.m in demo mode');
%__________ DEMO ends __________________________________________________________
end
