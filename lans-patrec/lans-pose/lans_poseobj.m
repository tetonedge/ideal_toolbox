%	lans_poseobj	- Initialize empty LANS pose object (i.e. structure)
%
%	[pobj]	= lans_poseobj(oname,method,xlist,ylist,nf)
%
%	_____OUTPUTS____________________________________________________________
%	pobj	LANS pose obj					(col vector)
%		.name	object name				(string)
%		.method	Feature extraction method		(string)
%		.x	list of x angles			(vector)
%		.y	list of y angles			(vector)
%		.f	features for each pose			Nx x Ny x Nf 
%		.misc	storage for miscellaneous items		(cell)
%
%	_____INPUTS_____________________________________________________________
%	oname	object name					(string)
%	xlist	list of x angles				(row vector)
%	ylist	list of y angles				(row vector)
%	nf	# of features					(scalar)
%
%	_____EXAMPLE____________________________________________________________
%
%	_____NOTES______________________________________________________________
%	- each object contains multiple poses
%
%	_____SEE ALSO___________________________________________________________
%	lans_posefe	lans_poseimg
%
%	(C) 1999.10.22 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________

function	[pobj]	= lans_poseobj(oname,method,xlist,ylist,nf)

lx	= length(xlist);
ly	= length(ylist);

pobj.name	= oname;
pobj.method	= method;
pobj.x		= xlist;
pobj.y		= ylist;
pobj.f(lx,ly,nf)= 0;
