%	lans_posetune	- Callibrate poses w.r.t. trained manifold
%
%	[cpose,cangle]	= lans_posetune(pps,yangle,pose,options)
%
%	_____OUTPUTS____________________________________________________________
%	cpose	callibrated pose				(col vectors)
%	cangle	rotation angle (degrees) in the image plane	(row vector)
%
%	_____INPUTS_____________________________________________________________
%	pps	probabilistic principal surface			(structure)
%	yangle	angle of raw complex features			(col vectors)
%		MUST provide phase of complex Zernike moments
%	pose	uncallibrated poses (y projected on pps)	(col vectors)
%
%	_____EXAMPLE____________________________________________________________
%
%	_____NOTES______________________________________________________________
%	for demo, call function without parameters
%
%	_____SEE ALSO___________________________________________________________
%	lans_ppsinit	lans_ppsproj
%	NETLAB:	kmeans
%
%	(C) 1999.12.27 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________
%	Interpolation among neighborhood co-ordinates

function	[cpose,cangle]	= lans_posetune(pps,yangle,pose,options,ycpx)

debugit=0;
if nargin>0
%__________ REGULAR ____________________________________________________________
[D,N]	= size(yangle);
useable	= [1 3:D+1];
netlab_options	= foptions;

elist	= -90:10:90;
rlist	= 0:10:350;

cpose	= pose;

%pps.angle{1}	= mod(pps.angle{1},2*pi);
%pps.angle{2}	= mod(pps.angle{2},2*pi);
%yangle		= mod(yangle,2*pi);
%lycpx		= lans_md2lin(ycpx);	% poles at last 2 position
%lycpx		= lycpx(:,[end-1 1:end-1]);	% shift south pole to first 

for n=1:N
	elev	= pose(1,n);
	rot	= pose(2,n);

	if abs(elev)>0
	%_______________________________________________________________________
	%	Find manifold node value
	[dum,eidx]	= min(abs(elist-elev));
	[dum,ridx]	= min(abs(rlist-rot));
	nnpose		= [elist(eidx);rlist(ridx)];
	if eidx==1			% South pole
		ymangle	= pps.angle{2}(:,1);
%		ppsf	= ycpx{2}(:,1);
	elseif eidx==length(elist)	% North Pole
		ymangle	= pps.angle{2}(:,2);
%		ppsf	= ycpx{2}(:,2);
	else
		ymangle	= pps.angle{1}(:,ridx,eidx-1);
%		ppsf	= ycpx{1}(:,ridx,eidx-1);
	end
%	if ymangle>pi
%		ymangle=ymangle-2*pi;
%	elseif ymangle<-pi
%		ymangle=ymangle+2*pi;
%	end
	%	Find angle between ymani and ytest
	[dum,m]	= lans_zi2nm(useable);
	nonzero	= find(m~=0);	% repetition m=0 not considered
	theta	= yangle(nonzero,n)-ymangle(nonzero);
	theta	= theta'./m(nonzero);

%	nonzero	= find(m==1);	% repetition m=0 not considered
%	theta	= mod(yangle(nonzero,n)',2*pi)-mod(ymangle(nonzero)',2*pi);

	shift	= theta;

	w	= find(shift>pi);
	shift(w)= shift(w)-2*pi;
	w	= find(shift<-pi);
	shift(w)= shift(w)+2*pi;
	shift	= shift*180/pi;

	% find mode of distribution
	%comp	= median(shift);	% median does not cut it
%	dshift	= 10*round(shift/10);
%	[udata,uidx]	= lans_finduniq(sort(dshift));
	
%	[kcenters,dum,post]	= kmeans(udata',shift',netlab_options);
	
%	[maxcount,midx]	= max(sum(post));
%	comp		= kcenters(midx);

	cangle(n)	= mean(shift([2 6]));
	% compensate
	cpose(2,n)	= mod(nnpose(2)-cangle(n),360); 
	if debugit
		v1	= lans_unitvec([real(ppsf)';imag(ppsf)']);
%		v2	= lans_unitvec([real(lycpx(:,n))';imag(lycpx(:,n))']);
		theangle= acos(sum(v1.*v2))*180/pi;
		theangle= theangle(nonzero)./m(nonzero);

		cangle(n)
		[m(nonzero(1:length(shift)));shift;theangle]
		l	= {'true',options(1,n),options(2,n);'output',pose(1,n),pose(2,n);'compensated',cpose(1,n),cpose(2,n)}
		pause
	end
	%_______________________________________________________________________
	end	% if abs(elev)>20
end
%__________ REGULAR ends _______________________________________________________
else
%__________ DEMO _______________________________________________________________
clf;clc;
disp('running lans_posetune.m in demo mode');

%__________ DEMO ends __________________________________________________________
end
