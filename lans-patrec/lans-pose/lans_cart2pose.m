%	lans_cart2pose	- Convert cartesion eye position to pose angles
%
%	[pose]	= lans_cart2pose(cart);
%
%	_____OUTPUTS____________________________________________________________
%	pose	view positions [elevation;rotation]		(col vectors)
%		-90 <= elevation <=  90
%		  0 <= rotation  <  360
%
%	_____INPUTS_____________________________________________________________
%	cart	Cartesian co-ordinates about a unit sphere	(col vectors)
%
%	_____EXAMPLE____________________________________________________________
%
%	_____NOTES______________________________________________________________
%	-works for 3-D only
%	-rotation about y-axis lies in the interval [0,360)
%		- cannot be determined at absolute poles (defaults to 0)
%	-elevation (rotate about x-axis) lies in the interval [-90,90]
%
%	_____SEE ALSO___________________________________________________________
%	lans_pose2cart	lans_ppsinit.m	cart2sph
%
%	(C) 1999.12.19 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________

function [pose]	= lans_cart2pose(cart);

fac	= 180/pi;
[d N]	= size(cart);
pose	= zeros(2,N);

[th,phi,r]	= cart2sph(cart(1,:),cart(2,:),cart(3,:));

pose(1,:)	= phi*fac;		% elevation
pose(2,:)	= mod(th*fac,360);	% rotation

% make sure elevation lies within [-90,90]
w	= find(pose(1,:)>90);
pose(1,w)= pose(1,w)-360;

% make rotation=0 at poles
atpose		= find(abs(pose(1,:))==90);
pose(2,atpose)	= zeros(size(atpose));
