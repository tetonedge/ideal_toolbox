%	lans_poseimg	- Load/display a scaled/thresholded pose image
%
%	[simg,gimg,rgbimg,plotit]= lans_poseimg(dpath,prefix,ele,rot,options)
%
%	_____OUTPUTS____________________________________________________________
%	simg	scaled, centered, thresholded image		(matrix)
%	gimg	grayscale image stack				(matrix)
%	rgbimg	RGB image stack					(matrix)
%	plotit	set to anything if plot desired			(anything)
%
%	_____INPUTS_____________________________________________________________
%	dpath	full path of pose image file			(string)
%	prefix	prefix of pose image file			(string)
%	ele	elevation angle		[-90,90]		(scalar)
%	rot	rotation angle index	[0  ,360)		(scalar)
%	options							(string)
%		-thold	binary threshold
%			10*		[1,255]
%		-interp	interpolation on ele, rot		(string)
%			none	no action, accept raw values
%			round*	rounded to nearest 10 integer
%
%		* default
%
%	_____EXAMPLE____________________________________________________________
%	lans_poseimg('/home/kuiyu/acad/proj/pose/dat/0train/0raw','b747',-90,10);
%
%	_____NOTES______________________________________________________________
%	for demo, call function without parameters
%	- specify 4th output arg or specify NO ouptut arg to auto plot
%	- ordered via Left Hand thumb rule
%		with left hand thumb pointing to the positive direction,
%		finger curl indicates postive angle rotation
%	- if options=[], then no scaling and thresholding will occur, simg is
%	  invalid
%
%	_____SEE ALSO___________________________________________________________
%	lans_imgscale
%
%	(C) 2000.01.25 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________

function [simg,gimg,rgbimg,plotit]= lans_poseimg(dpath,prefix,ele,rot,options)

if nargin>0
%__________ REGULAR ____________________________________________________________
if nargin<5
	options=[];
end

thold	= paraget('-thold',options,10);
interp	= paraget('-interp',options,'round');

%_____	filename formats and settings
angfix	= 'e000_r000';
suffix	= '.jpg';
anginc	= 10;				% angle increment

switch(interp)
case 'round'
	eangle	= 10*round(ele/anginc);
	rangle	= mod(10*round(rot/anginc),360);
case 'none'
	eangle	= ele;
	rangle	= rot;
end

graydec	= 75;				% for plotting grayscale image

%_____	prepare filename
estr	= num2str(eangle);
rstr	= num2str(rangle);
nulls	= '00';
alen	= length(estr);
if alen<3,
	if eangle<0
		estr =[estr(1) '0' estr(2)];
	else
		estr = [nulls(1:(3-alen)) estr];
	end
end;
alen	= length(rstr);
if alen<3
	rstr	= [nulls(1:(3-alen)) rstr];
end
fname	= [prefix '_e' estr '_r' rstr suffix];
pname	= [dpath filesep prefix filesep fname];

%-----	read/threshold/scale image
rgbimg	= imread(pname,'jpg');
gimg	= rgb2gray(rgbimg);

if ~isempty(options)
	[x,y]	= size(gimg);
	r	= min(x/2,y/2);
	simg	= lans_imgscale(gimg>thold,'disc');
			
%_____	stretch gray scale image for improved display
	w	= find(gimg~=0);
	gimg1	= double(gimg(w));
	egimg1	= max(gimg1-graydec,thold+1);
	egimg	= zeros(size(gimg));
	egimg(w)= egimg1;
else
	simg	= [];
	egimg	= zeros(size(gimg));
end	

%-----	plot image if no output arguments specified
if nargout==0 | nargout==4
	clf;
	cellimg	= {rgbimg,egimg,simg*255};
	tstr	= {sprintf('RGB\n'),sprintf('Grayscale\n'),sprintf('Binary\n(scaled & disc centered)')};
	for p=1:3
		subplot(1,3,p)
		image(cellimg{p});
		if p>1
			colormap gray;
		end
		axis image;
		axis off;
		title(tstr{p});
	end
	lans_plotcovars(r*r,[x/2+.5;y/2+.5],'g-',36);
	axis image;
	axis off;

	subplot(1,3,1);
	text(0,y+y/10,strfilter(fname,'insertbf','_\')); % escapes '_' TeX
	subplot(1,3,3);
	text(0,y+y/10,sprintf('Threshold = %d',thold));
end
plotit	= 0;
%__________ REGULAR ends _______________________________________________________
else
%__________ DEMO _______________________________________________________________
clf;clc;
disp('running lans_poseimg.m in demo mode');
ele	= -90;
for rot=[0:10:350]
	lans_poseimg('/home/guiyu/acad/proj/pose/dat/air/1train/0raw','f15',ele,rot,'-interp none');
	drawnow;
end
%__________ DEMO ends __________________________________________________________
end
