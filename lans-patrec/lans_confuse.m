%	lans_confuse	- Compute confusion matrix from ouput/desired classes
%
%	[confuse<,cidx>]= lans_confuse(target,out<,classes>)
%
%	_____OUTPUTS____________________________________________________________
%	confuse	confusion matrix ; true(row) x output(col)	(matrix)
%	cidx	confusion indices of misclassified samples	(matrix cell)
%		e.g. cidx{2,3} : indices of class 2 misclassified as 3
%
%	_____INPUTS_____________________________________________________________
%	target	True target class labels			(row cell)
%	out	output labels					(row cell)
%	classes	All class labels (ordered)			(row cell)
%		{'apples','oranges'}
%
%		* default
%
%	_____EXAMPLE____________________________________________________________
%
%	_____NOTES______________________________________________________________
%	- currently only works with integer class labels starting from 1
%	- if classes not specified, the maximum of the following is used
%		# unique [target out]
%	- method is scalable to very large matrices, as long as each target
%	  class entries fit into memory
%
%	_____SEE ALSO___________________________________________________________
%
%	(C) 1999.12.02 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [confuse,cidx]	= lans_confuse(target,out,classes)

N	= length(target);

if nargin<3
	C	= length(lans_finduniq(sort([target out])));
else
	C	= length(classes);
end

%_____	initialize null full confusion matrix
confuse	= zeros(C);
cidx	= cell(C);

%_____	compute it
[target2,idx2]	= sort(target);
out2		= out(idx2);

startidx	= 1;
endidx		= [find(diff(target2)~=0) N];	% class ending boundaries

for c=1:C
	if c==target2(startidx)	
		range		= startidx:endidx(c);
		target3		= target2(range);
		out3		= out2(range);
		idx3		= idx2(range);
		confuse(c,:)	= hist(out3,1:C);

		startidx	= endidx(c)+1;
		% put items in confusion matrices
		if nargout>1
			% good ones
			idx4	= find(target3==out3);
			cidx{c,c}=idx2(idx3(idx4));
			% bad ones
			idx4	= find(target3~=out3);
			for bad=1:length(idx4)
				cidx{c,out3(idx4(bad))}	= [cidx{c,out3(idx4(bad))} idx2(idx3(idx4(bad)))];
			end
		end
	end
end
