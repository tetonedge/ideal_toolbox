%	lans_zi2nm	- Convert index to order n and repetition m 
%
%	[n,m]	= lans_zi2nm(c)
%
%	_____OUTPUTS____________________________________________________________
%	n	order						(col vector)
%	m	repetition					(col vector)
%
%	_____INPUTS_____________________________________________________________
%	c	cumulative count of moments			(vector)
%
%	_____NOTES______________________________________________________________
%	for a demo table of Zmoment orders call function without parameters
%	- usu. this table is returned in zmlist of lans_zmoment
%	- upper bound of n is
%			-2 + 2*sqrt(c)
%
%	_____SEE ALSO___________________________________________________________
%	lans_znm2i	lans_zmoment
%
%	(C) 1999.12.19 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[n,m]	= lans_zi2nm(c)

if nargin>0
%__________ REGULAR ____________________________________________________________
N	= length(c);
if N>1
	for i=1:N
		[n(i),m(i)]	=lans_zi2nm(c(i));
	end
else
	maxn	= -2 + round(2*sqrt(c));

	cup	= lans_znm2i(maxn);
	cdown	= lans_znm2i(max(maxn-1,0));

	n	= maxn;
	if cup<c
		n	= maxn+1;
	end
	if cdown>c
		n	= maxn-1;
	end

	neven	= lans_iseven(n);
	remain	= c-lans_znm2i(n,~neven);
	m	= 1*(~neven)+remain*2;
end

%__________ REGULAR ends _______________________________________________________
else
%__________ DEMO _______________________________________________________________
clc;
disp('running lans_zi2nm.m in demo mode');
maxc	= 22;

maxn	= lans_zi2nm(maxc);
t	= NaN*ones(maxn+1);
for c=1:maxc
	[n,m]	= lans_zi2nm(c);
	t(n+1,1)= n;
	t(n+1,floor(m/2+1)+1)	= m;
end
t
%__________ DEMO ends __________________________________________________________
end
