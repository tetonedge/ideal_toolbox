%	lans_zmrecon	- Reconstruct image from Zernike Moments
%
%	[reimg]	= lans_zmrecon(A_nm,V_nm,zmlist,cidx,dim[,thold[,showi]])
%
%	_____OUTPUTS____________________________________________________________
%	rimg	reconstructed image				(real matrix)
%
%	_____INPUTS_____________________________________________________________
%	A_nm	Zernike moments					(col vector)
%		length of A_nm denotes maximum desired order
%	V_nm	Zernike polynomials size of img			(col vectors)
%	zmlist	keeps order and repetition information		(Mx2 matrix)
%		M = total # of moments
%	cidx	1-D indices of retained unit circle region	(vector)
%	dim	original dimension of image			(2-vector)
%	thold	threshold for discarding pixels 1-255		(scalar)
%		defaults	= 128
%	showi	show image of intermediate orders?0/1		(binary)
%
%	_____NOTES______________________________________________________________
%	see demo of lans_zmoment by typing help lans_zmoment
%
%	_____SEE ALSO___________________________________________________________
%	lans_zmoment
%
%	(C) 2000.11.27 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [reimg]	= lans_zmrecon(A_nm,V_nm,zmlist,cidx,dim,thold,showi)

if nargin<7
	showi	= 0;
	if nargin<6
		thold	= 128;	
	end
end

rimg		= zeros(size(cidx));

for c=1:length(A_nm)
	n	= zmlist(c,1);
	m	= zmlist(c,2);
	if n>zmlist(max(c-1,1),1)&showi
		eimg	= lans_equalize(real(rimg));
		if thold>1
			timg		= eimg;
			wcut		= find(eimg<thold);
			timg(wcut)	= zeros(size(wcut));
			wgood		= find(timg~=0);
			if ~isempty(wgood)
				timg(wgood)	= lans_equalize(timg(wgood));
			end
		else
			timg	= eimg;
		end

		hold off;
		pimg		= zeros(dim);
		pimg(cidx)	= timg;
		image(pimg);axis square;
		ts	= sprintf('Reconstructed Image using up to Order %d Zernike Moments',n);
		title(ts);
		axis off;
		drawnow
	else
		pimg	= zeros(dim);
	end
	rimg		= rimg+A_nm(c)*V_nm(:,c);
end
reimg		= zeros(dim);
reimg(cidx)	= timg;
