%	lans_cidx2class	- Recover class name from unique integer label
%
%	[cdata]	= lans_cidx2class(cidx,ctable)
%
%	_____OUTPUTS____________________________________________________________
%	cdata	data class labels			(cell/col vectors)
%
%	_____INPUTS_____________________________________________________________
%	cidx	converted class indices in integers	(row)
%	ctable	converstion lookup table		(cell)
%		(see lans_class2cidx)
%
%	_____EXAMPLE____________________________________________________________
%	[cidx,ctable]=lans_class2cidx({'A','B','A','A','B','B','C','B','C'});
%	cdata=lans_cidx2class(cidx,ctable)
%	[cidx,ctable]=lans_class2cidx({'5','3','4','2','10','B','C','B','C'});
%	cdata=lans_cidx2class(cidx,ctable)
%	[cidx,ctable]=lans_class2cidx([1 2 1 3;1 1 1 1]);
%	cdata=lans_cidx2class(cidx,ctable)
%	[cidx,ctable]=lans_class2cidx([5 2 2 3 5]);
%	cdata=lans_cidx2class(cidx,ctable)
%
%	_____NOTES______________________________________________________________
%	simple cell operations can be done via
%	cdata	= ctable(cidx)	!!!!!!!!!1
%	This function unifies cell and non-cell operations
%
%	_____SEE ALSO___________________________________________________________
%	lans_class2idx
%
%	(C) 1999.12.03 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________
%	append new classes to ctable when not already present in input ctable
%	lans_cidx2class

function [cdata]	= lans_cidx2class(cidx,ctable)

N	= length(cidx);

if iscell(ctable)
	cdata	= cell(1,N);
	C	= length(ctable);
	cdata	= ctable(cidx);
else
	C	= size(ctable,2);
	cdata	= ctable(:,cidx);
end
