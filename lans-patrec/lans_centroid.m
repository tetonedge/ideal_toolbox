%	lans_centroid	- Compute centroid of a 2-D image
%
%	[centroid]	= lans_centroid(img)
%
%	_____OUTPUTS____________________________________________________________
%	centroid	vector [x;y]				(col vector)
%
%	_____INPUTS_____________________________________________________________
%	img	row by col image of reals			(matrix)
%
%	_____NOTES______________________________________________________________
%	- x,y assumed to start from 1
%	- x,y follows raster scanning convention, i.e. x is row, y is col
%
%	_____SEE ALSO___________________________________________________________
%	lans_gmoment
%
%	(C) 1999.03.31 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [centroid]	= lans_centroid(img)

m00	= lans_gmoment(img,0,0);
m10	= lans_gmoment(img,1,0);
m01	= lans_gmoment(img,0,1);

x	= m10/m00;
y	= m01/m00;

centroid	= [x;y];
