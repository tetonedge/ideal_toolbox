%	knn1		- Find nearest k neighbours in 1 dim, sorted indices
%
%	[index,n_x,maxx,piv]	= knn1(x,pivot,k,wdist)
%
%	_____OUTPUTS____________________________________________________________
%	index	indices of k nearest neighbours			(row vector)
%	n_x	neighborhood (left points are -ve)		(row vector)
%	maxx	furthest dist					(scalar)
%	piv	new index for pivot				(scalar)
%
%	_____INPUTS_____________________________________________________________
%	x	row vector of sorted values			(row vector)
%	pivot	center index					(scalar)
%	k	# of neighbours including myself		(scalar)
%	wdist	wrap-around distance				(scalar)
%		R^1	actual distance
%		0	no wrap around 
%
%	_____NOTES______________________________________________________________
%	for demo, call function without parameters
%
%	(C) 1998.09.09 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [index,n_x,maxx,piv] = knn1(x,pivot,k,wdist)

if nargin>0

%--------------------	regular
l	= length(x);
cenx	= x-x(pivot);
closleng= x(l)-x(1)+wdist;
dist	= abs(cenx);
if wdist~=0
	dist	= min([dist;closleng-dist]);
	swap	= find(abs(cenx)>min(max(dist)));
	cenx(swap)	= (closleng-abs(cenx(swap))).*sign(-cenx(swap));
end
[sdist,ind]	= sort(dist);
index	= ind(1:k);
n_x	= cenx(ind);
n_x	= n_x(1:k);
maxx	= sdist(k);
piv	= find(ind==pivot);
%--------------------	regular ends
else
%--------------------	demo
disp('running knn1.m in demo mode');
x	= 1:10
pivot	= 2
k	= 5
w	= 1

[index,n_x,maxx,piv] = knn1(x,pivot,k,w)

%--------------------	demo ends
end
