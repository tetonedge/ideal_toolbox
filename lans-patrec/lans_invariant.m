%	lans_invariant	- Center and scale image to fixed mass
%
%	[iimg,x,y]	= lans_invariant(bimg <,para>)
%
%	_____OUTPUTS____________________________________________________________
%	iimg	invariant image					(matrix)
%	x	invariant co-ordinates (horizontal)		(matrix)
%	y	invariant co-ordinates (vertical)		(matrix)
%
%	_____INPUTS_____________________________________________________________
%	bimg	row by col image of reals			(binary matrix)
%	para	parameter string				(string)
%		-scale		0 or parameter, usu. m00
%		-translation	0
%
%	_____NOTES______________________________________________________________
%	- works only on binary images
%	- x,y assumed to start from 1
%	- x,y follows raster scanning convention
%	- move object's centroid to image's center
%	- scale object w.r.t. -scale
%
%	_____SEE ALSO___________________________________________________________
%	lans_centroid
%
%	(C) 2001.12.26 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[iimg,x,y]	= lans_invariant(bimg,para)

if nargin<2
	para	= [];
end

bimg	= double(bimg);
beta	= lans_paraget('-scale',para);
center	= lans_paraget('-translation',para);

[row,col]	= size(bimg);
[x,y]   	= meshgrid(1:row,1:col);

%----------	compute centroid
if center
	thecen	= lans_centroid(bimg);
else
	thecen	= zeros(2,1);
end

%----------	compute scale for given beta
if beta
	m00		= lans_gmoment(bimg,0,0);
	inva		= 1/sqrt(beta/m00);
else
	inva		= 1;
end

%----------	interpolate using new co-ordinates
if beta|center
	xi	= x*inva+thecen(1);
	yi	= y*inva+thecen(2);
	iimg	= interp2(x+inva*col/2+.5,y+inva*row/2+.5,bimg,xi,yi,'*linear');

	%----------	eliminate undefined values to zero (black)
	wnan		= find(isnan(iimg)==1);
	iimg(wnan)	= zeros(size(wnan));
else
	iimg	= bimg;
end
