%	lans_imgcrop	- Crop/Fill image
%
%	[cimg]	= lans_imgcrop(img,center,x,y)
%
%	_____OUTPUTS____________________________________________________________
%	cimg	cropped binary image 0/1			(matrix)
%
%	_____INPUTS_____________________________________________________________
%	img	image (binary)					(matrix)
%	center	center of crop operation			(vector)
%		[xcen ycen]
%	x	desired horizontal size (# columns)		(scalar)
%	y	desired vertical size (# rows)			(scalar)
%
%	_____NOTES______________________________________________________________
%	- cimg is y-rows by x-cols
%	- centroid is computed using a mask of threshold 0
%	- works best with binary images
%	- each pixel center corresponds to index value, i.e. pixel 1's center
%	  is at the x=1 position, extending a length of 0.5 in all directions
%	- it is ok if the context for x,y are reversed, as long the same is
%	  applied for x,y and center
%
%	_____SEE ALSO___________________________________________________________
%	lans_imgscale
%
%	(C) 1999.10.11 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[cimg]	= lans_imgcrop(img,center,x,y)

if nargin>0
%__________ REGULAR ____________________________________________________________
[row,col]	= size(img);
PRECISION	= 0.5;
x1		= x/2;
y1		= y/2;

height	= size(img,1);
%__________	Left
left	= center(1)-x1;
if	left>1			% crop
	croppix	= round(left+PRECISION);
	img	= img(:,croppix:end);
elseif	left<0			% fill
	fillpix	= round(abs(left-PRECISION));	
	img	= [zeros(height,fillpix) img];
end

%__________	Right
right	= center(1)+x1;
if	right>col+1		% fill
	fillpix	= round(right-(col+1)+PRECISION);
	img	= [img zeros(height,fillpix)];
elseif	right<col		% crop
	croppix	= round(col-right+PRECISION);
	img	= img(:,1:(end-croppix));	
end

width	= size(img,2);
%__________	Top
top	= center(2)-y1;
if	top>1			% crop
	croppix	= round(top+PRECISION);
	img	= img(croppix:end,:);
elseif	top<0			% fill
	fillpix	= round(abs(top));	
	img	= [zeros(fillpix,width);img];
end

%__________	bottom	
bot	= center(2)+y1;
if	bot>row+1		% fill
	fillpix	= round(bot-(row+1)+.5);
	img	= [img;zeros(fillpix,width)];
elseif	right<col		% crop
	croppix	= round(row-bot+PRECISION);
	img	= img(1:(end-croppix),:);	
end

cimg	= img;
%__________ REGULAR ends _______________________________________________________
else
%__________ DEMO _______________________________________________________________
clf;clc;
disp('running lans_imgcrop.m in demo mode');
img	= [0 0 0 1; 0 1 0 0;0 0 1 0;0 0 0 1];
cen	= lans_centroid(img);
[cimg]  = lans_imgcrop(img,cen,10,5);

subplot(1,2,1);
image(img*255);axis equal;
lans_plotmd(lans_centroid(img),'ro','-hold 1');
subplot(1,2,2);
image(cimg*255);axis equal;
lans_plotmd(lans_centroid(cimg),'ro','-hold 1');

%__________ DEMO ends __________________________________________________________
end
