%	lans_genball	- Generate uniform y within a ball in R^D
%	
%	[y,ball]	= lans_genball(<N><,rseed><,ball>)
%			  lans_genball(N,rseed,constraint)
%
%	_____OUTPUTS____________________________________________________________
%	y	D x N data points				(col vectors)
%	ball	specifications of data				(structure)
%
%	_____INPUTS_____________________________________________________________
%	N	# data points					(scalar)
%		defaults	100
%	rseed	random seed for rand
%	ball	structure					(ball)
%	*	.center		center of circle		(col vector)
%				defaults	[0 0 0]'
%	*	.radius		Outer radius of ball		(scalar)
%				defaults	1
%	*	.radiusIn	Inner radius of ball		(scalar)
%				defaults	0
%	*	.cons		constraints, if any		(string)
%				'none'
%				'hemisphere'
%	+	.rseed		random seed			(vector)
%
%		* optional, set to default if unspecified
%		+ read-only (i.e. specifying ball.rseed has no effect)
%
%	constraint	specify ball.cons			(string)
%
%	_____EXAMPLE____________________________________________________________
%	[y,ball]	= lans_genball(100,1,'hemisphere');
%
%	_____NOTES______________________________________________________________
%	- .center implicitly specifies dimension!
%	- Specify radiusIn to constrain points to lie within a shell between
%	  .radiusIn and .radius
%	- Due to curse of dimensionality and a uniform sampling method, higher
%	dimensions may take a excruciating long period of time to generate if
%	shell thickness is too thin
%	- 3rd arguement may either be a ball structure or constraint string
%
%	(C) 1999.08.14 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/


function	[y,ball]	= lans_genball(N,rseed,ball)

%_____	Default Ball
d_ball.center	= [0;0;0];
d_ball.radius	= 1;
d_ball.radiusIn	= 0;
d_ball.cons	= 'none';
d_ball.rseed	= sum(100*clock);

if nargin<3
	ball	= d_ball;
	if nargin<2
		if nargin<1
			N	= 100;
		end
	else
		ball.rseed	= rseed;
	end
else
	% if structure specified, check if all fields are specified
	if isstruct(ball)			% ball struct specified
		ball	= lans_default(ball,d_ball);
	else					% string specified
		constraint	= ball;
		ball		= d_ball;
		ball.cons	= constraint;
	end
end
%_____	Initialize random generator
rand('state',ball.rseed); randn('state',ball.rseed);

ro	= ball.radius;
ro2	= ro*ro;
ri	= ball.radiusIn;
ri2	= ri*ri;
D	= length(ball.center);

if strcmp(ball.cons,'hemisphere')
	cons	= 1;
else
	cons	= 0;
end

%_____	counters
sofar	= 0;		% # of valid data generated so far
remain	= N;		% # of remaining data to generate
%_____	pre-alocates y
y	= zeros(D,N);

while remain>0
	rdata	= (2*rand(D,remain*D)-1)*ro;
	if cons
		wheresat	= find(rdata(end,:)>0);
		rdata		= rdata(:,wheresat);
	end

	r2	= sum(rdata.*rdata);
	if ri2~=0
		goody	= find((r2<=ro2)&(r2>=ri2));
	else
		goody	= find(r2<=ro2);
	end
	[d,n]	= size(goody);
	if n>remain
		y(:,sofar+1:sofar+remain)	= rdata(:,goody(:,1:remain));	
		sofar	= sofar+remain;
		remain	= 0;
	else
		y(:,sofar+1:sofar+n)		= rdata(:,goody);
		sofar	= sofar+n;
		remain	= N-sofar;
	end
end
