%	lans_shape	- Generate normal samples about a shape
%	
%	y	= lans_shape(shape<,N><,sig><,rstate>)
%
%	_____OUTPUTS____________________________________________________________
%	y	normally distributed points about shape		(col vectors)
%
%	_____INPUTS_____________________________________________________________
%	shape	connected points				(col vectors)
%	N	# of points					(scalar)
%		100	default	
%	sig	noise level					(scalar)
%		.1 default
%	rstate	random state for randn				(scalar)
%
%	_____EXAMPLE____________________________________________________________
%	y	= lans_shape();
%
%	_____NOTES______________________________________________________________
%	- shape vectors assumed ordered and connected
%
%	_____SEE ALSO___________________________________________________________
%	lans_nonlinear
%
%	(C) 1999.08.11 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________
%	N is not very useful

function	[y] = lans_shape(shape,N,sig,rstate)

if nargin<4
	rstate	= sum(100*clock);
	if nargin<3
		sig	= .05;
		if nargin<2	
			N	= 100;
		end
	end
end

[D M]	= size(shape);
nseg	= M-1;
eachseg	= round(N/nseg);

y	= [];
for seg=1:nseg
	theseg	= shape(:,seg:seg+1);
	alpha	= (1:eachseg)/eachseg;
	mu	= theseg(:,1)*(1-alpha) + theseg(:,2)*alpha;
	y	= [y mu+sig*randn(size(mu))];
end

%y	= y(:,1:N);
