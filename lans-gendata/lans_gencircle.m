%	lans_gencircle	- Generate 2-D circle with indept. Gaussian noise
%	
%	[y]	= lans_gencircle(N[,rseed[,circle]])
%
%	_____OUTPUTS____________________________________________________________
%	y	D x N data points				(col vectors)
%
%	_____INPUTS_____________________________________________________________
%	N	# data points					(scalar)
%		defaults	100
%	rseed	random seed for rand and randn			(scalar)
%				default		not set
%	circle	structure					(structure)
%	*	.radius		radius of circle		(scalar)
%				defaults	1
%	*	.center		center of circle		(col vector)
%				defaults	[0 0]'
%	*	.var		variance of noise about circle	(col vector)
%				isotropic
%				defaults	[.1;.1]^2
%				.var has PREFERENCE over .sd
%	*	.sd		or standard deviation		(col vector)
%				defaults	[.1 .1]
%	*	.cycle		cycle				(scalar)
%				defaults	1
%
%	* optional, set to defaults if unspecified
%
%	_____NOTES______________________________________________________________
%	- Points on the circle are generated using uniform distribution
%	- Noise are independent gaussian about circle
%	- if both .var and .sd are specified, then .var takes preference
%	- one can specified just .sd or .var
%	- Centered @ origin
%
%	_____SEE ALSO___________________________________________________________
%
%	(C) 1999.05.03 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[y]	= lans_gencircle(N,rseed,circle)

%_____	Defaults
d_circle.center	= [0;0];
d_circle.cycle	= 1;
d_circle.radius	= 1;
d_circle.sd	= [.1;.1];
d_circle.var	= d_circle.sd.*d_circle.sd;

if nargin<3
	circle	= d_circle;
	if nargin<2
		if nargin<1
			N	= 100;
		end
	else
		rand('state',rseed);
		randn('state',rseed);
	end
else
	%_____	check if only one of .sd or .var is specified
	if isfield(circle,'sd')
		circle.var	= circle.sd.*circle.sd;
	end
	circle	= lans_default(circle,d_circle);
	rand('state',rseed);
	randn('state',rseed);
end

vd	= length(circle.var);
cd	= length(circle.center);

%_____	check if only one of center or variance was specified
if vd>cd
	circle.center	= zeros(vd,1);
elseif vd<cd
	circle.var	= circle.var(1)*ones(cd,1);	
end

if length(circle.center)~=2
	error('Only 2-D data can be generated');
end
wholec	= circle.cycle*2*pi;
Rr	= sqrt(circle.radius);
t	= rand(1,N)*wholec;
py	= [Rr*sin(t);Rr*cos(t)];
noise	= randn(2,N).*(circle.sd*ones(1,N));

y	= py+noise;
