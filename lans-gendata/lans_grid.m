%	lans_grid	- Generate regularly spaced points on rectangular grid
%	
%	y	= lans_grid(xlist,ylist)
%
%	_____OUTPUTS____________________________________________________________
%	y	points on rectangular grid			(2 x nx x ny)
%
%	_____INPUTS_____________________________________________________________
%	xlist   list of x points				(degrees)
%	ylist	list of y points				(degrees)
%
%	_____EXAMPLE____________________________________________________________
%	y	= lans_grid(0:10:350,-90:10:90);
%
%	_____NOTES______________________________________________________________
%	for demo, call function without parameters
%
%	_____SEE ALSO___________________________________________________________
%	lans_pcgrid	lans_sphere
%
%	(C) 1999.08.10 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________

function	[y] = lans_grid(xlist,ylist)

if nargin>0
%__________ REGULAR ____________________________________________________________
if nargin<3
	options	= '';
end

%__________	generate raw grid
[xgrid,ygrid]	= meshgrid(xlist,ylist);
y(1,:,:)	= xgrid';
y(2,:,:)	= ygrid';

%__________ REGULAR ends _______________________________________________________
else
%__________ DEMO _______________________________________________________________
clf;clc;
disp('running lans_grid.m in demo mode');
y	= lans_grid(0:10:350,-90:10:90);
lans_plotmd(y,'r.');
%__________ DEMO ends __________________________________________________________
end
