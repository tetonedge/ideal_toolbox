%	lans_sphere	- Generates angular spaced points on a unit sphere (3-D)
%	
%	[y,nnd]	= lans_sphere(elist,rlist)
%		= lans_sphere(elist,rlist,options)
%
%	_____OUTPUTS____________________________________________________________
%	y	points of y on a rect. grid			(cell/matrix)
%		y{1}	3 x nr x ne 	if repeat poles
%			3 x nr x (ne-2)	otherwise
%		y{2}	3 x 2		if repeat, holds {south,north} poles 
%
%	nnd	Nearest Neighbor Euclidean distances		(cell/matrix)
%		nnd{1}	follow y's topology
%		nnd{2}	follow y's topology
%	
%	_____INPUTS_____________________________________________________________
%	elist   list of elevations (rotate about x axis)	(degrees)
%		-90 < elist < 90
%			OR
%		# of distinct uniform elevations		(scalar)
%	rlist	list of rotations (rotate about y axis) 	(degrees)
%		0  <= rlist < 360
%			OR
%		# of distinct uniform rotations			(scalar)
%	options							(string)
%		-repeat		whether to duplicate poles	(binary)
%			0	No (default)
%			1	Yes
%		-linear
%			0	No (default)
%			1	output as 1/3 x N matrix instead of cell
%		-eint	overiding elevation interval		(scalar)
%		-rint	overiding rotation interval		(scalar)
%
%	_____EXAMPLE____________________________________________________________
%	y	= lans_sphere(-90:10:90,0:10:350,'-repeat 1');
%	y	= lans_sphere(3,10);
%
%	_____NOTES______________________________________________________________
%	for demo, call function without parameters
%	- if repeat=0, y,nnd will be cells with 2nd cell containing pole data
%	- y{1}(:,x1,x2) will specify a 3-D vector y3 = (x1,x2,x3)
%	- grid ordering follows Cartesian convention (NOT row,col), i.e.
%		y(:,x,y)
%	  refers to a point on discrete manifold grid (x,y)
%	- For repeated poles, do not execute lans_plotmd(y(:,:,end)) or 
%	  lans_plotmd(y(:,:,1)), as limited precision will cause matlab to hang
%	- eint or rint overrides elist or rlist if either is specified
%
%	_____SEE ALSO___________________________________________________________
%	lans_pose2cart
%
%	(C) 2000.02.26 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________
%	check validity of pose2cart

function	[y,nnd] = lans_sphere(elist,rlist,options)

if nargin>0
%__________ REGULAR ____________________________________________________________
if nargin<3
	options	= '';
end

linear	= paraget('-linear',options,0);
repeat	= paraget('-repeat',options,0);	
eint	= paraget('-eint',options,0);
rint	= paraget('-rint',options,0);

if (length(elist)==1)&(length(rlist)==1)
	e_num	= elist;
	if e_num>1
		elist	= lans_scale(1:enum,-90:90);
	else
		elist	= 0;
	end
	r_num	= rlist;
	rlist	= lans_scale(1:(r_num+1),0,360);
	rlist	= rlist(1:end-1);
end

%_____	overrides all other settings if eint or rint specified
if eint
	elist	= -90:eint:90;
end
if rint
	rlist	= 0:rint:360;
	rlist	= rlist(1:end-1);
end

%_____	make longitude (r) and latitude (e) indices
[rgrid,egrid]	= meshgrid(rlist,elist);
pose(1,:,:)	= egrid';
pose(2,:,:)	= rgrid';
y1		= lans_pose2cart(pose);

if ~repeat
	npole		= find(elist==90);
	spole		= find(elist==-90);
	allelevations	= 1:size(y1,3);
	nonrepeat	= lans_remove([npole spole],allelevations);
	y{1}		= y1(:,:,nonrepeat); 
	y{2}		= round([y1(:,1,spole) y1(:,1,npole)]);
else
	y{1}		= y1;
end


%_____	Nearest Neighbor Euclidean distances of each point
if nargout>1
	% const. vertical distance
	vdist	= norm(y1(:,1,1)-y1(:,1,2));
	nnd1	= ones(1,size(y{1},3))*vdist;

	% compare with horiz distance at each elevation
	for vert=1:size(y{1},3)
		hdist		= norm(y{1}(:,1,vert)-y{1}(:,2,vert));
		nnd1(vert)	= min(nnd1(vert),hdist);
	end
	nnd1	= ones(size(y{1},2),1)*nnd1;

	if linear
		nnd	= [reshape(nnd1,1,prod(size(nnd1))) ones(1,size(y{2},2))*vdist];
	else
		nnd{1}	= nnd1;
		nnd{2}	= ones(1,size(y{2},2))*vdist;
	end
end

if linear
	y	= lans_md2lin(y);
end

%__________ REGULAR ends _______________________________________________________
else
%__________ DEMO _______________________________________________________________
clf;clc;
disp('running lans_sphere.m in demo mode');
[y,nnd]	= lans_sphere(-90:10:90,0:10:350,'-repeat 0');
size(y);
lans_plotmd(y{1},'y.');
lans_plotmd(y{1}(:,1,5),'ro','-hold 1');
lans_plotmd(y{1}(:,5,10),'ko','-hold 1');
lans_plotmd(y{1}(:,10,15),'bo','-hold 1');
lans_plotmd(y{2},'m+','-hold 1');

rotate3d on;
%__________ DEMO ends __________________________________________________________
end
