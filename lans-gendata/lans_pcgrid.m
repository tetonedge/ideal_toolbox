%	lans_pcgrid	- Generate regularly spaced points on PCA 1/2-D grid
%	
%	[y,svar]	= lans_pcgrid(xlist,ylist,ydata)
%			  lans_pcgrid(xlist,ydata)
%
%	_____OUTPUTS____________________________________________________________
%	y	points on a Principal Component Surface grid	(2 x nx x ny)
%	svar	sorted variance/eigenvalues			(vector)
%
%	_____INPUTS_____________________________________________________________
%	xlist   list of x points				(degrees)
%	ylist	list of y points				(degrees)
%	ydata	raw data
%
%	_____EXAMPLE____________________________________________________________
%	y	= lans_pcgrid(0:10:350,-90:10:90,rand(2,10));
%
%	_____NOTES______________________________________________________________
%	for demo, call function without parameters
%	- largest 2 principal components used to form a surface in R^D space
%	  (in descending order)
%	- first principal component used to form a line in R^D space
%	- raw nodes first standardized to zero mean unit variance, then scaled
%	  by the standard deviation of each component
%
%	_____SEE ALSO___________________________________________________________
%	lans_pca	lans_grid	lans_sphere
%
%	(C) 1999.10.14 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [y,svar]	= lans_grid(xlist,ylist,ydata)

if nargin>0
%__________ REGULAR ____________________________________________________________
FAC	= 1;		% # of standard deviations in each spread direction
if nargin==2
	ydata	= ylist;
end

%__________	compute PCA
D		= size(ydata);
[cy,ybar]	= lans_center(ydata);
[dum,svar,paxis]= lans_pca(cy,'-largest 3');
sig		= FAC*sqrt([svar(1) svar(2)]);

%__________	generate raw grid proportional to standard deviation along paxis
xlist		= sig(1)*lans_stand(xlist);
if nargin>2
	themat		= [paxis(:,1:2) ybar];	% ybar = mean
	ylist		= sig(2)*lans_stand(ylist);
	[xgrid,ygrid]	= meshgrid(xlist,ylist);
	c(1,:,:)	= xgrid';		% coefficient
	c(2,:,:)	= ygrid';		% coefficient
	c(3,:,:)	= ones(size(ygrid'));
	[clin,ydim]	= lans_md2lin(c);
	ylin		= themat*clin;
	y		= lans_lin2md(ylin,ydim);
else
	themat		= [paxis(:,1) ybar];
	c(1,:)		= xlist;	
	c(2,:)		= ones(size(xlist));
	y		= themat*c;	
end

%__________ REGULAR ends _______________________________________________________
else
%__________ DEMO _______________________________________________________________
clf;clc;
disp('running lans_pcgrid.m in demo mode');
N	= 100;
[x,ball]	= lans_genball(N);
ball.cons	= 'hemisphere';
[x,ball]	= lans_genball(N,0,ball);
x	= x+10;
y1	= lans_pcgrid(-5:5,x);		
y2	= lans_pcgrid(1:5,1:5,x);
lans_plotmd(x,'g.',y2,'bo-');
lans_plotmd(y1,'ro-','-hold 1');
legend('data','2-D PCA grid','1-D PCA line');
rotate3d on;
%__________ DEMO ends __________________________________________________________
end
