%	lans_nonlinear	- Generate selected examples of nonlinear points
%	
%	y	= lans_nonlinear(exampleset<,N><,D>)
%
%	_____OUTPUTS____________________________________________________________
%	y	nonlinearly distributed points			(col vectors)
%
%	_____INPUTS_____________________________________________________________
%	exampleset	name of example				(string)
%		'S'	S shaped points
%	N		# of points				(scalar)
%			100	default	
%	D		dimensionality {2,3}			(scalar)
%			2	default
%
%	_____EXAMPLE____________________________________________________________
%	y	= lans_nonlinear();
%
%	_____NOTES______________________________________________________________
%	for demo, call function without parameters
%
%	_____SEE ALSO___________________________________________________________
%	lans_sphere
%
%	(C) 1999.08.11 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________

function	[y] = lans_nonlinear(exampleset,N,D)

if nargin>0
%__________ REGULAR ____________________________________________________________
SIG	= .05;		% noise

if nargin<3
	D	= 2;
	if nargin<2
		N	= 100;
	end
end

switch exampleset,
	case 'sinh',
		t=0:.1:2*pi;
		shape(1,:)	= lans_scale(t,-.5,.5);
		shape(2,:)	= .5*lans_scale(sin(t),-.5,.5);
		y		= lans_shape(shape,N,SIG);
	case 'sinv',
		t=0:.1:2*pi;
		shape(2,:)	= lans_scale(t,-.5,.5);
		shape(1,:)	= .5*lans_scale(sin(t),-.5,.5);
		y		= lans_shape(shape,N,SIG);
	case 'S',
		shape(1,:)	= [-.5 .5 .5 -.5 -.5 .5];
		shape(2,:)	= [-.5 -.5 0 0 .5 .5];
		y		= lans_shape(shape,N,SIG);
	case 'Z',
		shape(1,:)	= [-.5 .5 -.5 .5];
		shape(2,:)	= [.5 .5 -.5 -.5];
		y		= lans_shape(shape,N,SIG);
	otherwise,

end

%__________ REGULAR ends _______________________________________________________
else
%__________ DEMO _______________________________________________________________
clf;clc;
disp('running lans_nonlinear.m in demo mode');
y	= lans_nonlinear('S');
lans_plotmd(y,'r.');
%__________ DEMO ends __________________________________________________________
end
