%	tallyc		- Tally and sort classes 
%
%	[gdata,gindex,necl,cinfo]= tallyc(ldata[,cinfo])
%
%	_____OUTPUT_____________________________________________________________
%	gdata	class sorted labeled data			(col vectors)
%	gindex	index of class members w.r.t. gdata		(row vectors)
%		only of those that exists in ldata/gdata
%	necl	list of non-empty class labels			(row vector)
%		e.g. if complete class labels = [1 2 5 4]
%		necl = [2 5] => only class 2,5 samples were present
%		length(necl)= total # of non-empty classes
%	cinfo	FULL class membership tally table		(row vectors)
%		[classlabel size]
%		useful for subsequent tallying only
%
%	_____INPUT______________________________________________________________	
%	ldata	raw unsorted labeled data			(col vectors)
%		(last row is class label)
%	cinfo	FULL class membership tally table template	(row vectors)
%		[classlabel size]
%		useful for subsequent tallying only
%
%	_____SEE ALSO___________________________________________________________
%	group
%
%	(C) 1998.2.18 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [gdata,gindex,necl,cinfo]= tallyc(ldata,cinfo)

if isempty(ldata)
	gdata	=0;
	gindex	=0;
	necl	=0;
	cinfo	=0;
	return;
end

[d1,n]	= size(ldata);
d	= d1-1;
[gdata,gindex,numberclasses]= group(ldata);

necl	= gindex(:,1)';

if nargin<2				% tally total # of classes/items
	cinfo	= [gindex(:,1) gindex(:,3)-gindex(:,2)+1];
else
	cinfo(:,2)	= zeros(size(cinfo(:,2)));		%reset to 0
	for i=1:numberclasses
		thislabel	= gindex(i);	
		cinforow	= find(cinfo(:,1)==thislabel);
		cinfo(cinforow,2)= gindex(i,3)-gindex(i,2)+1;
	end
end

