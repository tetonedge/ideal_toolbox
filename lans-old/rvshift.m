%	rvshift		- Shift a single record in from end, displacing head of record
%
%	function [vrecord]	= rvshift(data1,vrecord);
%
%	INPUTS
%	======
%	data1	: single data record to be appended at end of vrecord
%	vrecord	: a vertical record
%
%	OUTPUTS
%	=======
%	vrecord	: appended vrecord
%	(C) 2000.06.28 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [vrecord]	= rvshift(data1,vrecord);

[rrow,rwidth]		= size(vrecord);
[dheight,dwidth]	= size(data1);

nrecords		= rrow/dheight;	
%(assuming data1 has same # of rows as records)

%peform shift up
vrecord(1:rrow-dheight,:)= vrecord(dheight+1:rrow,:);
vrecord	= rvput(data1,nrecords,vrecord);
