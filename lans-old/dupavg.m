%	dupavg		- Average values dictated by repeating row 
%
%	[udata,ux,w]	= dupavg(sdata,sx)
%
%	_____OUTPUTS____________________________________________________________
%	udata	averaged data w.r.t. each repeating x		(col vectors)
%	ux	unique x					(row vector)
%	w	specifying # repetitions for each repeated sx	(col vector)
%	
%	_____INPUTS_____________________________________________________________
%	sdata	data dependent on x				(col vectors)
%	sx	sorted repeating data				(row vector)
%
%	_____EXAMPLE____________________________________________________________
%
%
%	_____NOTES______________________________________________________________
%	(sx,sdata) are assumed to be sorted w.r.t. sx!!!!
%
%	_____SEE ALSO___________________________________________________________
%
%	(C) 1998.09.04 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [udata,ux,w]	= dupavg(sdata,sx)

[d,n]	= size(sdata);

[ux,ustart]	= lans_finduniq(sx);
uend		= [ustart(2:length(ustart))-1 n];
udata		= sdata(:,ustart);

w		= uend-ustart+1;		% weight = # repetitions
dupos		= find(w~=1);	% position in udata which is duplicated

for j=1:length(dupos)
	startsum		= ustart(dupos(j));
	endsum			= uend(dupos(j));
	udata(:,dupos(j))	= mean(sdata(:,startsum:endsum)')';
end
