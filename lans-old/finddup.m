%	finddup		- Categorize consecutive repetitions in 1D string/numbers
%
%	[ind]	= finddup(data)
%
%	_____OUTPUTS____________________________________________________________
%	ind	col indices of duplicating data w.r.t. data	(row vectors)
%		[startindex endindex count]
%		...
%
%	_____INPUTS_____________________________________________________________
%	data	non-unique data					(vector)
%
%	_____EXAMPLE____________________________________________________________
%
%	finddup([3 3 5 5 6 6 6 3]) returns
%
%	ans =
%
%	1     2     2
%	3     4     2
%	5     7     3
%
%
%	(C) 1998.4.20 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[ind]	= finddup(data)

if (min(size(data)))>1
	error('data is not a vector!');
end
dlength	= length(data);

likenext= [data(1:dlength-1)==data(2:dlength) 0];
likeprev= [0 likenext(1:dlength-1)];

if sum(likeprev)~=0
	ind	= [killadj(find(likenext==1))' killadj(find(likeprev==1),1)'];
	ind(:,3)= ind(:,2)-ind(:,1)+1;
else
	ind	= [];
end
