%	rvmake		- Create empty vertical record
%
%	[vrecord,vrpara]	= rvmake(rno,rrowsize,rcolsize);
%
%	_____OUTPUT_____________________________________________________________
%	vrecord	: a vertical record with (rno) vertical records
%		  each record has (rrowsize) rows
%		  left top # gives # of actual cols used in each record
%		  (used internally, invisible to user)
%	vrpara	: [rno rrowsize rcolsize]
%
%	_____INPUT______________________________________________________________
%	rno	: # records (vertical, top to bottom)
%	rrowsize: # rows per record
%	rcolsize: # cols per record
%
%	_____SEE ALSO___________________________________________________________
%	rvput rvget rvshift
%
%	_____EXAMPLE____________________________________________________________
%	[v vp]	= rvmake(3,2,4) returns
%v =
%	0     0     0     0     0
%	0     0     0     0     0
%	0     0     0     0     0
%	0     0     0     0     0
%	0     0     0     0     0
%	0     0     0     0     0
%vp =
%
%	3     2     4
%
%	(C) 1997.12.21 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [vrecord,vrpara]	= rvmake(rno,rrowsize,rcolsize);

vrecord	= zeros(rno*rrowsize,rcolsize+1);
vrpara	= [rno rrowsize rcolsize];
