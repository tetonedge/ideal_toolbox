%	vdist2		- Compute squared L2 metric of 2 col vectors (faster)
%
%	[dist2]	= vdist2(a[,b])
%
%	_____OUTPUTS____________________________________________________________
%	dist2	squared L2 distance				(row vector)
%
%	_____INPUTS_____________________________________________________________
%	a	single or multiple col vectors or 0		(col vectors)
%	b	single or multiple col vectors or 0		(col vectors)
%		optional
%
%	_____EXAMPLE____________________________________________________________
%	vdist2([1 2 3 4;1 2 3 4],0)	= [2 8 18 32];
%
%	(C) 1998.09.04 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[dist2]	= vdist2(a,b)

if nargin<2,
	b	= 0;
end

[d,n]	= size(a);

vdiff	= a-b;
dist2	= vdiff.*vdiff;

if d>1
	dist2	= sum(dist2);
end;
