%	lans_cplotmd	- Plot d-dimensional labelled vector 
%
%	[]	= lans_cplotmd(pdata)
%		  lans_cplotmd(pdata,ctype)
%		  lans_cplotmd(pdata,[ctype],options)
%		  lans_cplotmd(pdata,[ctype],ldata,[ltype])
%		  lans_cplotmd(pdata,[ctype],ldata,[ltype],options)
%
%	_____INPUTS_____________________________________________________________
%	pdata	labelled (d+1)-dimensional vector		(col vectors)
%		(d+1)th dimension contains label
%	ctype	plot type					(cell)
%		[]	uses lans_gensymbol.m
%	ldata	labelled (d+1)-dimensional vector		(col vectors)
%		(d+1)th dimension contains label
%		(ordered for line)
%	ltype	linetype					(string)
%		[]	uses lans_gensymbol.m
%		
%	options	see lanspara.m					(string)
%		-axis
%		-box
%		-grid
%		-plotdim
%		-hold
%
%	_____NOTES______________________________________________________________
%	- for demo, call functions without parameters
%	- plots a 3-D graph for >=3-D pdata unless plotdim is specified
%	- hold applies only to active subplot
%
%	_____SEE ALSO___________________________________________________________
%	lans_plotmd	lans_gensymbol
%
%	(C) 1999.04.18 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[]	= lans_cplotmd(pdata,ctype,ldata,ltype,options);

if nargin>0
%__________ REGULAR ____________________________________________________________
%----------	default options
if nargin<5
	if nargin<4
		if nargin<3
			options	= [];
			if nargin<2		% nothing specified
				ctype	= []; 
			end
		else
			options	= ldata;
		end
		ltype	= [];
	else
		options	= [];
	end
end

%----------	get parameters
axisit		= paraget('-axis',options);
boxit		= paraget('-box',options);
gridit		= paraget('-grid',options);
plotdim		= paraget('-plotdim',options);
holdplot	= paraget('-hold',options);

%----------	separate classes
[d1,n1]		= size(pdata);
d		= d1-1;
[pdata,gi1,nc1]	= group(pdata);
if nargin>3
	[ldata,gi2,nc2]	= group(ldata);
end
%----------	set plot parameters
if ~iscell(ctype)
	ctype	= lans_gensymbol(nc1);
end

if ~iscell(ltype)
	ltype	= ctype;
end

%----------	Plot the points and lines
options	= paraset('-hold',1,options);
for c	= 1:nc1
	crange1	= gi1(c,2):gi1(c,3);
	cdata1	= pdata(1:d,crange1);
	ccolor	= char(ctype(c));
	lcolor	= char(ltype(c));
	if nargin>3
		crange2	= gi2(c,2):gi2(c,3);
		cdata2	= ldata(1:d,crange2);
		lans_plotmd(cdata1,ccolor(1:2),cdata2,lcolor,options);
	else
		lans_plotmd(cdata1,ccolor(1:2),options);	
	end
end
%__________ REGULAR ends _______________________________________________________
else
%__________ DEMO _______________________________________________________________
clf;clc;
disp('running lans_cplotmd.m in demo mode');
nc	= 10;
cn	= 10;
y	= zeros(2,nc*cn);
x	= 0:(2*pi/(cn-1)):2*pi;
for c=1:nc
	y(1:2,(nc*(c-1)+1):(nc*c))	= [x;sin(x-.2*c)];
end
y(end+1,:)	= reshape(meshgrid(1:cn,1:nc),1,nc*cn);
lans_cplotmd(y,[],y,[]);

%__________ DEMO ends __________________________________________________________
end
