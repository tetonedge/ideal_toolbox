%	tryerr		- Invoke error and halt if value evaluates to true
%
%	[]	= tryerr(value[,emsg])
%
%	_____INPUT______________________________________________________________
%	value	value						(scalar)
%	emsg	error message to show				(string)
%
%	_____EXAMPLE____________________________________________________________
%	tryerr(3>2,'bigger') returns
%	??? Error using ==> tryerr
%	bigger
%
%	(C) 1998.2.18 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[]	= tryerr(value,emsg)

if nargin==1
	emsg	= 'Error in assertion';
end

if value==1
	error(emsg);
end;
