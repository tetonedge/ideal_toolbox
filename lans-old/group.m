%	group		- Gather/sort same class data and indexes them 
%
%	[gdata,gindex,nclass]= group(ldata[,sortit])
%
%	_____OUTPUT_____________________________________________________________
%	gdata	grouped & sorted data				(col vectors)
%	gindex	[classlabel startindex endindex]		(row vectors)
%			...
%	nclass	# classes					(scalar)
%
%	_____INPUT______________________________________________________________
%	ldata	class labeled data				(col vectors)
%		(last row contains labels)
%	sortit	indicate whether to sort data
%		{0,1}	default = 1
%
%	_____EXAMPLE____________________________________________________________
%	[gdata,gindex,nclass]=group([.1 .2 .1 .3;.1 .2 .1 .3;1 2 1 3]) returns
%
%gdata =
%	0.1000    0.1000    0.2000    0.3000
%	0.1000    0.1000    0.2000    0.3000
%	1.0000    1.0000    2.0000    3.0000
%
%gindex =
%	1     1     2
%	2     3     3
%	3     4     4
%
%nclass =
%	3
%
%	(C) 1998.09.04 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/


function [gdata,gindex,nclass]= group(ldata,sortit)
if nargin<2
	sortit=1;
end

[d1,n]		= size(ldata);

if sortit
	[slab,ind]	= sort(ldata(d1,:));	% sorted w.r.t. labels
	gdata		= ldata(:,ind);
else
	slab	= ldata(d1,:);
	gdata	= ldata;
end

aclass		= lans_finduniq(slab)';	% tabulate classes
nclass		= length(aclass);

for i=1:nclass				% create gindex
	thislab		= aclass(i);
	batch		= find(slab==thislab);
	gindex(i,:)	= [thislab min(batch) max(batch)];
end
