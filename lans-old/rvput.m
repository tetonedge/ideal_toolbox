%	rvput		- Put a single record into existing vertical data record postion
%
%	function [vrecord]	= rvput(data1,rno,vrecord);
%
%		mneumonic	put 'data1' at 'rno' of 'vrecord'
%
%	INPUTS
%	======
%	data1	: single data record
%	rno	: record # to put datablock
%	vrecord	: a vertical record
%
%	OUTPUTS
%	=======
%	vrecord: appended new vertical record
%
%	(C) 2000.06.28 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [vrecord]	= rvput(data1,rno,vrecord);

[dheight,dwidth]	= size(data1);

rstart	= (rno-1)*dheight+1;
rend	= rstart+dheight-1;

vrecord(rstart:rend,2:dwidth+1)	= data1;
vrecord(rstart,1)		= dwidth;
