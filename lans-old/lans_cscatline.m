%	lans_cscatline	- Plot class labeled data and regressed line
%
%	[]	= lans_cscatline(ldata,rdata,options)
%
%	_____INPUTS_____________________________________________________________
%	ldata	class labeled data				(col vectors)
%	rdata	regressed data					(col vectors)
%	options							[structure]
%	       cindex	color index				(scalar)
%			1,2,...	add to current plot
%			default:	0 (overwrites)
%	       close	{0,1}	regressed line type		(scalar)
%			default		0 (open)
%	       dim	{2,3}	dimension of plots		(scalar)
%			default:	2
%
%	_____SEE ALSO___________________________________________________________
%	lans_scatline
%
%	(C) 1999.02.25 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

%	_____TO DO______________________________________________________________
%	*** need to solve problem between difference between
%		# points and # points on curve

function []	= lans_cscatline(ldata,rdata,options)

[d1,n]		= size(ldata);
d		= d1-1;
[rdata,gi,nc]	= group(rdata);
[ldata,gi,nc]	= group(ldata);

for i=1:nc
	crange		= gi(i,2):gi(i,3);
	cdata		= ldata(1:d,crange);
	crdata		= rdata(1:d,crange);
	cindex		= paraget('-cindex',options);
	lans_scatline(cdata,crdata,options.cindex,options);
end
