%	paraget		- Extract '-para val' parameters from parameter string
%
%	[paras,option]	= paraget(pnames,pstring<,dparas>)
%
%	_____OUTPUTS____________________________________________________________
%	paras	numeric parameter values or 1st string		(row vector)
%		returns default values dparas if pstring does not contain
%		pnames
%		otherwise [] is return
%	option	set values (for explicity putting default values in pstring)
%		
%	_____INPUTS_____________________________________________________________
%	pnames	dashed header names of desired parameters	(string)
%	pstring	string containing parameters			(string)
%	dparas	Default values overiding those specified here	(row vector)
%
%	_____EXAMPLE____________________________________________________________
%	paraget('-maxiter -span','-maxiter 10 -span .1') returns [10 .1]
%	paraget('-maxiter -span','-maxiter -span') returns default values
%
%	_____NOTES______________________________________________________________
%	- dparas OVERRIDES default values if none specified in pstring
%	- To return non-numeric parameter values, specify only ONE pname
%	  e.g. paraget('-outputfile','-outputfile iris.lans') returns 'iris.lans'	
%	- CANNOT accept null pname in pstring, please remove it instead
%	  e.g. paraget('-outputfile','-outputfile -smoother 1') FAILS
%
%	_____SEE ALSO___________________________________________________________
%	paraset loadpara
%
%	(C) 2003.07.29 Kuiyu Chang
%	http://www.lans.ece.utexas.edu/~kuiyu
%
%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/
%	_____TO DO______________________________________________________________
%	rid dependence on default values specified here
%----------	Default parameter values shown
%
%-algo		2						(integer)
%		algorithm
%			1	Hastie & Stuetzle
%			2	Banfield & Raftery
%			3	Tibshirani EM
%			4	GTM based Principal Surface
%
%-attenuate	1						(scalar)
%-axis		1						(binary)
%		axis on/off
%
%-basedir	pwd						(string)
%		path of base simulation
%
%-beta		generic scalar					(scalar)
%		0	
%-box		0						(binary)
%		Turn box option on/off in plots
%
%-clos 		0						(binary)
%		open/closed curve
%
%-covarclip	0						(binary)
%		clip covariance values to covarmin & covarmax
%		if set to 0, the limits have no effect
%
%-covarmax	9999						(scalar)
%		maximum covariance*
%
%-covarmin	eps						(scalar)
%		minimum covariance*
%			* singular values are examined for full covars
%
%-covartype	'spherical'					(string)
%		covariance type for Gaussian mixture model
%			'spherical'
%			'diagonal'
%			'full'
%
%-discard	0						(binary)
%		discard zero eigen-pair?
%	
%-fix		'none'						(string)
%		fix center or variances
%			'centers'	fix the Gaussian centers
%			'covariances'	fix covariances
%
%-gradient	'right'						(string)
%		gradient approximation method
%			'right'		% use right segment
%			'left'		% use left segment
%			'avg'		% use average of left and right segment
%			'wavg		% length weighted version of 'avg'
%
%-hold		0						(binary)
%		toggles holding of plot
%
%-inputfile	'iris.lans'					(string)
%		input path/filename (dataset)
%	
%-init		1						(integer)
%		principal curve initialization
%			1	principal eigenvector direction
%			2	small random values about mean
%
%-intgap	.5						(scalar)
%		fraction of interval for variance to span
%
%-iter		20						(integer)
%		# iterations (max)
%
%-kernel	1						(integer)
%		neighborhood function
%			1	symmetric cubic kernel
%
%-L		0						(integer)
%		# latent basis functions (for GTM)
%		0 means it will be computed w.r.t. # latent vectors
%
%-lambda	1						(scalar)
%		Generic numerical variable
%
%-ldacrit	'fisher1'					(string)
%		LDA criterion
%			fisher1	Generalized LDA
%			fisher2	Generalized LDA using total scatter
%
%-logfile	'iris.log'					(string)
%		log path/filename
%		
%-manifold	'1line'						(string)
%		manifold type
%			'1line'
%			'2square'
%			'2hex'		(unimplemented)
%			'3sphere'
%		
%-markline	0						(binary)
%		whether to place a marker on line
%
%-minprior	1e-2						(scalar)
%		minimum prior probability value (% of 1/N)
%
%-mode		1						(scalar)
%		GTM mode of calculation {0,1,2}
%			0 fast
%			1 slower but more accurate
%			2 very slow and accurate
%-mtol		1e-3						(scalar)
%		stopping criterion, tolerance in mse change
%			vamse	= projdist(pcurve,points)
%			abs(vamse-oldvamse)/oldvamse
%
%-M		0						(integer)
%		# of components in mixture
%			0 sets M = N, # of sample points
%
%-orient	0						(scalar)
%		specifies covariance orientation 
%		0	No orientation
%		>0	perpendicular factor
%		<0	parallel factor
%
%-outputfile	'iris.out'					(string)
%		path/filename for result
%
%-pinittype	1						(integer)
%		principal curve initialization
%			1	principal eigenvector direction
%			2	small random values about mean
%			3	var. about data (ordered)
%
%-plotdim	2						(integer)
%		dimensions of each subplot
%			{2,3}
%
%-prec		1e-7						(scalar)
%		precision for various tuning operations, in particular
%			- discarding dimensions
%			- projection
%
%-regularize	0						(scalar)
%		regularization parameter
%
%-rsort		1						(binary)
%		whether to sort the projected knots
%
%-recompute	'unitinterval'					(string)
%		knot computation method (for smoothing EM)
%			'unitinterval'	unit interval
%			'unitspeed'	arc length
%			'projection'	projection onto principal surfaces
%			'optimized'	based on optimizing modified Likelihood	
%					(unimplemented)
%
%-s		1						(scalar)
%		fraction of dist. to nearest basis function to use as the
%		width (standard deviation) of GTM latent basis functions
%
%-scale		0						(scalar)
%		set scale invariance
%		if not zero, specify beta
%
%-showg		0						(binary)
%		whether to display graphical plot of pcurve
%
%-showt		0						(binary)
%		whether to display text of iteration
%
%-smoopara	1						(scalar)
%		smoother parameter
%		see llr.m, lwavg.m, cspline.m
%
%-smoother	3						(integer)
%		smoother type
%			0	none (for EM algorithm only)
%			1	local weighted regression
%				smoopara = polynomial order (1,2,3,...)
%			2	local weighted average
%			3	cubic smoothing splines (weighted)
%
%-span		0.5						(scalar)
%		initial span [0,1]
%
%-spdec		.1						(scalar)
%		span decrement [0,1] (fraction of initial span)
%
%-spmin		.1						(scalar)
%		minimum span [0,1] (fraction of initial span)
%
%-sptol		1e-2						(scalar)
%		tolerance for decreasing span
%
%-tol		1e-2						(scalar)
%		generic tolerance parameter
%
%-translation	0						(binary)
%		toggles translation invariance
%
%-win		10						(integer)
%		window size (for detecting trends)
%		1,2,3,...,iter	

function [paras,option]	= paraget(pnames,pstring,dparas)

prec	= 1e-7;		% precision

tab	= 9;		% ASCII code for tab
spc	= 32;		% ASCII code for space
lfd	= 10;		% ASCII code for linefeed
msg	= 13;		% ASCII code for DOS CTRL-M	

paras	= [];

if ~isempty(pstring)
	%========== preprocess
	%----- append 1 space to end
	pnames(length(pnames)+1)	= 32;
	pstring(length(pstring)+1)	= 32;
	
	%----- kill dos linefeeds, replace linefeeds by spaces and merge adjacent spaces
%	pnames	= strfilter(pnames,'killchar',msg);		% ok because this is
%	pnames	= strfilter(pnames,'replace',[lfd spc]);	% usually a string
	pnames	= strfilter(pnames,'merge',spc);
	
%	pstring	= strfilter(pstring,'killchar',msg);		%taken care by loadpara.m
%	pstring	= strfilter(pstring,'replace',[lfd spc]);	%taken care by loadpara.m
	pstring	= strfilter(pstring,'merge',spc);
	%----- remove first space if exist
	if pnames(1)==spc
		pnames	= pnames(2:length(pnames));
	end;
	if pstring(1)==spc
		pstring	= pstring(2:length(pstring));
	end;
		
	%----- tally # of parameters requested (separated by spaces)
	spc1	= find(pnames==spc);		% # parameters wanted
	spc2	= find(pstring==spc);		% 2*above
	npara	= length(spc1);
	
	%========== extract
	%-----	assumes each pname in pnames is followed by a space
	%-----	if a string is found among paras, loop is terminated immediately
	prvspc		= 0;
	for i=1:npara	
		pname	= pnames(prvspc+1:spc1(i)-1);	% current parameter name
		pstart	= findstr([pname ' '], pstring);	% find it in pstring

		if ~isempty(pstart)
			pstart	= max(pstart);			% take last one
				lowb	= lans_lub(spc2,pstart);		% space after pname
			if lowb<max(spc2)
				upb	= spc2(find(spc2==lowb)+1);	% 2nd space after pname
				pstr	= pstring(lowb:upb);
% the following will invoke the corresponding command if pstr is a reserved word
				thenum	= str2num(pstr);
				if thenum-str2num(num2str(thenum))<prec
					% its a number!
					paras(i)	= thenum;
				else
					% its a string
					paras	= pstr(2:length(pstr)-1);
					break
				end

				%-----	check if string or number
				%-----	added 2nd condition for matcom compatibility
				% fails if pstr is less than thenum's sig # of figures
%				plen	= length(pstr)-2;
%				relen	= length(num2str(thenum));			
%				if (isempty(thenum)|(relen<plen))
%					if pstr(2)~='-'
%						paras	= pstr(2:length(pstr)-1);
%						break;
%					end;
%				else	
%					paras(i)= str2num(pstr);	% its a number
%				end;	
			else
				error('Error in parameter string');
			end;
		end;
		prvspc	= spc1(i);
	end;
end;

%Default values assigned here!!!
if isempty(paras)
	lpn	= length(pnames);
	if pnames(lpn)==' '
		pnames	= pnames(1:lpn-1);
	end
	pnames	= lower(pnames);
	if nargin==3
		paras	= dparas;	% inline default values override
	else
	  switch pnames
		case '-algo'
			paras	= 2;
		case '-alpha'
			paras	= 1;
		case '-attenuate'
			paras	= 1;
		case '-axis'
			paras	= 1;
		case '-basedir'
			paras	= pwd;
		case '-beta'
			paras	= 0;
		case '-box'
			paras	= 0;
		case '-clos'
			paras	= 0;
		case '-covarclip'
			paras	= 0;
		case '-covarmax'
			paras	= 9999;
		case '-covarmin'
			paras	= eps;
		case '-covartype'
			paras	= 'spherical';
		case '-discard'
			paras	= 0;
		case '-fix'
			paras	= '';
		case '-gradient'
			paras	= 'right';
		case '-inputfile'
			paras	= 'iris.lans';
%		case '-init'
%			paras	= 1;
		case '-intgap'
			paras	= .5;
		case '-iter'
			paras	= 20;
		case '-kernel'
			paras	= 1;
		case '-lambda'
			paras	= 1;
		case '-ldacrit'
			paras	= 'fisher1';
		case '-logfile'
			paras	= 'iris.log';
		case '-l'
			paras	= 0;
		case '-manifold'
			paras	= '1line';
		case '-markline'
			paras	= 0;
		case '-minprior'
			paras	= 1e-2;
		case '-mode'
			paras	= 1;
		case '-mtol'
			paras	= 1e-3;
		case '-m'
			paras	= 0;
		case '-orient'
			paras	= 0;
		case '-outputfile'
			paras	= 'iris.out';
		case '-pinittype'
			paras	= 1;
		case '-plotdim'
			paras	= 2;
		case '-hold'
			paras	= 0;
		case '-prec'
			paras	= prec;
		case '-regularize'
			paras	= 0;
		case '-recompute'
			paras	= 'unitspeed';
		case '-rsort'
			paras	= 1;
		case '-s'
			paras	= 1;
		case '-scale'
			paras	= 0;
		case '-showg'
			paras	= 0;
		case '-showt'
			paras	= 0;
		case '-smoopara'
			paras	= 1;
		case '-smoother'
			paras	= 3;
		case '-span'
			paras	= .5;
		case '-spdec'
			paras	= .1;
		case '-spmin'
			paras	= .1;
		case '-sptol'
			paras	= 1e-2;
		case '-tol'
			paras	= 1e-2;
		case '-translation'
			paras	= 0;
		case '-win'
			paras	= 10;
		otherwise
			paras	= [];
	  end	% switch
	end	% if nargin==3
	if ~isempty(paras)
		option	= paraset(pnames,paras,pstring);
	end
else
	option	= [];
	return
end

