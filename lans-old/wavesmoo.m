%	wavesmoo	- Wavelet Smoothing
%
%	function [ny]	= wavesmoo(x,y,xwant,para,wavelet,wpara)
%
%	INPUTS
%	======
%	x	: independent scalars				(row vector)
%	y	: dependent vector				(col vectors)
%		  (indeptly smoothed in each row/dim)
%	xwant	: indices of x whose smoothed value is desired	(row vector)
%	para	: see getpara.m					(row vector)
%
%	OUTPUTS
%	=======
%	ny	: new y corresponding to x(xwant) (col vectors)
%
%	requires WAVELAB 0.700 or above
%
%	smoopara	= wiggly 1,.5,.25,.125,... smoother

%	(C) 2000.06.28 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[ny]	= wavesmoo(x,y,xwant,para,wavelet,wpara)

[d n]	= size(y);
getpara;

%----------	DWT parameters
L	= 0;
qmf	= makeonfilter(wavelet,wpara);

%----------	pad signal with 0's
[dum,j]	= dyadlength(y(1,:));
padsize	= 2^j-n;
py	= [y zeros(d,padsize)];
npy	= 2^j;

%----------	find smallest dyadic cutoff!
% smoothpara	0		1
%		bad		smooth!
% retain	all(2^j)	none except 1				
%retain	= max(smoopara,1/(2^(j/2)));
smoopara=span;
fs	= round(smoopara*j);

%----------	DWT each dimension
for dim=1:d
	wc	= fwt_po(py(dim,:),L,qmf);
	twc	= wc;
	retain	= 2^(j-fs);
	throwindex	= retain+1:npy;
	if ~isempty(throwindex)
		wc(throwindex)	= zeros(size(throwindex));
	end
	ry	= iwt_po(wc,L,qmf);
	ny(dim,1:n)	= ry(1:n);
end


return
%------------------------
figure(2)
scatline([x(1,:);y(1,:);x(1,:);y(2,:)],[x(1,:);ny(1,:);x(1,:);ny(2,:)]);
axis normal
drawnow;
figure(1)
