%	hplotv		- Plot an n-D vector in overwrite mode
%
%	function	[]	= hplotv(v1,t1,v2,t2,m);
%
%	INPUTS
%	======
%	v1	: vector 1					(col vectors)
%	t1	: 2 char string defining plot type		(row vector)
%	*v2	: vector 2					(col vectors)
%	*t2	: 2 char string defining plot type		(row vector)
%
%	*optional

%	(C) 2000.06.28 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function	[]	= hplotv(v1,t1,v2,t2,m);

[d n]	= size(v1);

ndig	= floor(d/2);
row	= floor(sqrt(ndig));
col	= ceil(sqrt(ndig));

if nargin<2
	t1 = 'x';
end

for j=1:ndig
	if d>2
		subplot(row,col,j);
	end
	hold on;
	if nargin>2
		plot(v1(2*j-1,:),v1(2*j,:),t1,v2(2*j-1,:),v2(2*j,:),t2);
	else
		plot(v1(2*j-1,:),v1(2*j,:),t1);
	end
end

hold off;

