%	str2row		- Convert a 1-D string to numbers  
%
%	[row]	= str2row(str)
%
%	_____OUTPUT_____________________________________________________________
%	row	output numbers				(row vector)
%
%	_____INPUT______________________________________________________________
%	str	row string with spaces as separators only	(string)
%		* assumed all spcs have been previously merged via strfilter
%
%	_____EXAMPLE____________________________________________________________
%	str2row('1 2 3 4 5') returns [1 2 3 4 5]
%	str2row('   1 2 3 4   5') returns error
%
%	_____SEE ALSO___________________________________________________________
%	kstr2num	strfilter
%
%	(C) 1998.02.18 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [row]	= str2row(str)

%----- append 1 spc at end
if str(length(str))~=' '
	str(length(str)+1)	= ' ';
end

%----- remove 1st spc if exists
if str(1)==' ',
	str	= str(2:length(str));
end

%----- find numbers (1 spc after each number)
spc	= findstr(str,' ');
n	= length(spc);
row	= zeros(1,n);

sidx	= 1;
for j=1:n
	eidx	= spc(j)-1;
	row(j)	= str2num(str(sidx:eidx));
	sidx	= spc(j)+1;	
end
