%	cscat		- Plot class labeled data points
%
%	[]	= cscat(ldata[,options])
%
%	_____INPUTS_____________________________________________________________
%	ldata		class labeled data			(col vectors)
%	options	see lanspara.m					(string)
%		-plotdim
%		-plotover
%
%	_____NOTES______________________________________________________________
%	for demo, call functions without parameters
%
%	_____SEE ALSO___________________________________________________________
%	scatline
%
%	(C) 1998.4.3 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function []	= cscat(ldata,options)

if nargin>0
%--------------------	regular
%----------	default options
if nargin<2
	options	= [];
end

[d1,n]		= size(ldata);
d		= d1-1;
[ldata,gi,nc]	= group(ldata);

for i=1:nc
	crange	= gi(i,2):gi(i,3);
	cdata	= ldata(1:d,crange);
	lans_scatline(cdata,[],i,options);
end
%--------------------	regular ends
else
%--------------------	demo
disp('running cscat.m in demo mode');
d	= 8;
n	= 10;
c	= 3;
data		= rand(d,n);
data(d+1,:)	= ones(1,n);
x		= data;
for i=2:c
	data		= rand(d,n);
	data(d+1,:)	= i*ones(1,n);
	x		= [x data];
end
cscat(x);
%--------------------	demo ends
end
