#!/usr/bin/perl
#
#	Kui-yu Chang
#
#	kuiyu@computer.org
#
#	14 Apr 1998
#
#	This script needs 1 arguement
#
#	e.g.	parse.pl sim.m
#
#	replaces "main()" in g_sim.cxx by "main(int *argc,char **argv)"
#	and overwrites g_sim.cxx
#
#	Filename can but need not have .m extension
#
#	Usually, matcom is first used to compile a program, e.g. matcom sim.m
#	which will generate a g_sim.cxx file
#
#	This program modifies the g_sim.cxx file to accept input arguements.
#	Use of the matcom directive %MATCOM% C= will embed argv[?] commands into
#	you matlab files.


#	(C) 1998.04.14	Kui-yu Chang
#	http://lans.ece.utexas.edu/~kuiyu
#	This program is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation; either version 2 of the License, or
#	(at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program; if not, write to the Free Software
#	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
#	or check
#			http://www.gnu.org/


$FileCase	= 1;
$arg		= shift;

#----------	remove .m suffix if present
$l	= length $arg;
$m	= ".m";

$mid	= $arg;

if ($l>2)	{
		$found	= index $arg,$m,$l-2;
		if ($found ne -1)	{
					$mid	= substr $arg,0,$l-2;
					}
		}

$file	= join "",('g_',$mid,'.cxx');

#----------	read in all lines of file
if (!open(F,"<$file")){
	print "Error reading $file\n";
	exit;
	}
@lines	= <F>;
close(F);

#----------	replace main() with main(int *argc,char **argv)
$Replacecommand	= 's"main\(\)"main(int *argc, char **argv)"';
$changed	= 0;
$linr		= 0;
for (@lines){
	$linr ++;
	$old	= $_;		# $_ is current line in for (@lines)
	$result	= eval $Replacecommand;
	if ($old ne $_){
		print "Replacing main() at Line ($linr) in $file:\n$old$_";
		$changed	= 1;
		last;
		}
	elsif( $result ){
		print "Scanning line ($linr): $_";
		}
	}

#----------	save modified file
if ($changed){
	if (!open(F,">$file")){
		print "$file not writable.\n";
		exit;
		}
	print F @lines;
	close F;
	print "$file modified.\n";
	}	

