%	lansparse.pl	- PERL script that adds input arguements to g_***.cxx
%
%	_____OUTPUTS____________________________________________________________
%	g_***.cxx	modified c++ file
%
%	_____INPUTS_____________________________________________________________
%	***		name of g_***.cxx file
%
%	_____EXAMPLE____________________________________________________________
%	lansparse.pl sim1
%	will convert 'main()' in g_sim1.cxx to
%	'main(int *argv, char **argc)'
%
%	_____NOTES______________________________________________________________
%	the actual script is lansparse.pl
%
%	_____SEE ALSO___________________________________________________________
%
%	(C) 1998.04.17 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

