%	alias.m		- aliases to append to .cshrc
%
%	_____NOTES______________________________________________________________
%	-Cut below the cutline (31 lines) and paste the contents into your
%	 unix shell initialization file (e.g. .cshrc for csh shell)
%	-The aliases ASSUMES your working directory is the base, e.g. sim1/
%		lansmd
%			creates default directories under sim1/, namely
%				/src
%				/bin
%				/tmp
%				/log
%				/in
%				/out
%
%		lanscp newsim
%			copies	lansroot/matlab/matcom.src/sim.m to
%				src/newsim.m
%			copies	lansroot/matlab/io/paraget.m to
%				bin/newsim.para
%
%		lanscc sim
%			translate/compile	src/sim.m	to	bin/sim
%			(using parse.pl)
%
%	_____SEE ALSO___________________________________________________________
%	lansparse.pl	sim.m
%
%	(C) 1998.4.17 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/


----------8<----------8<----------8<----------8<----------

#----------
#----------	PROJECT SETUP (assumes pwd is base directory)
#----------
#	� 1998.4.19 Kui-yu Chang
#	http://www.lans.ece.utexas.edu/~kuiyu
#
#
#		Simulation Procedure
#
#	1	Create a base directory and go there, e.g. sim1/
#	2	'lansmd' creates subdirectories
#	3	'lanscp newsim' creates sample src/newsim.m & bin/newsim.para
#	4	Modify src/newsim.m to your liking
#	5	Modify bin/newsim.para to your liking
#	6	'lanscc newsim' AFTER cd sim1/ generates bin/sim executable
#	7	Execute bin/newsim newsim.para (might need to switch to bin/)

#----------	root directory for lans library
setenv	lansroot	/home/pegasus/kuiyu/lans

#----------     initial setup of simulation directories (see documentation)
alias   lansmd	'mkdir src;mkdir bin;mkdir tmp;mkdir log;mkdir in;mkdir out'

#----------     copy matcom template files (NO SUFFIX .m)
alias   lanscp	'cp $lansroot/matlab/matcom.src/sim.m src/\!*.m;cp $lansroot/matlab/io/paraget.m bin/\!*.para'

#----------     MATCOM src/sim1.m file --> bin/sim1 executable
#----------	usage:	lanscc sim1	(NO SUFFIX .m)
alias   lanscc	'cd tmp;matcom ../src/\!* -make;$lansroot/matlab/matcom.src/lansparse.pl \!*;make -f \!*.mak;mv \!* ../bin/\!*;cd ..'
