%	sim.m		- template file for simulation
%
%	_____NOTES______________________________________________________________
%	Steps for compiling programs with MATCOM
%
%	1	Create a base directory and go there, e.g. sim1/
%	2	'lansmd' creates subdirectories
%	3	'lanscp newsim' creates sample src/newsim.m & bin/newsim.para
%	4	Modify src/newsim.m to your liking
%	5	Modify bin/newsim.para to your liking
%	6	'lanscc newsim' AFTER cd sim1/ generates bin/sim executable
%	7	Execute bin/newsim newsim.para (might need to switch to bin/)
%
%	_____SEE ALSO___________________________________________________________
%	alias.m		lansparse.pl	paraget.m
%
%	(C) 1998.04.19 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/


%----------	load parameter file
parafile	= '../bin/tg1.para';	%MATCOM% C= parafile	= TM(argv[1]);
para		= loadpara(parafile);

%----------	get basedir
s		= filesep;
basedir		= paraget('-basedir',para);
l		= length(basedir);
if (basedir(l)==s)	
	basedir	= basedir(1:l-1);
end

%----------	get output pathnames and filenames
inputfile	= paraget('-inputfile',para);
logpath		= [basedir s 'log'];
logfile		= [basedir s 'log' s paraget('-logfile',para)];
outputfile	= [basedir s 'out' s paraget('-outputfile',para)];

%----------     append simulation code below    --------------------------------
