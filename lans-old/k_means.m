%	k-means		- K-means clustering

%	(C) 2000.06.28 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/


function [means,tag]=k_means(box,K,weight);
%

%		The K_means algorithm (vish's) 
%function [means,tag]=k_means(box,K);
%box contains the data_set to be clustered one row per pattern
%K is the number of cluster centers needed
%means gives the resulting K means
%tag gives the number of patterns associated with each mean

means=box(1:K,:);               % K starting


%means   =box([1 13 25 37],:);
%means	= mean(box);
%means=(rand(size(means))-.5)*1e-2;      % random starting
%means	= zeros(size(means));	% zero starting
flag=1;
while(flag==1)

means_old=means;
means=zeros(K,size(box,2));
tag=zeros(K,1);

for i=1 : size(box,1)
	y=check_km(box(i,:),means_old,weight);
	for j=1:K
		if j==y
		  means(j,:)=(tag(j)*means(j,:) + box(i,:))/(tag(j)+1);
		  tag(j)=tag(j)+1;
		end
	end
end

if means==means_old
	flag=0;
end


end %while
