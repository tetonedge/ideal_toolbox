%	kstr2num	- Str2num version compatible with MATCOM 3.0
%
%	[num]	= kstr2num(str)
%	_____OUTPUT_____________________________________________________________
%	num	converted numbers				(row vector)
%
%	_____INPUT______________________________________________________________
%	str	string data containing numbers			(string)
%		if matrices, must have regular # rows and #cols
%
%	_____EXAMPLE____________________________________________________________
%	a='1 2 3 4 5 6 7 8';a(8)=10; %linefeed 
%	kstr2num(a)=
%	1     2     3     4
%	5     6     7     8
%
%	(C) 1998.02.18 Kui-yu Chang
%	http://lans.ece.utexas.edu/~kuiyu

%	This program is free software; you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation; either version 2 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program; if not, write to the Free Software
%	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA
%	or check
%			http://www.gnu.org/

function [num]	= kstr2num(str)

%----- append one linefeed at end
n	= length(str);		% total length
str(n+1)= 10;

%----- merge spaces, replace tabs
data	= pprocess(str);

%----- remove first linefeed if exists
if data(1)==10
	data=	data(2:length(data));
end

%----- find linefeeds, one after each line of data
lfeeds	= find(data==10);
if length(lfeeds)==0
	lfeeds	= length(data)+1;
	row	= 1;
else
	row	= length(lfeeds);
end

%----- find # items in first row (should be representative)
row1	= [data(1:lfeeds(1)-1)];
num1	= str2row(row1);
col	= length(num1);

%----- initialize num matrix
num	= zeros(row,col);

%========== extract subsequent rows 1 at a time
sidx	= 1;
for i=1:row
	eidx	= lfeeds(i)-1;
	therow	= data(sidx:eidx);
	num(i,:)= str2row(therow);
	sidx	= lfeeds(i)+1;	
end
